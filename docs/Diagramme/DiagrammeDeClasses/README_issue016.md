[retour au README.md](../../../README.md)  
[Retour au diagramme de classes](../README_DIAGRAMMES.md)  

# Introduction au Diagramme de Classes : Statistiques pour Coach

Bienvenue dans l'univers captivant de notre système de gestion d'activités sportives avec une mise au point spéciale sur les statistiques destinées aux coaches. Ce diagramme de classes offre une vue approfondie de la manière dont les utilisateurs, en particulier les athlètes et les coaches, interagissent avec les données de performance.

**Entités Principales :**

- **Utilisateur (User) :** Représente les individus inscrits sur notre plateforme, avec des détails personnels et un rôle spécifique dans l'écosystème sportif.

- **Athlète (Athlete) :** Un type spécialisé d'utilisateur qui peut enregistrer des statistiques liées à ses activités sportives.

- **Coach (Coach) :** Un rôle qui s'étend à partir de la classe abstraite Role, dédié à la gestion des athlètes et de leurs statistiques.

- **Statistique (Statistique) :** Contient des informations détaillées sur les performances sportives d'un athlète, telles que la distance totale, le poids, le temps total, la fréquence cardiaque moyenne, minimale et maximale, ainsi que les calories brûlées.

**Relations Clés :**

- Les Utilisateurs ont un rôle spécifique (Athlete, Coach) qui influence leurs interactions au sein de la plateforme.

- Un Coach peut gérer une liste d'athlètes et avoir accès à leurs statistiques.

- Un Athlète peut enregistrer plusieurs statistiques liées à ses activités.

**Objectif Principal :**

- Permettre aux coaches d'accéder et de surveiller les statistiques détaillées de leurs athlètes, offrant ainsi un aperçu complet de leurs performances sportives. 

Explorez ce diagramme pour découvrir comment notre application crée une synergie entre les utilisateurs, les rôles, et les statistiques, contribuant ainsi à une expérience enrichissante dans le suivi des activités sportives.


```plantuml
@startuml
class Athlete {
    + getAthlete(): Athlete
    + getStatistic(): ?array
    + getUsersList(): array
    + getUserList(user: User): User
    + CheckAdd(user: User): bool
    + addUser(user: User): bool
    + removeUser(user: User): bool
}

abstract class Coach {
    + abstract getUsersList(): ?array
    + abstract getUserList(user: User): User
}

class CoachAthlete {
    + getUsersList(): ?array
    + getUserList(user: User): User
}

abstract class Role {
    - int id
    - array usersList
    - TrainingRepository trainingRepository
    + abstract __construct(trainingRepository: ?TrainingRepository)
    + abstract getUsersList(): ?array
    + abstract getUserList(user: User): User
    + abstract getTraining(): ?TrainingRepository
    + abstract getTrainingsList(): ?array
    + abstract getTrainingList(training: Training): ?Training
    + abstract CheckAdd(user: User): bool
    + abstract CheckAddTraining(training: Training): bool
    + abstract addUser(user: User): bool
    + abstract removeUser(user: User): bool
    + abstract addTraining(training: Training): bool
    + abstract removeTraining(training: Training): bool
}

class User {
    - int id
    - String username
    - string nom
    - string prenom
    - string email
    - string motDePasse
    - string sexe
    - float taille
    - float poids
    - DateTime dateNaissance
    + __construct(id: int, username: String, nom: string, prenom: string, email: string, motDePasse: string, sexe: string, taille: float, poids: float, dateNaissance: DateTime, role: Role)
    + getId(): int
    + setId(id: int): void
    + getUsername(): String
    + setUsername(username: int): void
    + getNom(): string
    + setNom(nom: string): void
    + getPrenom(): string
    + setPrenom(prenom: string): void
    + getEmail(): string
    + setEmail(email: string): void
    + getMotDePasse(): string
    + setMotDePasse(motDePasse: string): void
    + getSexe(): string
    + setSexe(sexe: string): void
    + getTaille(): float
    + setTaille(taille: float): void
    + getPoids(): float
    + setPoids(poids: float): void
    + getDateNaissance(): DateTime
    + setDateNaissance(dateNaissance: DateTime): void
    + getRole(): Role
    + setRole(role: Role): void
    + isValidPassword(password: string): bool
    + __toString(): String
}

class Statistique {
    - idStat: int
    - distanceTotale: float
    - poids: float
    - tempsTotal: time
    - FCmoyenne: int
    - FCmin: int
    - FCmax: int
    - cloriesBrulees: int
    + getIdStat(): int
    + getDistanceTotale(): float
    + getPoids(): float
    + getTempsTotal(): time
    + getFCmoyenne(): int
    + getFCmin(): int
    + getFCmax(): int
    + getCloriesBrulees(): int
    + __toString(): String
}

CoachAthlete --|> Coach
Coach --|> Role
Athlete --|> Role
User -> Role : role
Role -> User : usersList
Athlete -> Statistique : statsList
@enduml
````
