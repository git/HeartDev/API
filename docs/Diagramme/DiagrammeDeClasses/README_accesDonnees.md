[retour au README.md](../../../README.md)  
[Retour au diagramme de classes](../README_DIAGRAMMES.md)  

# Introduction au Diagramme de la Couche d'Accès aux Données

Bienvenue dans le cœur de notre système, où les données prennent vie à travers des ensembles de données (repositories) structurés et performants. Ce diagramme met en lumière la conception de la couche d'accès aux données de notre application, offrant un aperçu clair de la gestion des entités clées telles que les utilisateurs, les notifications, les demandes de relations et les entraînements.

**Principes Fondamentaux :**

- **IGenericRepository :** Une abstraction générique établissant les contrats essentiels pour l'accès aux données. Définissant des opérations standardisées telles que la récupération, la mise à jour, l'ajout et la suppression d'entités.

- **Interfaces Spécialisées :** Des interfaces telles que `IUserRepository`, `INotificationRepository`, `IRelationshipRequestRepository` et `ITrainingRepository` étendent les fonctionnalités génériques pour répondre aux besoins spécifiques de chaque entité.

**Repositories Concrets :**

- **UserRepository :** Gère les données relatives aux utilisateurs, permettant des opérations de récupération, de mise à jour et de suppression avec une efficacité optimale.

- **NotificationRepository :** Responsable de la gestion des notifications, assurant un accès structuré et une manipulation sécurisée de ces informations cruciales.

- **RelationshipRequestRepository :** Facilite la gestion des demandes de relations entre utilisateurs, garantissant une interaction claire et ordonnée.

- **TrainingRepository :** Permet l'accès et la manipulation des données liées aux entraînements, facilitant le suivi des performances athlétiques.

Explorez ce diagramme pour découvrir la robustesse de notre architecture de gestion des données, mettant en œuvre des pratiques de développement SOLID pour assurer une expérience utilisateur fiable et évolutive.

```plantuml
@startuml couche_acces_aux_donnees
abstract class IGenericRepository {
    + getItemById(int id) : object
    + getNbItems() : int
    + getItems(int index, int count, string orderingPropertyName, bool descending) : array
    + getItemsByName(string substring, int index, int count, string orderingPropertyName, bool descending) : array
    + getItemByName(string substring, int index, int count, string orderingPropertyName, bool descending) : object
    + updateItem(oldItem, newItem) : void
    + addItem(item) : void
    + deleteItem(item) : bool
}
interface IUserRepository extends IGenericRepository {
}
interface INotificationRepository extends IGenericRepository {
}
interface IRelationshipRequestRepository extends IGenericRepository {
}
interface ITrainingRepository extends IGenericRepository {
}
class NotificationRepository implements INotificationRepository {
    - notifications : array
    + getItemById(int id) : object
    + getNbItems() : int
    + getItems(int index, int count, string orderingPropertyName, bool descending) : array
    + getItemsByName(string substring, int index, int count, string orderingPropertyName, bool descending) : array
    + getItemByName(string substring, int index, int count, string orderingPropertyName, bool descending) : object
    + updateItem(oldItem, newItem) : void
    + addItem(item) : void
    + deleteItem(item) : bool
}
class RelationshipRequestRepository implements IRelationshipRequestRepository {
    - requests : array
    + getItemById(int id) : object
    + getNbItems() : int
    + getItems(int index, int count, string orderingPropertyName, bool descending) : array
    + getItemsByName(string substring, int index, int count, string orderingPropertyName, bool descending) : array
    + getItemByName(string substring, int index, int count, string orderingPropertyName, bool descending) : object
    + updateItem(oldItem, newItem) : void
    + addItem(item) : void
    + deleteItem(item) : bool
}
class TrainingRepository implements ITrainingRepository {
    - trainings : array
    + getItemById(int id) : object
    + getNbItems() : int
    + getItems(int index, int count, string orderingPropertyName, bool descending) : array
    + getItemsByDate(date, int index, int count, string orderingPropertyName, bool descending) : array
    + updateItem(oldItem, newItem) : void
    + addItem(item) : void
    + deleteItem(item) : bool
}
class UserRepository implements IUserRepository {
    - users : array
    + getItemById(int id) : object
    + getNbItems() : int
    + getItems(int index, int count, string orderingPropertyName, bool descending) : array
    + getItemsByName(string substring, int index, int count, string orderingPropertyName, bool descending) : array
    + getItemByName(string substring, int index, int count, string orderingPropertyName, bool descending) : object
    + updateItem(oldItem, newItem) : void
    + addItem(item) : void
    + deleteItem(item) : bool
}
@enduml
```