[retour au README.md](../../../README.md)  
[Retour au diagramme de classes](../README_DIAGRAMMES.md)  

# Introduction au Diagramme de Séquence : Gestion des Amis

Bienvenue dans le processus dynamique de gestion des amis au sein de notre application ! Ce diagramme de séquence met en lumière les interactions entre l'utilisateur et l'application, ainsi que le flux d'informations entre les différentes composantes du système.

**Acteurs Principaux :**

- **Utilisateur (u) :** L'individu interagissant avec l'application, souhaitant consulter et gérer sa liste d'amis.

**Flux d'Interaction :**

1. **Demande de la Page d'Amis :** L'utilisateur déclenche la demande de la page d'amis, amorçant le processus d'affichage de sa liste d'amis.

2. **Récupération des Amis :** Le contrôleur (Controller) reçoit la demande et interagit avec le modèle (Model) pour récupérer la liste d'amis associée à l'identifiant de l'utilisateur.

    - *Cas de Récupération Réussi :* Si la récupération est réussie, le modèle transmet la liste d'amis au contrôleur.
    
    - *Cas d'Échec de Récupération :* En cas d'échec, une notification d'erreur est renvoyée.

3. **Affichage de la Liste d'Amis :** Le contrôleur rend la vue (View) en utilisant la liste d'amis récupérée, qui est ensuite affichée à l'utilisateur.

4. **Suppression d'un Ami :** L'utilisateur décide de supprimer un ami spécifique en cliquant sur l'option correspondante.

5. **Traitement de la Suppression :** Le contrôleur, en réponse à la demande de suppression, envoie une requête au modèle pour effectuer la suppression de l'ami identifié par son identifiant utilisateur (idUser).

    - *Cas de Suppression Réussie :* Si la suppression est réussie, le modèle renvoie la liste d'amis mise à jour.
    
    - *Cas d'Échec de Suppression :* En cas d'échec, une notification d'erreur est renvoyée.

6. **Affichage de la Liste d'Amis Mise à Jour :** La vue est mise à jour avec la nouvelle liste d'amis, qui est ensuite affichée à l'utilisateur.

À travers ce diagramme de séquence, découvrez comment notre application gère de manière fluide et réactive les interactions de l'utilisateur avec sa liste d'amis, garantissant une expérience utilisateur cohérente et sans heurts.

```plantuml
actor User as u
boundary View as v
control Controller as c
entity Model as m

u->v: Request Friends Page
v->c: Get /Friends
c->m: getFriends(userId)
alt successful retrieval
    m-->c: friendsList: User[]
else retrieval failed
    m-->c: error
end
c-->v: renderView(friendsList)
v-->u: Display Friends

u->v: clickDeleteFriend(idUser)
v->c: Post: deleteFriend(idUser)
c->m: deleteFriend(idUser)
alt successful deletion
    m-->c: updatedFriendsList: User[]
else deletion failed
    m-->c: error
end
c-->v: renderView(updatedFriendsList)
v-->u: Display Updated Friends
```