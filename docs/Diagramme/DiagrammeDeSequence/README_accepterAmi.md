[retour au README.md](../../../README.md)  
[Retour au diagramme de classes](../README_DIAGRAMMES.md)  

# Introduction au Diagramme de Séquence : Gestion des Demandes d'Amis

Bienvenue dans le processus dynamique de gestion des demandes d'amis au sein de notre application ! Ce diagramme de séquence met en évidence les étapes clés impliquées dans la gestion des demandes d'amis entre utilisateurs.

**Acteurs Principaux :**

- **Utilisateur (u) :** L'individu interagissant avec l'application, recevant et répondant aux demandes d'amis.

**Flux d'Interaction :**

1. **Réception d'une Demande d'Ami :** Lorsqu'un utilisateur reçoit une demande d'ami, le modèle (Model) notifie le contrôleur (Controller) de la nouvelle demande, spécifiant l'identifiant de l'utilisateur émetteur.

2. **Affichage de la Demande d'Ami :** Le contrôleur transmet l'information à la vue (View), qui affiche la demande d'ami à l'utilisateur.

3. **Affichage de la Page des Demandes d'Amis :** L'utilisateur visualise la page des demandes d'amis dans l'interface utilisateur.

4. **Réponse à la Demande d'Ami :** L'utilisateur prend une décision quant à la demande d'ami, en répondant par un choix binaire (accepter ou refuser).

5. **Enregistrement de la Réponse :** La vue (View) transmet la réponse de l'utilisateur au contrôleur, qui enregistre cette réponse.

6. **Envoi de la Réponse :** Le contrôleur communique avec le modèle pour envoyer la réponse, indiquant si la demande a été acceptée (true) ou refusée (false).

À travers ce diagramme de séquence, découvrez comment notre application gère efficacement le processus de gestion des demandes d'amis, offrant aux utilisateurs une expérience transparente et réactive lors de l'établissement de connexions sociales au sein de la plateforme.

````plantuml
@startuml
actor User as u
boundary View as v
control Controller as c
entity Model as m

m-->c: pendingRequests: Request[]

c-->v: DisplayPendingRequests(pendingRequests)
v-->u: Show Friend Requests

u->v: RespondToRequest(requestId, response)
v-->c: RecordResponse(requestId, response)

    c->m: UpdateRequestStatus(requestId, response)
    m-->c: updateStatus: success/failure
    c-->v: NotifyUpdateResult(updateStatus)
    v-->u: Show Response Result

@enduml
``````