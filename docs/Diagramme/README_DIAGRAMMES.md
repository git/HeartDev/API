[retour au README.md](../../README.md)

# Diagrammes nécéssaires à notre projet

## Diagrammes de classes
- [issue016  - Statistiques coach ](DiagrammeDeClasses/README_issue016.md)
- [couche d'accès aux données](DiagrammeDeClasses/README_accesDonnees.md)
- [Diagramme général](DiagrammeDeClasses/README_DIAGRAMME.md)

## Diagrammes de séquence
- [Envoi de demande d'ami](DiagrammeDeSequence/README_demandeAmi.md)
- [Accepter une demande d'ami](DiagrammeDeSequence/README_accepterAmi.md)
- [Supprimer un ami](DiagrammeDeSequence/README_suppressionAmi.md)
- [issue021  - Authentification ](DiagrammeDeSequence/README_issue021.md)

## Diagrammes de cas d'utilisation
- [Cas d'utilisation pour la gestion du compte et des amitiés](CasUtilisations/README_gestionCompteAmitie.md)  
- [Cas d'utilisation pour la gestion des activités et données](CasUtilisations/README_gestionActivites.md)

## Base de données
- [MCD - MLD](BDD/README_BDD.md)