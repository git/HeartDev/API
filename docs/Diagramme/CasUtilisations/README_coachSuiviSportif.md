[retour au README.md](../../../README.md)  
[Retour au diagramme de classes](../README_DIAGRAMMES.md)  

# Introduction au Cas d'utilisation : Suivi d'une Équipe Sportive

Bienvenue dans le monde dynamique du suivi d'équipe sportive, où notre application offre une plateforme complète pour les entraîneurs soucieux d'optimiser les performances de leurs athlètes. Ce diagramme de cas d'utilisation vous plonge dans les fonctionnalités clés qui facilitent la gestion d'une équipe sportive avec efficacité.

**Acteurs Principaux :**

- **Coach :** Le protagoniste central, utilisant l'application pour gérer et superviser son équipe.

**Fonctionnalités Clés :**

- **Ajouter un Athlète :** Permet au coach d'ajouter de nouveaux membres à son équipe, avec des étapes incluant la validation par l'athlète et l'authentification.

- **Supprimer un Athlète :** Offre la possibilité de retirer des athlètes de l'équipe, avec une authentification préalable pour garantir la légitimité de l'action.

- **Afficher ses Athlètes :** Permet au coach de visualiser la liste complète de ses athlètes, nécessitant une authentification pour accéder à ces informations sensibles.

- **Afficher les Activités de Tous les Athlètes :** Donne au coach un aperçu global des activités de toute l'équipe, nécessitant une authentification pour garantir la confidentialité des données.

**Flux d'Interaction :**

- Le processus d'ajout d'un athlète inclut des étapes telles que la validation par l'athlète et l'authentification, garantissant une intégration fluide.

- Les actions de suppression, affichage des athlètes et affichage des activités nécessitent une authentification préalable pour assurer la sécurité des données.

- Des extensions telles que la visualisation des activités d'un athlète et l'analyse des performances offrent des fonctionnalités avancées pour un suivi détaillé.

Explorez ce diagramme pour comprendre l'étendue des fonctionnalités que notre application offre aux entraîneurs, les aidant à gérer leurs équipes de manière efficace et à maximiser le potentiel de chaque athlète.

```plantuml
left to right direction
:Coach: as a

a --> (Ajouter un athlète)
a --> (Supprimer un athlète)
a --> (Afficher ses athlètes )
a --> (Afficher les activités de tous les athlètes)
(Ajouter un athlète).>(Validation par l'athlète) : <<include>>
(Ajouter un athlète)..>(S'authentifier) : <<include>>
(Supprimer un athlète)..>(S'authentifier) : <<include>>
(Afficher ses athlètes )..>(S'authentifier) : <<include>>
(Afficher les activités de tous les athlètes)..>(S'authentifier) : <<include>>
(S'authentifier)..>(S'inscrire) : <<extends>>
(S'inscrire).>(Inscription Coach) : <<include>>
(S'authentifier)..>(Se connecter) : <<include>>
(Afficher ses athlètes )..>(Voir les activités d'un athlète) : <<extends>>
(Afficher ses athlètes )..>(Voir les stats d'un athlète) : <<extends>>
(Afficher les activités de tous les athlètes)..>(Sélectionner une activité) : <<include>>
(Sélectionner une activité)..>(Voir l'analyse) : <<extends>>
(Sélectionner une activité)..>(Exporter l'analyse) : <<extends>>
(Voir les activités d'un athlète)..>(Voir l'analyse) : <<extends>>
(Voir les activités d'un athlète)..>(Exporter l'analyse) : <<extends>>
```