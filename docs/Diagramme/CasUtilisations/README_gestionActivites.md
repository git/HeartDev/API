[retour au README.md](../../../README.md)  
[Retour au diagramme de classes](../README_DIAGRAMMES.md)  

# Introduction au Cas d'utilisation : Gestion d'Activités pour un Athlète

Bienvenue dans l'univers dédié à la gestion d'activités sportives personnalisées pour les athlètes ! Ce diagramme de cas d'utilisation explore les différentes fonctionnalités offertes aux utilisateurs, mettant en avant la flexibilité et la richesse d'interactions pour une expérience utilisateur optimale.

**Acteurs Principaux :**

- **Athlète :** Le protagoniste central, utilisant l'application pour importer, gérer et analyser ses activités sportives.

**Fonctionnalités Clés :**

- **Importer des Données :** Permet à l'athlète d'importer des données d'activités depuis différentes sources, avec la possibilité de spécifier la source pour une intégration transparente.

- **Exporter Mes Données :** Offre la possibilité d'exporter l'ensemble des activités, avec des extensions pour exporter une activité spécifique, le tout soumis à une authentification préalable.

- **Ajouter une Activité :** Permet à l'athlète d'ajouter de nouvelles activités, avec des étapes inclusives telles que la saisie du titre, du type d'activité, de la source, du matériel utilisé et de la visibilité, chacune accessible via l'authentification.

- **Voir une Activité :** Permet à l'athlète de visualiser en détail une activité particulière, avec la possibilité d'exporter une analyse et de gérer la visibilité, soumis à une authentification.

- **Supprimer une Activité :** Offre la possibilité de retirer une activité, requérant une authentification pour garantir la sécurité des données.

**Flux d'Interaction :**

- Les actions telles que l'importation, l'exportation, l'ajout et la visualisation d'activités impliquent une authentification préalable pour garantir la confidentialité des données personnelles.

- Des inclusions précises, telles que la saisie du titre, du type d'activité, de la source, du matériel utilisé et de la visibilité, sont incorporées dans le processus d'ajout d'une activité, offrant une expérience utilisateur détaillée et conviviale.

Explorez ce diagramme pour comprendre la manière dont notre application place la gestion d'activités entre les mains des athlètes, les encourageant à suivre, analyser et optimiser leurs performances sportives de manière personnalisée et efficace.


```plantuml
left to right direction
:Athlete: as a

a --> (Importer des données) 
(Importer des données) .> (Saisir la source) : <<include>>
a --> (Exporter mes données)
(Exporter mes données) .>(Exporter toutes les activités): <<extends>>
(Exporter mes données) ..>(Exporter une activité): <<include>>
a --> (Ajouter une activité) 
(Ajouter une activité) ..>(Saisir un titre et une description): <<include>>
(Ajouter une activité) ..>(Saisir le type d'activité): <<include>>
(Ajouter une activité) .>(Saisir la source): <<include>>
(Saisir la source) ..>(Saisir le matériel utilisé): <<include>>
(Ajouter une activité) ..>(Saisir la visibilité): <<include>>
a --> (Voir une activité)
(Voir une activité) ..>(Exporter l'analyse): <<extends>>
(Voir une activité) ..>(Saisir la visibilité): <<extends>>
a --> (Supprimer une activité) 
(Supprimer une activité) ..>(S'authentifier): <<include>>
(Importer des données) ...>(S'authentifier): <<include>>
(Exporter mes données) ...>(S'authentifier): <<include>>
(Ajouter une activité) ...>(S'authentifier): <<include>>
(Voir une activité) ...>(S'authentifier): <<include>>
```