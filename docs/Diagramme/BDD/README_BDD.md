[retour au README.md](../../../README.md)  
[Retour au diagramme de classes](../README_DIAGRAMMES.md)

# BDD

## Modèle Logique de Données (MLD)

Le MLD représente la structure de données de l'application, décrivant les entités et les relations entre elles. Voici un aperçu des principales entités du MLD :

### Athlète (Athlete)

L'entité principale représentant un athlète avec ces informations propre à lui telles que l'identifiant, le nom, le prénom, l'email, etc. Les athlètes peuvent être coach avec le boolean idCoach et être liés par des amitiés, ou par un coaching via la table `Amitie`.

### Amitié (Friendship)

Une entité qui modélise les relations d'amitié entre les athlètes et de coaching entre les athlètes et les coachs. Elle stocke les identifiants des deux utilisateurs impliqués.

### Notification (Notification)

L'entité qui stocke les notifications destinées aux athlètes, avec des détails tels que le message, la date, le statut, et le degré d'urgence.

### Envoi de Notification (SendNotification)

Une entité de liaison entre les athlètes et les notifications, indiquant quel athlète ou coach a envoyé quelle notification. Cela peut-être utile lors d'une notification d'ajout d'amie par exemple.

### Statistique (Statistic)

Les statistiques relatives à un athlètes, y compris le poids, la fréquence cardiaque moyenne, la fréquence cardiaque maximale, etc.

### Entraînement (Training)

Détails sur les sessions d'entraînement planifiés par un coach pour ses athlètes, comprenant la date, la description, la localisation, etc. Les athlètes peuvent participer à des entraînements et donner leur feedback sur l'entrainement donné.

### Participation (Participate)

Une entité de liaison entre les athlètes et les entraînements, indiquant quels athlètes participent à quels entraînements.

### Don (GiveParticipation)

Une entité de liaison entre les coachs et les entraînements, indiquant quels coachs ont attribué quels entraînements.

### Source de Données (DataSource)

L'entité représentant la source des données des enregistrements sportif, telle que le type, le modèle, la précision, etc., utilisée par les athlètes pour enregistrer une ou des activités.

### Activité (Activity)

Les détails des activités des athlètes, y compris le type, la date, les heures de début et de fin, l'effort ressenti, etc.

### Fréquence Cardiaque (HeartRate)

Les données de fréquence cardiaque enregistrées pendant les activités, avec des informations telles que l'altitude, la température, etc.

Ce MLD forme la base de données sous-jacente pour l'application, offrant une structure organisée pour stocker et récupérer les informations relatives aux athlètes et à leurs activités.

```plantuml
@startuml
skinparam classAttributeIconSize 0
package MLD{
entity "Athlete" as athlete {
    {static} idAthlete
    username
    nom
    prenom
    email
    sexe
    taille
    poids
    motDePasse
    dateNaissance
    isCoach
}

entity "Amitie" as friendship{
{static}# idAthlete1
{static}# idAthlete2
début
}

entity "Notification" as notif {
    {static} idNotif
    message
    date
    statut
    urgence
    #athleteId
}

entity "Envoi" as sendNotif{
{static}# idAthlete
{static}# idNotif
}

entity "Statistique" as stats {
    {static} idStatistique
    poids
    fcMoyenne
    fcMax
    caloriesBruleesMoy
    date 
    #athleteId
}

entity "Entrainement" as training {
    {static} idEntrainement
    date
    description
    latitude
    longitude
    feedback
    #athleteId
}

entity "Participe" as takepart {
    {static} #athleteId
    {static} #entrainementId   
}

entity "Donne" as givepart {
    {static} #coachId
    {static} #entrainementId   
}


entity "SourceDonnee" as source {
    {static} idSource
    type 
    modele
    precision
    #athleteId
}  

entity "Activite" as activity {
    {static} idActivité
    type
    date
    heureDeDebut
    heureDeFin
    effortRessent
    variabilite
    variance
    ecartType
    moyenne
    maximum
    minimum
    temperatureMoyenne
    #athleteId 
    #sourceId
}
entity "FréquenceCardiaque" as fc {
    {static} idFc
    altitude
    temps : time
    température    
    bpm
    longitude
    latitude
    #activitéId
}

}
activity --> athlete
activity --> source
activity <-- fc
athlete <-- source
stats --> athlete
takepart --> athlete
takepart --> training
givepart --> athlete
givepart --> training
sendNotif --> athlete
sendNotif --> notif
friendship --> athlete
notif --> athlete
athlete <-- friendship
@enduml
```

```plantuml
@startuml
skinparam classAttributeIconSize 0
package MCD{
entity "Athlete" as athlete {
    {static} idAthlete
    username
    nom
    prenom
    email
    sexe
    taille
    poids
    motDePasse
    dateNaissance
    isCoach
}

entity "Notification" as notif {
    {static} idNotif
    message
    date
    statut
    urgence
    #athleteId
}

entity "Statistique" as stats {
    {static} idStatistique
    poids
    fcMoyenne
    fcMax
    caloriesBruleesMoy
    date 
    #athleteId
}

entity "Entrainement" as training {
    {static} idEntrainement
    date
    description
    latitude
    longitude
    feedback
    #athleteId
}

entity "SourceDonnee" as source {
    {static} idSource
    type 
    modele
    precision
    #athleteId
}  

entity "Activite" as activity {
    {static} idActivité
    type
    date
    heureDeDebut
    heureDeFin
    effortRessent
    variabilite
    variance
    ecartType
    moyenne
    maximum
    minimum
    temperatureMoyenne
    #athleteId 
    #sourceId
}

entity "FréquenceCardiaque" as fc {
    {static} idFc
    altitude
    temps : time
    température    
    bpm
    longitude
    latitude
    #activitéId
}

}
activity "0..n" --- "1..1" athlete : réalise
activity "1..n" --- "1..1" source : possede
activity "1..1" --- "1..n" fc : enregistre
athlete "1..n" --- "0..1" source : possede
stats "0..n" --- "1..1" athlete : possede
training "0..n" --- "1..n" athlete : participe
training "0..n" --- "1..1" athlete : donne
athlete "0..n" --- "1..n" athlete : est ami
notif "0..n" --- "1..n" athlete : recoit
notif "0..n" --- "1..1" athlete : envoie
@enduml
```