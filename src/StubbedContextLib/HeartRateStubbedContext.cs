//-----------------------------------------------------------------------
// FILENAME: HeartRateStubbedContext.cs
// PROJECT: StubbedContextLib
// SOLUTION: HeartTrack
// DATE CREATED: 22/02/2024
// AUTHOR: Antoine PEREDERII
//-----------------------------------------------------------------------

using DbContextLib;
using Entities;
using Microsoft.EntityFrameworkCore;

namespace StubbedContextLib
{
    /// <summary>
    /// Represents the stubbed context for heart rate entities.
    /// </summary>
    public class HeartRateStubbedContext : FriendshipStubbedContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HeartRateStubbedContext"/> class.
        /// </summary>
        public HeartRateStubbedContext() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="HeartRateStubbedContext"/> class with the specified options.
        /// </summary>
        /// <param name="options">The options for the context.</param>
        public HeartRateStubbedContext(DbContextOptions<HeartTrackContext> options) : base(options) { }

        /// <summary>
        /// Configures the model for the heart rate stubbed context.
        /// </summary>
        /// <param name="modelBuilder">The model builder instance.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<HeartRateEntity>().HasData(
                new HeartRateEntity { IdHeartRate = 1, Altitude = 0.0, Time = new TimeOnly(13, 00, 30), Temperature = 20.0f, Bpm = 60, Longitude = 35f, Latitude = 66f, ActivityId = 1 },
                new HeartRateEntity { IdHeartRate = 2, Altitude = 10, Time = new TimeOnly(13, 00, 31), Temperature = 20.5f, Bpm = 65, Longitude = 35f, Latitude = 67f, ActivityId = 2 },
                new HeartRateEntity { IdHeartRate = 3, Altitude = 11, Time = new TimeOnly(13, 00, 32), Temperature = 20.0f, Bpm = 71, Longitude = 36f, Latitude = 66f, ActivityId = 1 },
                new HeartRateEntity { IdHeartRate = 4, Altitude = 12, Time = new TimeOnly(13, 00, 33), Temperature = 20.5f, Bpm = 75, Longitude = 36f, Latitude = 67f, ActivityId = 2 },
                new HeartRateEntity { IdHeartRate = 5, Altitude = 13, Time = new TimeOnly(13, 00, 34), Temperature = 20.0f, Bpm = 80, Longitude = 37f, Latitude = 66f, ActivityId = 4 }
            );
        }
    }
}