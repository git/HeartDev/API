//-----------------------------------------------------------------------
// FILENAME: ActivityStubbedContext.cs
// PROJECT: StubbedContextLib
// SOLUTION: HeartTrack
// DATE CREATED: 22/02/2024
// AUTHOR: Antoine PEREDERII
//-----------------------------------------------------------------------

using DbContextLib;
using Entities;
using Microsoft.EntityFrameworkCore;

namespace StubbedContextLib
{
    /// <summary>
    /// Represents a stubbed context for activities.
    /// </summary>
    public class ActivityStubbedContext : HeartTrackContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ActivityStubbedContext"/> class.
        /// </summary>
        public ActivityStubbedContext() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ActivityStubbedContext"/> class with the specified options.
        /// </summary>
        /// <param name="options">The options for the context.</param>
        public ActivityStubbedContext(DbContextOptions<HeartTrackContext> options) : base(options) { }

        /// <summary>
        /// Configures the model for the activity context.
        /// </summary>
        /// <param name="modelBuilder">The model builder instance.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Seed data for activities
            modelBuilder.Entity<ActivityEntity>().HasData(
                new ActivityEntity { IdActivity = 1, Type = "Running", Date = new DateOnly(2023, 01, 10), StartTime = new TimeOnly(13, 00, 34), EndTime = new TimeOnly(14, 00, 22), EffortFelt = 5, Variability = 0.5f, Variance = 0.5f, StandardDeviation = 0.5f, Average = 0.5f, Maximum = 0, Minimum = 0, AverageTemperature = 20.0f, HasAutoPause = false, DataSourceId = 1, AthleteId = 1 },
                new ActivityEntity { IdActivity = 2, Type = "Cycling", Date = new DateOnly(2023, 01, 25), StartTime = new TimeOnly(13, 04, 34), EndTime = new TimeOnly(14, 00, 22), EffortFelt = 5, Variability = 0.5f, Variance = 0.5f, StandardDeviation = 0.5f, Average = 0.5f, Maximum = 0, Minimum = 0, AverageTemperature = 20.0f, HasAutoPause = false, DataSourceId = 2, AthleteId = 2 },
                new ActivityEntity { IdActivity = 3, Type = "Swimming", Date = new DateOnly(2023, 12, 10), StartTime = new TimeOnly(13, 30, 34), EndTime = new TimeOnly(15, 02, 22), EffortFelt = 5, Variability = 0.5f, Variance = 0.5f, StandardDeviation = 0.5f, Average = 0.5f, Maximum = 0, Minimum = 0, AverageTemperature = 20.0f, HasAutoPause = false, DataSourceId = 1, AthleteId = 1 },
                new ActivityEntity { IdActivity = 4, Type = "Walking", Date = new DateOnly(2024, 01, 02), StartTime = new TimeOnly(15, 00, 00), EndTime = new TimeOnly(16, 01, 55), EffortFelt = 5, Variability = 0.5f, Variance = 0.5f, StandardDeviation = 0.5f, Average = 0.5f, Maximum = 0, Minimum = 0, AverageTemperature = 20.0f, HasAutoPause = false, DataSourceId = 3, AthleteId = 5 },
                new ActivityEntity { IdActivity = 5, Type = "Hiking", Date = new DateOnly(2024, 01, 12), StartTime = new TimeOnly(07, 45, 34), EndTime = new TimeOnly(09, 00, 22), EffortFelt = 5, Variability = 0.5f, Variance = 0.5f, StandardDeviation = 0.5f, Average = 0.5f, Maximum = 0, Minimum = 0, AverageTemperature = 20.0f, HasAutoPause = false, DataSourceId = 4, AthleteId = 4 },
                new ActivityEntity { IdActivity = 6, Type = "Climbing", Date = new DateOnly(2024, 01, 27), StartTime = new TimeOnly(13, 30, 01), EndTime = new TimeOnly(14, 00, 22), EffortFelt = 5, Variability = 0.5f, Variance = 0.5f, StandardDeviation = 0.5f, Average = 0.5f, Maximum = 0, Minimum = 0, AverageTemperature = 20.0f, HasAutoPause = false, DataSourceId = 4, AthleteId = 4 },
                new ActivityEntity { IdActivity = 7, Type = "Yoga", Date = new DateOnly(2024, 02, 22), StartTime = new TimeOnly(22, 00, 34), EndTime = new TimeOnly(23, 50, 58), EffortFelt = 5, Variability = 0.5f, Variance = 0.5f, StandardDeviation = 0.5f, Average = 0.5f, Maximum = 0, Minimum = 0, AverageTemperature = 20.0f, HasAutoPause = false, DataSourceId = 5, AthleteId = 3 }
            );
        }
    }
}