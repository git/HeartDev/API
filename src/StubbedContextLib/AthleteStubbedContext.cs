﻿//-----------------------------------------------------------------------
// FILENAME: AthleteStubbedContext.cs
// PROJECT: StubbedContextLib
// SOLUTION: HeartTrack
// DATE CREATED: 22/02/2024
// AUTHOR: Antoine PEREDERII
//-----------------------------------------------------------------------

using DbContextLib;
using Entities;
using Microsoft.EntityFrameworkCore;

namespace StubbedContextLib
{
    /// <summary>
    /// Represents the stubbed context for athletes.
    /// </summary>
    public class AthleteStubbedContext : ActivityStubbedContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AthleteStubbedContext"/> class.
        /// </summary>
        public AthleteStubbedContext() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="AthleteStubbedContext"/> class with the specified options.
        /// </summary>
        /// <param name="options">The options for the context.</param>
        public AthleteStubbedContext(DbContextOptions<HeartTrackContext> options) : base(options) { }

        /// <summary>
        /// Configures the model for the athlete stubbed context.
        /// </summary>
        /// <param name="modelBuilder">The model builder instance.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            var picture2 =
                "https://davidalmeida.site/assets/me_avatar.f77af006.png";
            LargeImageEntity picture = new LargeImageEntity { Id = Guid.Parse("{8d121cdc-6787-4738-8edd-9e026ac16b65}"), Base64 = "UklGRtwDAABXRUJQVlA4INADAAAwEACdASoqACoAAMASJZgCdMoSCz655ndU4XXAP2yXIge5neM/Qd6WCfO8evoj2S0A/p7+f0An85cBxlLDgPC8jO/0nsl/13/O8vvzj7Af8s/p3/H4FU6td4MCwq23z1H2uzoKIXaqJniPI/bRMf8qzv0Zp+HE1RCBw5WQ1j/JovdM1FS52+QcaAAA/v/+NxU4DpPk3+xQPW7tcmURSo9vC4qc+XMxNVBzEM5E8actDz98gmwTXgD62e9EmG/ervdd2ovFFSuxYppWl/wtaX3rkn0xrt8qOql/5I2jfLOnCU0kALLcW4F/wTjU10qsxZXW9fxauC6OPVRF28sc94V9ocmoSWy+sf6jW3vYkVOh+gE/RE0L6b2d3oFyHmkRJnfYwG8o3p6fv9pivNF5aopIBzFnjzwb/VqSq3/b+MWKFmjr8T1qe4/fITo2vBWEqDyogV3ZVGnDVi2DbiEFVSUr2eXTNZQ9V/D9QC/+vCR5TGyX9QOVBgtAYtm/ZTIwzPEYB9NrV1NeO1/sAz78u0tW59r0I+SO5Jgm3B9i1toRurzHv9EZJ9yZL8nafb/T1FaoPDkuJfM+iPs0j8xnS7TaU/gEK0wCxeDYRYtJx9j4hUQq7pAu/T2yWy0vjcUHki952ZNbXnXxB8m8pV5x9E1sfLj5MZEgpU2XV8RHrVvWniCjsf6vgxmR7+KtwIbMjahitUGtHet1WdL+8MmdL29iQJC37pDXirir1NibxKKhFYRuJ3xW9O0r9+Vnh8diqbBuXqDbYR/MSoHvscOCm2t95dN5WBdRUoD7YCG/ZHWc7Ypv/x/al4fkB2lZlYhVWHxjaoeF9jEPI0gAN5XsvUI6hbzEzWMsNW/1orkNOnlskalgmpI4B2rm4Gc7LNui+MuMBrpnBvLkbYX9exe9g8tu7wLt7ScOjDcL99oOyR89Mh9L8rd4+43+JQyR6tsIfcPJo6T6FxHf11d/MGayJi+SWct/uhvvua0oOh+zXNIaUzgoBmu1XULjkpuA0Ghzctf30jbY1AOM49qbMZRYS9A+0S1HrHPnwRvpQY/Sj4xKPn0gdpv/+iTbKJb8zkPC4/9af0Jvesa+GDG0/iw3TswenMhqlh7BM9MW5txpeblsByx4WnJ/oHv6cc0dmM7tsV36lYkCTUXEf/0eKlnfivnN0g1g+j/Lk9et/uoa6TFCW0HgwFOIVFumEYdT675PfuTrYO5o8ZrWEIHtv2Ctlrv9J3TrslD/iKEwtipGHtn0Vak8B9wLL+kz+CIQ/VG4KJpXjx88CeCC4XaGitEdjAAA" };
            
            modelBuilder.Entity<LargeImageEntity>().HasData(picture);

            modelBuilder.Entity<AthleteEntity>().HasData(
                new AthleteEntity { Id = 1, UserName = "Doe",ProfilPicture = picture2, ImageId = Guid.Parse("{8d121cdc-6787-4738-8edd-9e026ac16b65}") ,LastName = "Doe", FirstName = "John", Email = "john.doe@example.com", PasswordHash = "password123", Sexe = 'M', Length = 1.80, Weight = 75, DateOfBirth = new DateOnly(1990, 01, 01), IsCoach = true , DataSourceId = 1},
                new AthleteEntity { Id = 2, UserName = "Smith",ProfilPicture = picture2,LastName = "Smith", FirstName = "Jane", Email = "jane.smith@exemple.com", PasswordHash = "secure456", Sexe = 'F', Length = 1.65, Weight = 60, DateOfBirth = new DateOnly(1995, 01, 01), IsCoach = false, DataSourceId = 1 },
                new AthleteEntity { Id = 3, UserName = "Martin",ProfilPicture = picture2,ImageId = Guid.Parse("{8d121cdc-6787-4738-8edd-9e026ac16b65}") ,LastName = "Martin", FirstName = "Paul", Email = "paul.martin@example.com", PasswordHash = "super789", Sexe = 'M', Length = 1.75, Weight = 68, DateOfBirth = new DateOnly(1992, 01, 01), IsCoach = true, DataSourceId = 1},
                new AthleteEntity { Id = 4, UserName = "Brown",ProfilPicture = picture2, ImageId = Guid.Parse("{8d121cdc-6787-4738-8edd-9e026ac16b65}"),LastName = "Brown", FirstName = "Anna", Email = "anna.brown@example.com", PasswordHash = "test000", Sexe = 'F', Length = 1.70, Weight = 58, DateOfBirth = new DateOnly(1993, 01, 01), IsCoach = false, DataSourceId = 2 },
                new AthleteEntity { Id = 5, UserName = "Lee", ProfilPicture = picture2, ImageId = Guid.Parse("{8d121cdc-6787-4738-8edd-9e026ac16b65}"),LastName = "Lee", FirstName = "Bruce", Email = "bruce.lee@example.com", PasswordHash = "hello321", Sexe = 'M', Length = 2.0, Weight = 90, DateOfBirth = new DateOnly(1991, 01, 01), IsCoach = false, DataSourceId = 3 }
            );
        }
    }
}