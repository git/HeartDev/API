//-----------------------------------------------------------------------
// FILENAME: TrainingStubbedContext.cs
// PROJECT: StubbedContextLib
// SOLUTION: HeartTrack
// DATE CREATED: 22/02/2024
// AUTHOR: Antoine PEREDERII
//-----------------------------------------------------------------------

using DbContextLib;
using Entities;
using Microsoft.EntityFrameworkCore;

namespace StubbedContextLib
{
    /// <summary>
    /// Represents the stubbed context for training entities.
    /// </summary>
    public class TrainingStubbedContext : StatisticStubbedContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TrainingStubbedContext"/> class.
        /// </summary>
        public TrainingStubbedContext() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="TrainingStubbedContext"/> class with the specified options.
        /// </summary>
        /// <param name="options">The options for the context.</param>
        public TrainingStubbedContext(DbContextOptions<HeartTrackContext> options) : base(options) { }

        /// <summary>
        /// Configures the model for the training stubbed context.
        /// </summary>
        /// <param name="modelBuilder">The model builder instance.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<TrainingEntity>().HasData(
                new TrainingEntity { IdTraining = 1, Date = new DateOnly(2024, 01, 19), Description = "Running", Latitude = 48.8566f, Longitude = 2.3522f, FeedBack = "Good", CoachId = 1 },
                new TrainingEntity { IdTraining = 2, Date = new DateOnly(2024, 02, 20), Description = "Cycling", Latitude = 48.8566f, Longitude = 2.3522f, CoachId = 5 },
                new TrainingEntity { IdTraining = 3, Date = new DateOnly(2024, 02, 21), Latitude = 48.8566f, Longitude = 2.3522f, FeedBack = "Good", CoachId = 4 },
                new TrainingEntity { IdTraining = 4, Date = new DateOnly(2024, 02, 22), Description = "Running", Latitude = 48.8566f, Longitude = 2.3522f, FeedBack = "Good", CoachId = 3 },
                new TrainingEntity { IdTraining = 5, Date = new DateOnly(2024, 02, 23), Description = "Cycling", Latitude = 48.8566f, Longitude = 2.3522f, CoachId = 1 }
            );
        }
    }
}