//-----------------------------------------------------------------------
// FILENAME: StatisticStubbedContext.cs
// PROJECT: StubbedContextLib
// SOLUTION: HeartTrack
// DATE CREATED: 22/02/2024
// AUTHOR: Antoine PEREDERII
//-----------------------------------------------------------------------

using DbContextLib;
using Entities;
using Microsoft.EntityFrameworkCore;

namespace StubbedContextLib
{
    /// <summary>
    /// Represents the stubbed context for statistic entities.
    /// </summary>
    public class StatisticStubbedContext : NotificationStubbedContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StatisticStubbedContext"/> class.
        /// </summary>
        public StatisticStubbedContext() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="StatisticStubbedContext"/> class with the specified options.
        /// </summary>
        /// <param name="options">The options for the context.</param>
        public StatisticStubbedContext(DbContextOptions<HeartTrackContext> options) : base(options) { }

        /// <summary>
        /// Configures the model for the statistic stubbed context.
        /// </summary>
        /// <param name="modelBuilder">The model builder instance.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<StatisticEntity>().HasData(
                new StatisticEntity { IdStatistic = 1, Weight = 75, AverageHeartRate = 120, MaximumHeartRate = 180, AverageCaloriesBurned = 500, Date = new DateOnly(2021, 12, 12), AthleteId = 1 },
                new StatisticEntity { IdStatistic = 2, Weight = 60, AverageHeartRate = 130, MaximumHeartRate = 190, AverageCaloriesBurned = 600, Date = new DateOnly(2021, 01, 11), AthleteId = 2 },
                new StatisticEntity { IdStatistic = 3, Weight = 68, AverageHeartRate = 125, MaximumHeartRate = 185, AverageCaloriesBurned = 550, Date = new DateOnly(2022, 12, 30), AthleteId = 1 },
                new StatisticEntity { IdStatistic = 4, Weight = 58, AverageHeartRate = 135, MaximumHeartRate = 195, AverageCaloriesBurned = 650, Date = new DateOnly(2023, 02, 20), AthleteId = 3 },
                new StatisticEntity { IdStatistic = 5, Weight = 90, AverageHeartRate = 110, MaximumHeartRate = 170, AverageCaloriesBurned = 450, Date = new DateOnly(2024, 01, 10), AthleteId = 4 }
            );
        }
    }
}