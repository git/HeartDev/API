//-----------------------------------------------------------------------
// FILENAME: NotificationStubbedContext.cs
// PROJECT: StubbedContextLib
// SOLUTION: HeartTrack
// DATE CREATED: 22/02/2024
// AUTHOR: Antoine PEREDERII
//-----------------------------------------------------------------------

using DbContextLib;
using Entities;
using Microsoft.EntityFrameworkCore;

namespace StubbedContextLib
{
    /// <summary>
    /// Represents the stubbed context for notification entities.
    /// </summary>
    public class NotificationStubbedContext : HeartRateStubbedContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationStubbedContext"/> class.
        /// </summary>
        public NotificationStubbedContext() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationStubbedContext"/> class with the specified options.
        /// </summary>
        /// <param name="options">The options for the context.</param>
        public NotificationStubbedContext(DbContextOptions<HeartTrackContext> options) : base(options) { }

        /// <summary>
        /// Configures the model for the notification stubbed context.
        /// </summary>
        /// <param name="modelBuilder">The model builder instance.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<NotificationEntity>().HasData(
                new NotificationEntity { IdNotif = 1, Message = "You have a new activity to check", Date = new DateTime(2023, 12, 25, 13, 00, 40), Statut = true, Urgence = "A", SenderId = 1 },
                new NotificationEntity { IdNotif = 2, Message = "You have a new athlete to check", Date = new DateTime(2023, 12, 26, 13, 10, 40), Statut = false, Urgence = "3", SenderId = 2 },
                new NotificationEntity { IdNotif = 3, Message = "You have a new heart rate to check", Date = new DateTime(2023, 12, 26, 16, 10, 04), Statut = true, Urgence = "2", SenderId = 3 },
                new NotificationEntity { IdNotif = 4, Message = "You have a new data source to check", Date = new DateTime(2024, 01, 12, 09, 30, 50), Statut = false, Urgence = "1", SenderId = 4 },
                new NotificationEntity { IdNotif = 5, Message = "You have a new notification to check", Date = new DateTime(2024, 02, 22, 12, 10, 00), Statut = true, Urgence = "3", SenderId = 5 }
            );
        }
    }
}