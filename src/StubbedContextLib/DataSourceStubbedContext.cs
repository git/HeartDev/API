//-----------------------------------------------------------------------
// FILENAME: DataSourceStubbedContext.cs
// PROJECT: StubbedContextLib
// SOLUTION: HeartTrack
// DATE CREATED: 22/02/2024
// AUTHOR: Antoine PEREDERII
//-----------------------------------------------------------------------

using DbContextLib;
using Entities;
using Microsoft.EntityFrameworkCore;

namespace StubbedContextLib
{
    /// <summary>
    /// Represents the stubbed context for data sources.
    /// </summary>
    public class DataSourceStubbedContext : AthleteStubbedContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataSourceStubbedContext"/> class.
        /// </summary>
        public DataSourceStubbedContext() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataSourceStubbedContext"/> class with the specified options.
        /// </summary>
        /// <param name="options">The options for the context.</param>
        public DataSourceStubbedContext(DbContextOptions<HeartTrackContext> options) : base(options) { }

        /// <summary>
        /// Configures the model for the data source stubbed context.
        /// </summary>
        /// <param name="modelBuilder">The model builder instance.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<DataSourceEntity>().HasData(
                new DataSourceEntity { IdSource = 1, Type = "Smartwatch", Model = "Garmin", Precision = 0.5f },
                new DataSourceEntity { IdSource = 2, Type = "Smartwatch", Model = "Polar", Precision = 0.5f },
                new DataSourceEntity { IdSource = 3, Type = "Smartwatch", Model = "Suunto", Precision = 0.5f },
                new DataSourceEntity { IdSource = 4, Type = "Smartwatch", Model = "Fitbit", Precision = 0.5f },
                new DataSourceEntity { IdSource = 5, Type = "Smartwatch", Model = "Apple Watch", Precision = 0.5f }
            );
        }
    }
}