//-----------------------------------------------------------------------
// FILENAME: FriendshipStubbedContext.cs
// PROJECT: StubbedContextLib
// SOLUTION: HeartTrack
// DATE CREATED: 22/02/2024
// AUTHOR: Antoine PEREDERII
//-----------------------------------------------------------------------

using DbContextLib;
using Entities;
using Microsoft.EntityFrameworkCore;

namespace StubbedContextLib
{
    /// <summary>
    /// Represents the stubbed context for friendship entities.
    /// </summary>
    public class FriendshipStubbedContext : DataSourceStubbedContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FriendshipStubbedContext"/> class.
        /// </summary>
        public FriendshipStubbedContext() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="FriendshipStubbedContext"/> class with the specified options.
        /// </summary>
        /// <param name="options">The options for the context.</param>
        public FriendshipStubbedContext(DbContextOptions<HeartTrackContext> options) : base(options) { }

        /// <summary>
        /// Configures the model for the heart rate stubbed context.
        /// </summary>
        /// <param name="modelBuilder">The model builder instance.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<FriendshipEntity>().HasData(
                new FriendshipEntity { FollowerId = 1, FollowingId = 2 },
                new FriendshipEntity { FollowerId = 1, FollowingId = 3 },
                new FriendshipEntity { FollowerId = 3, FollowingId = 1 }
            );
        }
    }
}