﻿//-----------------------------------------------------------------------
// FILENAME: HeartTrackContextLibraryContext.cs
// PROJECT: DbContextLib
// SOLUTION: FitnessApp
// DATE CREATED: 22/02/2024
// AUTHOR: Antoine PEREDERII
//-----------------------------------------------------------------------

using Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DbContextLib
{
    /// <summary>
    /// Represents the database context for the FitnessApp.
    /// </summary>
    public class HeartTrackContext : IdentityDbContext<AthleteEntity,IdentityRole<int>,int>
    {
        /// <summary>
        /// Gets or sets the set of athletes.
        /// </summary>
        public DbSet<AthleteEntity> AthletesSet { get; set; }

        /// <summary>
        /// Gets or sets the set of activities.
        /// </summary>
        public DbSet<ActivityEntity> ActivitiesSet { get; set; }

        /// <summary>
        /// Gets or sets the set of data sources.
        /// </summary>
        public DbSet<DataSourceEntity> DataSourcesSet { get; set; }

        /// <summary>
        /// Gets or sets the set of heart rates.
        /// </summary>
        public DbSet<HeartRateEntity> HeartRatesSet { get; set; }

        /// <summary>
        /// Gets or sets the set of notifications.
        /// </summary>
        public DbSet<NotificationEntity> NotificationsSet { get; set; }

        /// <summary>
        /// Gets or sets the set of statistics.
        /// </summary>
        public DbSet<StatisticEntity> StatisticsSet { get; set; }

        /// <summary>
        /// Gets or sets the set of trainings.
        /// </summary>
        public DbSet<TrainingEntity> TrainingsSet { get; set; }
        
        /// <summary>
        /// Gets or sets the set of large images.
        /// </summary>
        public DbSet<LargeImageEntity> LargeImages { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="HeartTrackContext"/> class.
        /// </summary>
        public HeartTrackContext() : base()
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="HeartTrackContext"/> class with the specified options.
        /// </summary>
        /// <param name="options">The options for the context.</param>
        public HeartTrackContext(DbContextOptions<HeartTrackContext> options) : base(options)
        { }
        
        public HeartTrackContext(string dbPlatformPath)
            : this(InitPlaformDb(dbPlatformPath))
        {
        }
        
        private static DbContextOptions<HeartTrackContext> InitPlaformDb(string dbPlatformPath)
        {
            var options = new DbContextOptionsBuilder<HeartTrackContext>()
                .UseMySql($"{dbPlatformPath}", new MySqlServerVersion(new Version(10, 11, 1)))
                .Options;
            return options;
        }
       



        /// <summary>
        /// Configures the database options if they are not already configured.
        /// </summary>
        /// <param name="optionsBuilder">The options builder instance.</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                Console.WriteLine("!IsConfigured...");
                optionsBuilder.UseSqlite($"Data Source=uca_HeartTrack.db");
            }
        }

        /// <summary>
        /// Configures the model for the library context.
        /// </summary>
        /// <param name="modelBuilder">The model builder instance.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ActivityEntity>()
                .HasKey(a => a.IdActivity);
            //generation mode (at insertion)
            modelBuilder.Entity<ActivityEntity>()
                        .Property(a => a.IdActivity)
                        .ValueGeneratedOnAdd();

            //primary key of HeartRateEntity
            modelBuilder.Entity<HeartRateEntity>()
                        .HasKey(h => h.IdHeartRate);
            //generation mode (at insertion)
            modelBuilder.Entity<HeartRateEntity>()
                        .Property(h => h.IdHeartRate)
                        .ValueGeneratedOnAdd();

            //primary key of DataSourceEntity
            modelBuilder.Entity<DataSourceEntity>()
                        .HasKey(d => d.IdSource);
            //generation mode (at insertion)
            modelBuilder.Entity<DataSourceEntity>()
                        .Property(d => d.IdSource)
                        .ValueGeneratedOnAdd();

            //primary key of AthleteEntity
            modelBuilder.Entity<AthleteEntity>()
                        .HasKey(at => at.Id);
            //generation mode (at insertion)
            modelBuilder.Entity<AthleteEntity>()
                        .Property(at => at.Id)
                        .ValueGeneratedOnAdd();
            // add image column type
            // modelBuilder.Entity<AthleteEntity>()
            //             .Property(at => at.ProfilPicture)
            //             .HasColumnType("image");


            //primary key of StatisticEntity
            modelBuilder.Entity<StatisticEntity>()
                        .HasKey(s => s.IdStatistic);
            //generation mode (at insertion)
            modelBuilder.Entity<StatisticEntity>()
                        .Property(s => s.IdStatistic)
                        .ValueGeneratedOnAdd();

            //primary key of TrainingEntity
            modelBuilder.Entity<TrainingEntity>()
                        .HasKey(t => t.IdTraining);
            //generation mode (at insertion)
            modelBuilder.Entity<TrainingEntity>()
                        .Property(t => t.IdTraining)
                        .ValueGeneratedOnAdd();
            
            //primary key of NotificationEntity
            modelBuilder.Entity<NotificationEntity>()
                        .HasKey(n => n.IdNotif);
            //generation mode (at insertion)
            modelBuilder.Entity<NotificationEntity>()
                        .Property(n => n.IdNotif)
                        .ValueGeneratedOnAdd();

            modelBuilder.Entity<FriendshipEntity>()
                        .HasKey(f => new { f.FollowerId, f.FollowingId });

            modelBuilder.Entity<FriendshipEntity>()
                        .HasOne(fing => fing.Following)
                        .WithMany(fings => fings.Followers)
                        .HasForeignKey(fing => fing.FollowingId);

            modelBuilder.Entity<FriendshipEntity>()
                        .HasOne(fer => fer.Follower)
                        .WithMany(fers => fers.Followings)
                        .HasForeignKey(fing => fing.FollowerId);

            // !
            // ? Plusieurs questions sur les required ou non, différence difficile à comprendre

            modelBuilder.Entity<AthleteEntity>()
                .HasMany(at => at.TrainingsCoach)
                .WithOne(tc => tc.Coach)
                .HasForeignKey(tc => tc.CoachId);

            modelBuilder.Entity<AthleteEntity>()
                .HasMany(at => at.TrainingsAthlete)
                .WithMany(ta => ta.Athletes);
            
            modelBuilder.Entity<AthleteEntity>()
                    .HasMany(at => at.NotificationsReceived)
                    .WithMany(nr => nr.Receivers);

            modelBuilder.Entity<AthleteEntity>()
                    .HasMany(at => at.NotificationsSent)
                    .WithOne(ns => ns.Sender)
                    .HasForeignKey(ns => ns.SenderId);
                    // required car on veut toujours savoir le receveur et l'envoyeur de la notification meme admin ou systeme

            modelBuilder.Entity<AthleteEntity>()
                .HasMany(at => at.Statistics)
                .WithOne(s => s.Athlete)
                .HasForeignKey(s => s.AthleteId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<AthleteEntity>()
                .HasMany(at => at.Activities)
                .WithOne(a => a.Athlete)
                .HasForeignKey(a => a.AthleteId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<ActivityEntity>()
                .HasMany(a => a.HeartRates)
                .WithOne(h => h.Activity)
                .HasForeignKey(h => h.ActivityId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<DataSourceEntity>()
                .HasMany(d => d.Activities)
                .WithOne(a => a.DataSource)
                .HasForeignKey(a => a.DataSourceId)
                .IsRequired(false);
            
            modelBuilder.Entity<DataSourceEntity>()
                .HasMany(ds => ds.Athletes)
                .WithOne(at => at.DataSource)
                .HasForeignKey(at => at.DataSourceId)
                .IsRequired(false);
            List<IdentityRole<int>> roles =
            [
                new()
                {
                    Id = 1,
                    Name = "Athlete",
                    NormalizedName = "ATHLETE"
                },

                new()
                {
                    Id = 2,
                    Name = "Coach",
                    NormalizedName = "COACH"
                }
            ];
            modelBuilder.Entity<IdentityRole<int>>().HasData(roles);
        }
    }
}