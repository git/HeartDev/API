using Entities;
using Model;
using Shared;

namespace EFMappers;

public static class ActivityMapper
{
    private static GenericMapper<Activity, ActivityEntity> _mapper = new ();
    public static void Reset()
    {
        _mapper.Reset();
    }

    public static Activity ToModel(this ActivityEntity entity)
    {
        Func<ActivityEntity, Activity> create = activityEntity => new Activity
        {
            Id = activityEntity.IdActivity,
            Type = activityEntity.Type,
            Date = activityEntity.Date.ToDateTime(TimeOnly.MinValue),
            StartTime = activityEntity.Date.ToDateTime(activityEntity.StartTime),
            EndTime = activityEntity.Date.ToDateTime(activityEntity.EndTime),
            Effort = activityEntity.EffortFelt,
            Variability = activityEntity.Variability,
            Variance = activityEntity.Variance,
            StandardDeviation = activityEntity.StandardDeviation,
            Average = activityEntity.Average,
            Maximum = activityEntity.Maximum,
            Minimum = activityEntity.Minimum,
            AverageTemperature = activityEntity.AverageTemperature,
            HasAutoPause = activityEntity.HasAutoPause
        };
        
        Action<ActivityEntity, Activity> link = (activityEntity, model) =>
        {
            model.HeartRates.AddRange(activityEntity.HeartRates?.ToModels());
            model.DataSource = activityEntity.DataSource.ToModel();
            model.Athlete = activityEntity.Athlete.ToModel();
        };
        return entity.ToT(_mapper, create, link);
    }


    public static ActivityEntity ToEntity(this Activity model)
    {
        Func<Activity, ActivityEntity> create = activity => new ActivityEntity
        {
            IdActivity = activity.Id,
            Type = activity.Type,
            Date = DateOnly.FromDateTime(activity.Date),
            StartTime = TimeOnly.FromDateTime(activity.StartTime),
            EndTime = TimeOnly.FromDateTime(activity.EndTime),
            EffortFelt = activity.Effort,
            Variability = activity.Variability,
            Variance = activity.Variance,
            StandardDeviation = activity.StandardDeviation,
            Average = activity.Average,
            Maximum = activity.Maximum,
            Minimum = activity.Minimum,
            AverageTemperature = activity.AverageTemperature,
            HasAutoPause = activity.HasAutoPause
        };
        Action<Activity, ActivityEntity> link = (activity, entity) =>
        {
            entity.HeartRates = activity.HeartRates.ToEntities().ToList();
            entity.DataSource = activity.DataSource != null ? activity.DataSource.ToEntity() : null;
            entity.Athlete = activity.Athlete.ToEntity();
        };
        return model.ToU(_mapper, create, link);
    }

    public static IEnumerable<Activity> ToModels(this IEnumerable<ActivityEntity> entities)
        => entities.Select(a => a.ToModel());

    public static IEnumerable<ActivityEntity> ToEntities(this IEnumerable<Activity> models)
        => models.Select(a => a.ToEntity());
}