using Entities;
using Model;
using Shared;

namespace EFMappers;

public static class DataSourceMapper
{
    
    private static GenericMapper<DataSource, DataSourceEntity> _mapper = new ();

    public static DataSource ToModel(this DataSourceEntity entity)
    {
        Func<DataSourceEntity, DataSource> create = dataSourceEntity => 
            new DataSource( dataSourceEntity.IdSource, dataSourceEntity.Type, dataSourceEntity.Model, dataSourceEntity.Precision, dataSourceEntity.Athletes.ToModels().ToList(), dataSourceEntity.Activities.ToModels().ToList());
        /*
        Action<DataSourceEntity, DataSource> link = (dataSourceEntity, model) =>
        {
            model.Activities.AddRange(dataSourceEntity.Activities.ToModels());
            model.Athletes.AddRange(dataSourceEntity.Athletes.ToModels());
        };*/
        
        return entity.ToT(_mapper, create);
    }

    public static DataSourceEntity ToEntity(this DataSource model)
    {
        Func<DataSource, DataSourceEntity> create = dataSource => 
            new DataSourceEntity
            {
                IdSource = dataSource.Id,
                Type = dataSource.Type,
                Model = dataSource.Model,
                Precision = dataSource.Precision
            };
        
        Action<DataSource, DataSourceEntity> link = (dataSource, entity) =>
        {
            entity.Activities = dataSource.Activities.ToEntities().ToList();
            entity.Athletes = dataSource.Athletes.ToEntities().ToList();
        };
        
        return model.ToU(_mapper, create, link);
    }

    public static IEnumerable<DataSource> ToModels(this IEnumerable<DataSourceEntity> entities)
    => entities.Select(e => e.ToModel());
    
    public static IEnumerable<DataSourceEntity> ToEntities(this IEnumerable<DataSource> models)
    => models.Select(m => m.ToEntity());
}