﻿using System.Buffers;
using Dto;
using Entities;
using Model;
using Shared;

namespace EFMappers;
public static class UserMappeur
{
    private static GenericMapper<User, AthleteEntity> _mapper = new ();
    
    public static void Reset()
    {
        _mapper.Reset();
    }
    
    public static User ToModel(this AthleteEntity entity)
    {
        Func<AthleteEntity, User> create = athleteEntity => new User
        {
            Id = athleteEntity.Id,
            FirstName = athleteEntity.FirstName,
            LastName = athleteEntity.LastName,
            Email = athleteEntity.Email,
            MotDePasse = athleteEntity.PasswordHash,
            DateOfBirth = athleteEntity.DateOfBirth.ToDateTime(TimeOnly.MinValue),
            Sexe = athleteEntity.Sexe,
            Username = athleteEntity.UserName,
            Weight = athleteEntity.Weight,
            Lenght = (float)athleteEntity.Length,
            ProfilePicture = athleteEntity.ProfilPicture,
            // Role = athleteEntity.IsCoach ?  new Coach() : new Athlete(),
        };
        
        Action<AthleteEntity, User> link = (athleteEntity, model) =>
        {
            model.Role = athleteEntity.IsCoach ?  new Coach() : new Athlete();
            if (athleteEntity.DataSource != null) model.DataSources.Add(athleteEntity.DataSource.ToModel());
            if (athleteEntity.Activities != null) model.Activities.AddRange(athleteEntity.Activities.ToModels());
            //model.Image = athleteEntity.Image.ToModel();
        };
        
        return entity.ToT(_mapper, create, link);
    }

    public static AthleteEntity ToEntity(this User model)
    {
        Func<User, AthleteEntity> create = user => new AthleteEntity
        {
            Id = user.Id,
            UserName = user.Username,
            Sexe = user.Sexe,
            FirstName = user.FirstName,
            LastName = user.LastName,
            Email = user.Email,
            PasswordHash = user.MotDePasse,
            DateOfBirth = DateOnly.FromDateTime(user.DateOfBirth),
            IsCoach = user.Role is Coach,
            Weight = user.Weight,
            Length = user.Lenght,
            ProfilPicture = user.ProfilePicture,
        };
        
        Action<User, AthleteEntity> link = (user, entity) =>
        {
            entity.DataSource = user.DataSources.ToEntities().First();
            entity.Activities = user.Activities.ToEntities().ToList();
            entity.IsCoach = user.Role is Coach;
            entity.Image = user.Image.ToEntity();
        };
        
        return model.ToU(_mapper, create, link);
    }
    
    public static IEnumerable<User> ToModels(this IEnumerable<AthleteEntity> entities)
        => entities.Select(e => e.ToModel());
    
    public static IEnumerable<AthleteEntity> ToEntities(this IEnumerable<User> models)
    => models.Select(m => m.ToEntity());
}