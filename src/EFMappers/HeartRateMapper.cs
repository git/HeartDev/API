using Entities;
using Model;
using Shared;

namespace EFMappers;

public static class HeartRateMapper
{
    private static GenericMapper<HeartRate, HeartRateEntity> _mapper = new ();

    public static HeartRate ToModel(this HeartRateEntity entity)
    {
        Func<HeartRateEntity,HeartRate> create = heartRateEntity => 
            new HeartRate(heartRateEntity.IdHeartRate, heartRateEntity.Bpm, heartRateEntity.Time, heartRateEntity.Activity.ToModel(),heartRateEntity.Latitude, heartRateEntity.Longitude, heartRateEntity.Altitude, heartRateEntity.Cadence, heartRateEntity.Distance, heartRateEntity.Speed, heartRateEntity.Power, heartRateEntity.Temperature);
        
        Action<HeartRateEntity, HeartRate> link = (heartRateEntity, model) =>
        {
            model.Activity = heartRateEntity.Activity.ToModel();
        };
        
        return entity.ToT(_mapper, create, link);
    }

    public static HeartRateEntity ToEntity(this HeartRate model)
    {
        Func<HeartRate, HeartRateEntity> create = heartRate => 
            new HeartRateEntity
            {
                IdHeartRate = heartRate.Id,
                Bpm = heartRate.Bpm,
                Time = heartRate.Timestamp,
                Latitude = heartRate.Latitude,
                Longitude = heartRate.Longitude,
                Altitude = heartRate.Altitude,
                Cadence = heartRate.Cadence??0,
                Distance = heartRate.Distance,
                Speed = heartRate.Speed,
                Power = heartRate.Power,
                Temperature = heartRate.Temperature
            };
        
        Action<HeartRate, HeartRateEntity> link = (heartRate, entity) =>
        {
            entity.Activity = heartRate.Activity.ToEntity();
        };
        
        return model.ToU(_mapper, create, link);
    }
    
    public static IEnumerable<HeartRate> ToModels(this IEnumerable<HeartRateEntity> entities)
        => entities.Select(h => h.ToModel());

    public static IEnumerable<HeartRateEntity> ToEntities(this IEnumerable<HeartRate> models)
        => models.Select(h => h.ToEntity());

}