//-----------------------------------------------------------------------
// FILENAME: StatisticEntity.cs
// PROJECT: Entities
// SOLUTION: HeartTrack
// DATE CREATED: 22/02/2024
// AUTHOR: HeartTeam
//-----------------------------------------------------------------------

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    /// <summary>
    /// Represents a statistic entity in the database.
    /// </summary>
    [Table("Statistic")]
    public class StatisticEntity
    {
        /// <summary>
        /// Gets or sets the unique identifier of the statistic.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdStatistic { get; set; }

        /// <summary>
        /// Gets or sets the weight recorded in the statistic.
        /// </summary>
        public float Weight { get; set; }

        /// <summary>
        /// Gets or sets the average heart rate recorded in the statistic.
        /// </summary>
        public double AverageHeartRate { get; set; }

        /// <summary>
        /// Gets or sets the maximum heart rate recorded in the statistic.
        /// </summary>
        public double MaximumHeartRate { get; set; }

        /// <summary>
        /// Gets or sets the average calories burned recorded in the statistic.
        /// </summary>
        public double AverageCaloriesBurned { get; set; }

        /// <summary>
        /// Gets or sets the date of the statistic.
        /// </summary>
        [Required(ErrorMessage = "Statistic Date is required")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateOnly Date { get; set; }

        public int AthleteId { get; set; }

        public AthleteEntity Athlete { get; set; } = null!;
    }
}