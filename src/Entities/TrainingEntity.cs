//-----------------------------------------------------------------------
// FILENAME: TrainingEntity.cs
// PROJECT: Entities
// SOLUTION: HeartTrack
// DATE CREATED: 22/02/2024
// AUTHOR: HeartTeam
//-----------------------------------------------------------------------

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    /// <summary>
    /// Represents a training entity in the database.
    /// </summary>
    [Table("Training")]
    public class TrainingEntity
    {
        /// <summary>
        /// Gets or sets the unique identifier of the training.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdTraining { get; set; }

        /// <summary>
        /// Gets or sets the date of the training.
        /// </summary>
        [Required(ErrorMessage = "Training Date is required")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateOnly Date { get; set; }

        /// <summary>
        /// Gets or sets the description of the training.
        /// </summary>
        [MaxLength(300)]
        public string? Description { get; set; }

        /// <summary>
        /// Gets or sets the latitude of the training location.
        /// </summary>
        public float Latitude { get; set; }

        /// <summary>
        /// Gets or sets the longitude of the training location.
        /// </summary>
        public float Longitude { get; set; }

        /// <summary>
        /// Gets or sets the feedback for the training.
        /// </summary>
        [MaxLength(300)]
        public string? FeedBack { get; set; }

        public int CoachId { get; set; }

        public AthleteEntity Coach { get; set; } = null!;
        public ICollection<AthleteEntity> Athletes { get; set; } = new List<AthleteEntity>();
    }
}