//-----------------------------------------------------------------------
// FILENAME: ActivityEntity.cs
// PROJECT: Entities
// SOLUTION: HeartTrack
// DATE CREATED: 22/02/2024
// AUTHOR: HeartTeam
//-----------------------------------------------------------------------

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    /// <summary>
    /// Represents an activity entity in the database.
    /// </summary>
    [Table("Activity")]
    public class ActivityEntity
    {
        /// <summary>
        /// Gets or sets the unique identifier of the activity.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdActivity { get; set; }

        /// <summary>
        /// Gets or sets the type of the activity.
        /// </summary>
        [Required]
        [MaxLength(100)]
        public string?  Type { get; set; } = null!;

        /// <summary>
        /// Gets or sets the date of the activity.
        /// </summary>
        [Required(ErrorMessage = "Activity Date is required")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateOnly Date { get; set; }

        /// <summary>
        /// Gets or sets the start time of the activity.
        /// </summary>
        [Required(ErrorMessage = "Start Activity Hour is required")]
        [DisplayFormat(DataFormatString = "{0:HH:mm;ss}", ApplyFormatInEditMode = true)]
        public TimeOnly StartTime { get; set; }

        /// <summary>
        /// Gets or sets the end time of the activity.
        /// </summary>
        [Required(ErrorMessage = "End Activity Hour is required")]
        [DisplayFormat(DataFormatString = "{0:HH:mm;ss}", ApplyFormatInEditMode = true)]
        public TimeOnly EndTime { get; set; }

        /// <summary>
        /// Gets or sets the perceived effort of the activity.
        /// </summary>
        public int EffortFelt { get; set; }

        /// <summary>
        /// Gets or sets the variability of the activity.
        /// </summary>
        public float Variability { get; set; }

        /// <summary>
        /// Gets or sets the variance of the activity.
        /// </summary>
        public float Variance { get; set; }

        /// <summary>
        /// Gets or sets the standard deviation of the activity.
        /// </summary>
        public float StandardDeviation { get; set; }

        /// <summary>
        /// Gets or sets the average of the activity.
        /// </summary>
        public float Average { get; set; }

        /// <summary>
        /// Gets or sets the maximum value of the activity.
        /// </summary>
        public int Maximum { get; set; }

        /// <summary>
        /// Gets or sets the minimum value of the activity.
        /// </summary>
        public int Minimum { get; set; }

        /// <summary>
        /// Gets or sets the average temperature during the activity.
        /// </summary>
        public float AverageTemperature { get; set; }

        /// <summary>
        /// Gets or sets whether the activity has an automatic pause feature.
        /// </summary>
        public bool HasAutoPause { get; set; }

        public ICollection<HeartRateEntity>? HeartRates { get; set; } = new List<HeartRateEntity>();

        public int? DataSourceId { get; set; }

        public DataSourceEntity? DataSource { get; set; } = null!;

        public int AthleteId { get; set; }

        public AthleteEntity Athlete { get; set; }
    }
}