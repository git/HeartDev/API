﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Entities;

public class FriendshipEntity
{
    [ForeignKey("FollowingId")]
    public int FollowingId { get; set; }
    public AthleteEntity Following { get; set; } = null!;
    [ForeignKey("FollowerId")]
    public int FollowerId { get; set; }
    public AthleteEntity Follower { get; set; } = null!;
    public DateTime StartDate { get; set; }
}