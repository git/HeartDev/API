//-----------------------------------------------------------------------
// FILENAME: AthleteEntity.cs
// PROJECT: Entities
// SOLUTION: HeartTrack
// DATE CREATED: 22/02/2024
// AUTHOR: HeartTeam
//-----------------------------------------------------------------------

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace Entities
{
    //dentityRole<int>,int
    /// <summary>
    /// Represents an athlete entity in the database.
    /// </summary>
    [Table("Athlete")]
    public class AthleteEntity : IdentityUser<int>
    {
        /// <summary>
        /// Gets or sets the unique identifier of the athlete.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override int Id { get; set; }

        /// <summary>
        /// Gets or sets the username of the athlete.
        /// </summary>
        [MaxLength(100)]
        [Required(ErrorMessage = "Athlete Username is ")]
        public override string UserName { get; set; }

        /// <summary>
        /// Gets or sets the last name of the athlete.
        /// </summary>
        [MaxLength(100)]
        [Required(ErrorMessage = "Athlete Last Name is ")]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the first name of the athlete.
        /// </summary>
        [MaxLength(150)]
        [Required(ErrorMessage = "Athlete First Name is ")]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the email of the athlete.
        /// </summary>
        [MaxLength(100)]
        [Required(ErrorMessage = "Athlete Email is ")]
        public override string Email { get; set; }

        /// <summary>
        /// Gets or sets the gender of the athlete.
        /// </summary>
        [MaxLength(1)]
        [Required(ErrorMessage = "Athlete Sexe is ")]
        public char Sexe { get; set; }

        /// <summary>
        /// Gets or sets the height of the athlete.
        /// </summary>
        public double Length { get; set; }

        /// <summary>
        /// Gets or sets the weight of the athlete.
        /// </summary>
        public float Weight { get; set; }
/*
        /// <summary>
        /// Gets or sets the password of the athlete.
        /// </summary>
        [Required(ErrorMessage = "Athlete PasswordHash is ")]
        public string PasswordHash { get; set; }*/

        /// <summary>
        /// Gets or sets the date of birth of the athlete.
        /// </summary>
        [Required(ErrorMessage = "Athlete Date of Birth is ")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateOnly DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets whether the athlete is a coach.
        /// </summary>
        public bool IsCoach { get; set; }

        public  string? ProfilPicture { get; set; }

        public LargeImageEntity? Image { get; set; }

        [ForeignKey("Image")]
        public Guid? ImageId { get; set; }

        public ICollection<ActivityEntity> Activities { get; set; } = new List<ActivityEntity>();

        public ICollection<StatisticEntity> Statistics { get; set; } = new List<StatisticEntity>();

        public ICollection<TrainingEntity> TrainingsAthlete { get; set; } = new List<TrainingEntity>();
        public ICollection<TrainingEntity> TrainingsCoach { get; set; } = new List<TrainingEntity>();

        public ICollection<NotificationEntity> NotificationsReceived { get; set; } = new List<NotificationEntity>();
        public ICollection<NotificationEntity> NotificationsSent { get; set; } = new List<NotificationEntity>();

        public int? DataSourceId { get; set; }
        public DataSourceEntity? DataSource { get; set; }

        public ICollection<FriendshipEntity> Followers { get; set; } = [];
        public ICollection<FriendshipEntity> Followings { get; set; } = [];
    }
}