//-----------------------------------------------------------------------
// FILENAME: NotificationEntity.cs
// PROJECT: Entities
// SOLUTION: HeartTrack
// DATE CREATED: 22/02/2024
// AUTHOR: HeartTeam
//-----------------------------------------------------------------------

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    /// <summary>
    /// Represents a notification entity in the database.
    /// </summary>
    [Table("Notification")]
    public class NotificationEntity
    {
        /// <summary>
        /// Gets or sets the unique identifier of the notification.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdNotif { get; set; }

        /// <summary>
        /// Gets or sets the message of the notification.
        /// </summary>
        [MaxLength(100)]
        [Required(ErrorMessage = "Message is required")]
        public string Message { get; set; } = null!;

        /// <summary>
        /// Gets or sets the date of the notification.
        /// </summary>
        [Required(ErrorMessage = "Notification Date is required")]
        [DisplayFormat(DataFormatString = "{0:HH.mm.ss - HH.mm.ss}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        /// <summary>
        /// Gets or sets the status of the notification.
        /// </summary>
        public bool Statut { get; set; }

        /// <summary>
        /// Gets or sets the urgency of the notification.
        /// </summary>
        [MaxLength(100)]
        public string Urgence { get; set; } = null!;

        public int SenderId { get; set; }

        public AthleteEntity Sender { get; set; } = null!;
        public ICollection<AthleteEntity> Receivers { get; set; } = new List<AthleteEntity>();
    }
}