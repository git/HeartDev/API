//-----------------------------------------------------------------------
// FILENAME: HeartRateEntity.cs
// PROJECT: Entities
// SOLUTION: HeartTrack
// DATE CREATED: 22/02/2024
// AUTHOR: HeartTeam
//-----------------------------------------------------------------------

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    /// <summary>
    /// Represents a heart rate entity in the database.
    /// </summary>
    [Table("HeartRate")]
    public class HeartRateEntity
    {
        /// <summary>
        /// Gets or sets the unique identifier of the heart rate entry.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdHeartRate { get; set; }

        /// <summary>
        /// Gets or sets the altitude.
        /// </summary>
        public double? Altitude { get; set; }

        /// <summary>
        /// Gets or sets the time of the heart rate measurement.
        /// </summary>
        [Required(ErrorMessage = "HeartRate Time is required")]
        [DisplayFormat(DataFormatString = "{0:HH:mm;ss}", ApplyFormatInEditMode = true)]
        public TimeOnly Time { get; set; }

        /// <summary>
        /// Gets or sets the temperature.
        /// </summary>
        public double? Temperature { get; set; }

        /// <summary>
        /// Gets or sets the heart rate in beats per minute (bpm).
        /// </summary>
        public int Bpm { get; set; }

        /// <summary>
        /// Gets or sets the longitude.
        /// </summary>
        public double? Longitude { get; set; }

        /// <summary>
        /// Gets or sets the latitude.
        /// </summary>
        public double? Latitude { get; set; }
        
        /// <summary>
        /// Gets or sets the cadence.
        /// </summary>
        public int? Cadence { get; set; }
        
        /// <summary>
        /// Gets or sets the distance.
        /// </summary>
        public double? Distance { get; set; }
        
        /// <summary>
        /// Gets or sets the speed.
        /// </summary>
        public double? Speed { get; set; }
        
        /// <summary>
        /// Gets or sets the power.
        /// </summary>
        public int? Power { get; set; }

        public int ActivityId { get; set; }

        public ActivityEntity Activity { get; set; } = null!;
    }
}