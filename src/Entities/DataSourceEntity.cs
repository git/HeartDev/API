//-----------------------------------------------------------------------
// FILENAME: DataSourceEntity.cs
// PROJECT: Entities
// SOLUTION: HeartTrack
// DATE CREATED: 22/02/2024
// AUTHOR: HeartTeam
//-----------------------------------------------------------------------

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    /// <summary>
    /// Represents a data source entity in the database.
    /// </summary>
    [Table("DataSource")]
    public class DataSourceEntity
    {
        /// <summary>
        /// Gets or sets the unique identifier of the data source.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdSource { get; set; }

        /// <summary>
        /// Gets or sets the type of the data source.
        /// </summary>
        [MaxLength(100)]
        [Required]
        public required string Type { get; set; }

        /// <summary>
        /// Gets or sets the model of the data source.
        /// </summary>
        [MaxLength(100)]
        [Required]
        public required string Model { get; set; }

        /// <summary>
        /// Gets or sets the precision of the data source.
        /// </summary>
        public float Precision { get; set; }

        public ICollection<ActivityEntity> Activities { get; set; } = new List<ActivityEntity>();

        public ICollection<AthleteEntity> Athletes { get; set; } = new List<AthleteEntity>();
    }
}