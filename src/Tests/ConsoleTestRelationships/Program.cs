﻿
using DbContextLib;
using Entities;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using StubbedContextLib;
namespace ConsoleTestRelationships;
class Program
{
    static void Main(string[] args)
    {

        try {
            var options = new DbContextOptionsBuilder<HeartTrackContext>()
                .UseSqlite("Data Source=uca.HeartTrack.db")
                .Options;

            using (HeartTrackContext db = new TrainingStubbedContext(options))
            {
                db.Database.EnsureCreated();
                ActivityTests(db);

                DataSourceTests(db);

                AthleteTests(db);

                FriendshipTests(db);
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Une erreur s'est produite : {ex.Message}");
        }
    }
    static void ActivityTests(HeartTrackContext db)
    {
        Console.WriteLine("Accès à toutes les activités avec leurs fréquences cardiaques :");

        Console.WriteLine("Activités :");
        Console.WriteLine("---------------------------------");

        foreach (var activity in db.ActivitiesSet.Include(a => a.HeartRates))
        {
            Console.WriteLine($"\t{activity.IdActivity} - {activity.Type}, {activity.Date}, {activity.StartTime}, {activity.EndTime}, {activity.EffortFelt}, {activity.Variability}, {activity.Variance}, {activity.StandardDeviation}, {activity.Average}, {activity.Maximum}, {activity.Minimum}, {activity.AverageTemperature}, {activity.HasAutoPause}");

            Console.WriteLine("\t\tFréquences cardiaques :");
            Console.WriteLine("\t\t---------------------------------");

            foreach (var heartRate in activity.HeartRates)
            {
                Console.WriteLine($"\t\t\t{heartRate.IdHeartRate} - {heartRate.Altitude}, {heartRate.Time}, {heartRate.Temperature}, {heartRate.Bpm}, {heartRate.Longitude}, {heartRate.Latitude}");
            }
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à l'activité d'id '2' :");
        Console.WriteLine("---------------------------------");

        foreach (var activity in db.ActivitiesSet.Where(a => a.IdActivity == 2).Include(a => a.HeartRates))
        {
            Console.WriteLine($"\t{activity.IdActivity} - {activity.Type}, {activity.Date}, {activity.StartTime}, {activity.EndTime}, {activity.EffortFelt}, {activity.Variability}, {activity.Variance}, {activity.StandardDeviation}, {activity.Average}, {activity.Maximum}, {activity.Minimum}, {activity.AverageTemperature}, {activity.HasAutoPause}");

            Console.WriteLine("\t\tFréquences cardiaques :");
            Console.WriteLine("\t\t---------------------------------");

            foreach (var heartRate in activity.HeartRates)
            {
                Console.WriteLine($"\t\t\t{heartRate.IdHeartRate} - {heartRate.Altitude}, {heartRate.Time}, {heartRate.Temperature}, {heartRate.Bpm}, {heartRate.Longitude}, {heartRate.Latitude}");
            }
        }
    }

    static void DataSourceTests(HeartTrackContext db)
    {
        Console.WriteLine("Accès à toutes les sources de données avec leurs activités :");

        Console.WriteLine("Sources de données :");
        Console.WriteLine("---------------------------------");

        foreach (var dataSource in db.DataSourcesSet.Include(ds => ds.Activities).Include(ds => ds.Athletes))
        {
            Console.WriteLine($"\t{dataSource.IdSource} - {dataSource.Model}");

            Console.WriteLine("\t\tActivités :");
            Console.WriteLine("\t\t---------------------------------");

            foreach (var activity in dataSource.Activities)
            {
                Console.WriteLine($"\t\t\t{activity.IdActivity} - {activity.Type}, {activity.Date}, {activity.StartTime}, {activity.EndTime}, {activity.EffortFelt}, {activity.Variability}, {activity.Variance}, {activity.StandardDeviation}, {activity.Average}, {activity.Maximum}, {activity.Minimum}, {activity.AverageTemperature}, {activity.HasAutoPause}");
            }

            Console.WriteLine("\t\tAthletes :");
            Console.WriteLine("\t\t---------------------------------");

            foreach (var athlete in dataSource.Athletes)
            {
                Console.WriteLine($"\t\t\t{athlete.Id} - {athlete.FirstName}, {athlete.LastName}, {athlete.DateOfBirth}, {athlete.Sexe}, {athlete.Weight}, {athlete.IsCoach}");
            }
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à la source de données d'id '2' :");
        Console.WriteLine("---------------------------------");

        foreach (var dataSource in db.DataSourcesSet.Where(ds => ds.IdSource == 2).Include(ds => ds.Activities).Include(ds => ds.Athletes))
        {
            Console.WriteLine($"\t{dataSource.IdSource} - {dataSource.Model}");

            Console.WriteLine("\t\tActivités :");
            Console.WriteLine("\t\t---------------------------------");

            foreach (var activity in dataSource.Activities)
            {
                Console.WriteLine($"\t\t\t{activity.IdActivity} - {activity.Type}, {activity.Date}, {activity.StartTime}, {activity.EndTime}, {activity.EffortFelt}, {activity.Variability}, {activity.Variance}, {activity.StandardDeviation}, {activity.Average}, {activity.Maximum}, {activity.Minimum}, {activity.AverageTemperature}, {activity.HasAutoPause}");
            }

            Console.WriteLine("\t\tAthletes :");
            Console.WriteLine("\t\t---------------------------------");

            foreach (var athlete in dataSource.Athletes)
            {
                Console.WriteLine($"\t\t\t{athlete.Id} - {athlete.FirstName}, {athlete.LastName}, {athlete.DateOfBirth}, {athlete.Sexe}, {athlete.Weight}, {athlete.IsCoach}");
            }
        }

        Console.WriteLine("---------------------------------\n");
    }

    static void AthleteTests(HeartTrackContext db)
    {
        Console.WriteLine("Accès à tous les athlètes avec leurs statistiques :");

        Console.WriteLine("Athlètes :");
        Console.WriteLine("---------------------------------");

        foreach (var athlete in db.AthletesSet.Include(a => a.Statistics).Include(a => a.Activities).Include(a => a.TrainingsAthlete).Include(a => a.NotificationsSent).Include(a => a.DataSource).Include(a => a.TrainingsCoach).Include(a => a.NotificationsReceived))
        {
            Console.WriteLine($"\t{athlete.Id} - {athlete.FirstName}, {athlete.LastName}, {athlete.DateOfBirth}, {athlete.Sexe}, {athlete.Weight}, {athlete.IsCoach}");

            Console.WriteLine("\t\tStatistiques :");
            Console.WriteLine("\t\t---------------------------------");

            foreach (var statistic in athlete.Statistics)
            {
                Console.WriteLine($"\t\t\t{statistic.IdStatistic} - {statistic.Date}, {statistic.AverageCaloriesBurned}, {statistic.AverageHeartRate}, {statistic.MaximumHeartRate}");
            }

            Console.WriteLine("\t\tActivités :");
            Console.WriteLine("\t\t---------------------------------");

            foreach (var activity in athlete.Activities)
            {
                Console.WriteLine($"\t\t\t{activity.IdActivity} - {activity.Type}, {activity.Date}, {activity.StartTime}, {activity.EndTime}, {activity.EffortFelt}, {activity.Variability}, {activity.Variance}, {activity.StandardDeviation}, {activity.Average}, {activity.Maximum}, {activity.Minimum}, {activity.AverageTemperature}, {activity.HasAutoPause}");
            }

            Console.WriteLine("\t\tEntraînements :");
            Console.WriteLine("\t\t---------------------------------");

            foreach (var training in athlete.TrainingsAthlete)
            {
                Console.WriteLine($"\t\t\t{training.IdTraining} - {training.Date}, {training.Latitude}, {training.Longitude}, {training.Description}, {training.FeedBack}");
            }

            Console.WriteLine("\t\tEntrainements données :");
            Console.WriteLine("\t\t---------------------------------");

            foreach (var training in athlete.TrainingsCoach)
            {
                Console.WriteLine($"\t\t\t{training.IdTraining} - {training.Date}, {training.Latitude}, {training.Longitude}, {training.Description}, {training.FeedBack}");
            }

            Console.WriteLine("\t\tNotifications reçus :");
            Console.WriteLine("\t\t---------------------------------");

            foreach (var notification in athlete.NotificationsReceived)
            {
                Console.WriteLine($"\t\t\t{notification.IdNotif} - {notification.Date}, {notification.Statut}, {notification.Message}");
            }

            Console.WriteLine("\t\tNotifications données :");
            Console.WriteLine("\t\t---------------------------------");

            foreach (var notification in athlete.NotificationsSent)
            {
                Console.WriteLine($"\t\t\t{notification.IdNotif} - {notification.Date}, {notification.Statut}, {notification.Message}");
            }

            Console.WriteLine("\t\tSources de données :");
            Console.WriteLine("\t\t---------------------------------");

            Console.WriteLine("\t\t\t" + (athlete.DataSource?.Model ?? "Aucune source de données"));
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à l'athlète d'id '2' :");
        Console.WriteLine("---------------------------------");

        foreach (var athlete in db.AthletesSet.Where(a => a.Id == 2).Include(a => a.Statistics).Include(a => a.Activities).Include(a => a.TrainingsAthlete).Include(a => a.NotificationsSent).Include(a => a.DataSource).Include(a => a.TrainingsCoach).Include(a => a.NotificationsReceived))
        {
            Console.WriteLine($"\t{athlete.Id} - {athlete.FirstName}, {athlete.LastName}, {athlete.DateOfBirth}, {athlete.Sexe}, {athlete.Weight}, {athlete.IsCoach}");

            Console.WriteLine("\t\tStatistiques :");
            Console.WriteLine("\t\t---------------------------------");

            foreach (var statistic in athlete.Statistics)
            {
                Console.WriteLine($"\t\t\t{statistic.IdStatistic} - {statistic.Date}, {statistic.AverageCaloriesBurned}, {statistic.AverageHeartRate}, {statistic.MaximumHeartRate}");
            }

            Console.WriteLine("\t\tActivités :");
            Console.WriteLine("\t\t---------------------------------");

            foreach (var activity in athlete.Activities)
            {
                Console.WriteLine($"\t\t\t{activity.IdActivity} - {activity.Type}, {activity.Date}, {activity.StartTime}, {activity.EndTime}, {activity.EffortFelt}, {activity.Variability}, {activity.Variance}, {activity.StandardDeviation}, {activity.Average}, {activity.Maximum}, {activity.Minimum}, {activity.AverageTemperature}, {activity.HasAutoPause}");
            }

            Console.WriteLine("\t\tEntraînements :");
            Console.WriteLine("\t\t---------------------------------");

            foreach (var training in athlete.TrainingsAthlete)
            {
                Console.WriteLine($"\t\t\t{training.IdTraining} - {training.Date}, {training.Latitude}, {training.Longitude}, {training.Description}, {training.FeedBack}");
            }

            Console.WriteLine("\t\tEntrainements données :");
            Console.WriteLine("\t\t---------------------------------");

            foreach (var training in athlete.TrainingsCoach)
            {
                Console.WriteLine($"\t\t\t{training.IdTraining} - {training.Date}, {training.Latitude}, {training.Longitude}, {training.Description}, {training.FeedBack}");
            }

            Console.WriteLine("\t\tNotifications reçus :");
            Console.WriteLine("\t\t---------------------------------");

            foreach (var notification in athlete.NotificationsReceived)
            {
                Console.WriteLine($"\t\t\t{notification.IdNotif} - {notification.Date}, {notification.Statut}, {notification.Message}");
            }

            Console.WriteLine("\t\tNotifications données :");
            Console.WriteLine("\t\t---------------------------------");

            foreach (var notification in athlete.NotificationsSent)
            {
                Console.WriteLine($"\t\t\t{notification.IdNotif} - {notification.Date}, {notification.Statut}, {notification.Message}");
            }
            
            Console.WriteLine("\t\tSources de données :");
            Console.WriteLine("\t\t---------------------------------");

            Console.WriteLine("\t\t\t" + (athlete.DataSource?.Model ?? "Aucune source de données"));
        }

        Console.WriteLine("---------------------------------\n");
    }

    static void FriendshipTests(HeartTrackContext db) 
    {
        Console.WriteLine("Accès à toutes les amitiés :");

        Console.WriteLine("Amitiés :");
        Console.WriteLine("---------------------------------");

        foreach (var athlete in db.AthletesSet.Include(f => f.Followers).Include(f => f.Followings))
        {
            Console.WriteLine($"\t{athlete.Id} - {athlete.FirstName}, {athlete.LastName}, {athlete.DateOfBirth}, {athlete.Sexe}, {athlete.Weight}, {athlete.IsCoach}");

            Console.WriteLine($"");
            Console.WriteLine($"");
            
            Console.WriteLine($"\t\t{athlete.Followers.Aggregate("", (seed, kvp) => $"{seed} [{kvp.FollowerId} ; {kvp.FollowingId} ; {kvp.StartDate.ToString("dd/MM/yyyy hh:mm:ss")}]")}");
        }
    }
}