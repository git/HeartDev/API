using APIMappers;
using Dto;
using Dto.Tiny;
using HeartTrackAPI.Controllers;
using HeartTrackAPI.Request;
using HeartTrackAPI.Responce;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging.Abstractions;
using Model;
using Model.Manager;
using Model.Repository;
using Moq;
using Shared;
using StubAPI;

namespace UnitTestApi.Controllers;

[TestClass]
[TestSubject(typeof(UsersController))]
public class UsersControllerTest
{
    private Mock<IDataManager> _dataManagerMock;
    private IDataManager _dataManager;
    private UsersController _usersController;

    private readonly List<User> _users =
    [
        new User
        {
            Id = 1, Username = "DoeDoe",
            ProfilePicture =
                "https://images.unsplash.com/photo-1682687982134-2ac563b2228b?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
            FirstName = "John", LastName = "Doe",
            Sexe = 'M', Lenght = 180, Weight = 70, DateOfBirth = new DateTime(1990, 1, 1),
            Email = "john.doe@example.com", Role = new Athlete()
        },

        new User
        {
            Id = 2, Username = "SmithSmith",
            ProfilePicture =
                "https://images.unsplash.com/photo-1709507779917-242b560288be?q=80&w=2080&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
            FirstName = "Jane", LastName = "Smith",
            Sexe = 'F', Lenght = 170, Weight = 60, DateOfBirth = new DateTime(1992, 2, 2),
            Email = "athlete2@example.com", Role = new Coach()
        },

        new User
        {
            Id = 3, Username = "Athlete3",
            ProfilePicture =
                "https://plus.unsplash.com/premium_photo-1705091981693-6006f8a20479?q=80&w=1974&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
            FirstName = "First3", LastName = "Last3",
            Sexe = 'M', Lenght = 190, Weight = 80, DateOfBirth = new DateTime(1994, 3, 3), Email = "ath@ex.fr",
            Role = new Athlete()
        }
    ];

   [TestInitialize]
    public void SetUp()
    {
        _dataManagerMock = new Mock<IDataManager>();

        _dataManagerMock.Setup(dm => dm.UserRepo.GetNbItems()).ReturnsAsync(_users.Count);
        _dataManagerMock.Setup(dm =>
            dm.UserRepo.GetUsers(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<AthleteOrderCriteria>(),
                It.IsAny<bool>())).ReturnsAsync(
            (int index, int count, AthleteOrderCriteria criteria, bool descending) =>
                _users.GetItemsWithFilterAndOrdering(c => true, index, count,
                    criteria != AthleteOrderCriteria.None ? criteria : null, descending)
        );

            

        _usersController = new UsersController(new NullLogger<UsersController>(), _dataManagerMock.Object);
    }
/*
    [TestMethod]
    public async Task Get_ReturnsPageResponse_WhenRequestIsValid()
    {
        
        var request = new PageRequest
        {
            Index = 0,
            Count = 3,
            OrderingPropertyName = "Id",
            Descending = false
        };

        // Act
        var result = await _usersController.Get(request);
        Assert.IsInstanceOfType(result.Result, typeof(OkObjectResult));
        var okResult = result.Result as OkObjectResult;
        // Assert
        Assert.IsNotNull(okResult);
        Assert.IsInstanceOfType(okResult.Value, typeof(PageResponse<UserTinyDto>));
        var pageResponse = okResult.Value as PageResponse<UserTinyDto>;
        Assert.IsNotNull(pageResponse); 
        Assert.AreEqual(3, pageResponse.Items.Count());
        Assert.AreEqual(3, pageResponse.Total);
        Assert.AreEqual(0, pageResponse.Index);
        Assert.AreEqual(3, pageResponse.Count);
        Assert.AreEqual(3, pageResponse.Count);
    }

    [DataTestMethod]
    [DataRow(0, 2, "Id", false, 2)]
    [DataRow(1, 1, "Id", false, 1)]
    [DataRow(0, 3, "Id", true, 3)]
    public async Task Get_ReturnsCorrectPaginationAndOrdering(int index, int count, string orderingProperty,
        bool descending, int expectedItemCount)
    {
        
        var request = new PageRequest
        {
            Index = index,
            Count = count,
            OrderingPropertyName = orderingProperty,
            Descending = descending
        };
        // Act
        var result = await _usersController.Get(request);
        Assert.IsInstanceOfType(result.Result, typeof(OkObjectResult));
        var okResult = result.Result as OkObjectResult;
        // Assert
        Assert.IsNotNull(okResult);
        Assert.IsInstanceOfType(okResult.Value, typeof(PageResponse<UserTinyDto>));
        var pageResponse = okResult.Value as PageResponse<UserTinyDto>;
        Assert.IsNotNull(pageResponse);
        Assert.AreEqual(expectedItemCount, pageResponse.Items.Count());
    }

    [TestMethod]
    public async Task Get_ReturnsInternalServerError_OnException()
    {
        _dataManagerMock.Setup(dm =>
                dm.UserRepo.GetUsers(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<AthleteOrderCriteria>(),
                    It.IsAny<bool>()))
            .ThrowsAsync(new Exception("Simulated database failure."));

        var request = new PageRequest { Index = 0, Count = 3 };

        var result = await _usersController.Get(request);

        Assert.IsInstanceOfType(result.Result, typeof(ObjectResult));
        var objectResult = result.Result as ObjectResult;
        Assert.AreEqual(500, objectResult.StatusCode);
    }


    
     [TestMethod]
     public async Task GetById_ReturnsUserDto_WhenRequestIsValid()
     {
         
         var id = 1;
         _dataManagerMock.Setup(dm => dm.UserRepo.GetUserById(id)).ReturnsAsync(List<ResponseUserDto>()
         {
             
         });

         // Act
         var result = await _usersController.GetById(id) ;
         Assert.IsInstanceOfType(result.Result, typeof(OkObjectResult));
         var okResult = result.Result as OkObjectResult;

         // Assert
         Assert.IsNotNull(okResult);
         var resultObject = result.Result as OkObjectResult;
         Assert.IsNotNull(resultObject);
         Assert.IsInstanceOfType(resultObject.Value, typeof(ResponseUserDto));
         var user = resultObject.Value as ResponseUserDto;
         Assert.IsNotNull(user);
         var tmp = _users.First(x => x.Id == id).ToDto();
         Assert.AreEqual(tmp.Id, user.Id);
     }*/
     
     [TestMethod]
     public async Task GetById_ReturnsUserDto_WhenRequestUserDoesNotExist()
     {
         
         var id = 0;
         _dataManagerMock.Setup(dm => dm.UserRepo.GetItemById(id)).ReturnsAsync((User)null!);

         // Act
         var result = await _usersController.GetById(id) ;

         // Assert
         Assert.IsInstanceOfType(result.Result, typeof(NotFoundObjectResult));
     }


     [TestMethod]
     public async Task GetById_Returns404_WhenIdIsInvalid()
     {
         
         var id = -2;

         // Act
         var result = await _usersController.GetById(id);
         
         // Assert
         Assert.IsInstanceOfType(result.Result, typeof(NotFoundObjectResult));
     }


     [TestMethod]
     public async Task Count_ReturnsInt_WhenRequestIsValid()
     {
         // Act
         var result = await _usersController.Count();
         Assert.IsNotNull(result);
         result = result.Result as OkObjectResult;

         // Assert
         Assert.IsNotNull(result);
         Assert.IsInstanceOfType(result.Value, typeof(int));
     }

}