﻿namespace UnitTestsEntities;
using Xunit;
using System.Linq;
using Entities;
using Microsoft.EntityFrameworkCore;
using StubbedContextLib;

public class TrainingEntityTests (DatabaseFixture fixture) : IClassFixture<DatabaseFixture>
{
    [Fact]
    public void Add_Training_Success()
    {
        var coach = new AthleteEntity
        {
            UserName = "coach_user",
            LastName = "Coach",
            FirstName = "User",
            Email = "coach.user@example.com",
            Sexe = 'M',
            Length = 180.0,
            Weight = 75.0f,
            PasswordHash = "coachpassword",
            DateOfBirth = new DateOnly(1985, 5, 15),
            IsCoach = true,
            DataSourceId = 1
        };

        var athlete = new AthleteEntity
        {
            UserName = "athlete_user",
            LastName = "Athlete",
            FirstName = "User",
            Email = "athlete.user@example.com",
            Sexe = 'F',
            Length = 165.0,
            Weight = 60.0f,
            PasswordHash = "athletepassword",
            DateOfBirth = new DateOnly(1990, 3, 20),
            IsCoach = false,
            DataSourceId = 1
        };

        var training = new TrainingEntity
        {
            Date = new DateOnly(2024, 3, 15),
            Description = "Training Description",
            Latitude = 40.12345f,
            Longitude = -74.56789f,
            FeedBack = "Training feedback",
            Coach = coach,
            Athletes = { athlete }
        };

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.AthletesSet.Add(coach);
            context.AthletesSet.Add(athlete);
            context.TrainingsSet.Add(training);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var savedTraining = context.TrainingsSet.FirstOrDefault(t => t.Description == "Training Description" && t.FeedBack == "Training feedback");
            Assert.NotNull(savedTraining);
            Assert.Equal("Training Description", savedTraining.Description);
        }
    }

    [Fact]
    public void Update_Training_Success()
    {
        var coach = new AthleteEntity
        {
            UserName = "coach_user",
            LastName = "Coach",
            FirstName = "User",
            Email = "coach.user@example.com",
            Sexe = 'M',
            Length = 180.0,
            Weight = 75.0f,
            PasswordHash = "coachpassword",
            DateOfBirth = new DateOnly(1985, 5, 15),
            IsCoach = true,
            DataSourceId = 1
        };

        var athlete = new AthleteEntity
        {
            UserName = "athlete_user",
            LastName = "Athlete",
            FirstName = "User",
            Email = "athlete.user@example.com",
            Sexe = 'F',
            Length = 165.0,
            Weight = 60.0f,
            PasswordHash = "athletepassword",
            DateOfBirth = new DateOnly(1990, 3, 20),
            IsCoach = false,
            DataSourceId = 1
        };

        var training = new TrainingEntity
        {
            Date = new DateOnly(2024, 3, 15),
            Description = "Training description",
            Latitude = 40.12345f,
            Longitude = -74.56789f,
            FeedBack = "Training feedback",
            Coach = coach,
            Athletes = { athlete }
        };

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.AthletesSet.Add(coach);
            context.AthletesSet.Add(athlete);
            context.TrainingsSet.Add(training);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var savedTraining = context.TrainingsSet.First();
            savedTraining.Description = "Updated training description";
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var updatedTraining = context.TrainingsSet.First();
            Assert.NotNull(updatedTraining);
            Assert.Equal("Updated training description", updatedTraining.Description);
        }
    }

    [Fact]
    public void Delete_Training_Success()
    {
        var coach = new AthleteEntity
        {
            UserName = "coach_user",
            LastName = "Coach",
            FirstName = "User",
            Email = "coach.user@example.com",
            Sexe = 'M',
            Length = 180.0,
            Weight = 75.0f,
            PasswordHash = "coachpassword",
            DateOfBirth = new DateOnly(1985, 5, 15),
            IsCoach = true,
            DataSourceId = 1
        };

        var athlete = new AthleteEntity
        {
            UserName = "athlete_user",
            LastName = "Athlete",
            FirstName = "User",
            Email = "athlete.user@example.com",
            Sexe = 'F',
            Length = 165.0,
            Weight = 60.0f,
            PasswordHash = "athletepassword",
            DateOfBirth = new DateOnly(1990, 3, 20),
            IsCoach = false,
            DataSourceId = 1
        };

        var training = new TrainingEntity
        {
            Date = new DateOnly(2024, 3, 15),
            Description = "Training description suppression",
            Latitude = 40.12345f,
            Longitude = -74.56789f,
            FeedBack = "Training feedback suppression",
            Coach = coach,
            Athletes = { athlete }
        };

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.AthletesSet.Add(coach);
            context.AthletesSet.Add(athlete);
            context.TrainingsSet.Add(training);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var savedTraining = context.TrainingsSet.FirstOrDefault(t => t.Description == "Training description suppression" && t.FeedBack == "Training feedback suppression");
            context.TrainingsSet.Remove(savedTraining);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var deletedTraining = context.TrainingsSet.FirstOrDefault(t => t.Description == "Training description suppression" && t.FeedBack == "Training feedback suppression");
            Assert.Null(deletedTraining);
        }
    }

    [Fact]
    public void Add_Training_With_All_Properties_Success()
    {
        var coach = new AthleteEntity
        {
            UserName = "coach_user",
            LastName = "Coach",
            FirstName = "User",
            Email = "coach.user@example.com",
            Sexe = 'M',
            Length = 180.0,
            Weight = 75.0f,
            PasswordHash = "coachpassword",
            DateOfBirth = new DateOnly(1985, 5, 15),
            IsCoach = true,
            DataSourceId = 1
        };

        var athlete = new AthleteEntity
        {
            UserName = "athlete_user",
            LastName = "Athlete",
            FirstName = "User",
            Email = "athlete.user@example.com",
            Sexe = 'F',
            Length = 165.0,
            Weight = 60.0f,
            PasswordHash = "athletepassword",
            DateOfBirth = new DateOnly(1990, 3, 20),
            IsCoach = false,
            DataSourceId = 1
        };

        var training = new TrainingEntity
        {
            Date = new DateOnly(2024, 3, 15),
            Description = "Training DescriptionAll",
            Latitude = 40.12345f,
            Longitude = -74.56789f,
            FeedBack = "Training feedbackAll",
            Coach = coach,
            Athletes = { athlete }
        };

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.AthletesSet.Add(coach);
            context.AthletesSet.Add(athlete);
            context.TrainingsSet.Add(training);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var savedTraining = context.TrainingsSet.FirstOrDefault(t => t.Description == "Training DescriptionAll" && t.FeedBack == "Training feedbackAll");
            Assert.NotNull(savedTraining);
            Assert.Equal(new DateOnly(2024, 3, 15), savedTraining.Date);
            Assert.Equal("Training DescriptionAll", savedTraining.Description);
            Assert.Equal(40.12345f, savedTraining.Latitude);
            Assert.Equal(-74.56789f, savedTraining.Longitude);
            Assert.Equal("Training feedbackAll", savedTraining.FeedBack);
        }
    }

    [Fact]
    public void Add_Training_With_Null_Values_Fails()
    {
        var training = new TrainingEntity
        {
            Date = default,
            Description = null,
            Latitude = 45,
            Longitude = 45,
            FeedBack = null,
            Coach = null,
            Athletes = null 
        };

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.TrainingsSet.Add(training);
            Assert.Throws<DbUpdateException>(() => context.SaveChanges());
        }
    }

}
