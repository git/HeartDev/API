﻿namespace UnitTestsEntities;
using Xunit;
using System.Linq;
using Entities;
using Microsoft.EntityFrameworkCore;
using StubbedContextLib;

public class FriendshipEntityTests (DatabaseFixture fixture) : IClassFixture<DatabaseFixture>
{
    [Fact]
    public void Add_Friendship_Success()
    {
        var follower = new AthleteEntity
        {
            UserName = "follower_user1",
            LastName = "Follower",
            FirstName = "User",
            Email = "follower.user@example.com",
            Sexe = 'M',
            Length = 170.0,
            Weight = 70.0f,
            PasswordHash = "followerpassword",
            DateOfBirth = new DateOnly(1990, 1, 1),
            IsCoach = false,
            DataSourceId = 1
        };

        var following = new AthleteEntity
        {
            UserName = "following_user1",
            LastName = "Following",
            FirstName = "User",
            Email = "following.user@example.com",
            Sexe = 'F',
            Length = 165.0,
            Weight = 60.0f,
            PasswordHash = "followingpassword",
            DateOfBirth = new DateOnly(1995, 5, 10),
            IsCoach = false,
            DataSourceId = 2
        };

        var friendship = new FriendshipEntity
        {
            Follower = follower,
            Following = following,
            StartDate = DateTime.Now
        };

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.AthletesSet.Add(follower);
            context.AthletesSet.Add(following);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Attach(friendship); // Attachez l'amitié au contexte
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var savedFriendship = context.AthletesSet.Include(a => a.Followers).Include(a => a.Followings).FirstOrDefault(a => a.UserName == "follower_user1").Followings.FirstOrDefault(f => f.FollowerId == follower.Id && f.FollowingId == following.Id);
            
            Assert.NotNull(savedFriendship);
            if (savedFriendship != null)
            {
                Assert.Equal(follower.Id, savedFriendship.FollowerId);
                Assert.Equal(following.Id, savedFriendship.FollowingId);
            }
        }
    }

    [Fact]
    public void Update_Friendship_Success()
    {
        var follower = new AthleteEntity
        {
            UserName = "follower_user",
            LastName = "Follower",
            FirstName = "User",
            Email = "follower.user@example.com",
            Sexe = 'M',
            Length = 170.0,
            Weight = 70.0f,
            PasswordHash = "followerpassword",
            DateOfBirth = new DateOnly(1990, 1, 1),
            IsCoach = false
        };

        var following = new AthleteEntity
        {
            UserName = "following_user",
            LastName = "Following",
            FirstName = "User",
            Email = "following.user@example.com",
            Sexe = 'F',
            Length = 165.0,
            Weight = 60.0f,
            PasswordHash = "followingpassword",
            DateOfBirth = new DateOnly(1995, 5, 10),
            IsCoach = false
        };

        var thirdAthlete = new AthleteEntity
        {
            UserName = "third_user",
            LastName = "Third",
            FirstName = "User",
            Email = "third.user@example.com",
            Sexe = 'M',
            Length = 180.0,
            Weight = 75.0f,
            PasswordHash = "thirdpassword",
            DateOfBirth = new DateOnly(1988, 3, 15),
            IsCoach = false
        };

        var friendship = new FriendshipEntity
        {
            Follower = follower,
            Following = following,
            StartDate = DateTime.Now
        };/*

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.AthletesSet.Add(follower);
            context.AthletesSet.Add(following);
            context.AthletesSet.Add(thirdAthlete);
            context.Friendships.Add(friendship);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var savedFriendship = context.Friendships.FirstOrDefault();
            savedFriendship.Follower = thirdAthlete; // Update the follower
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var updatedFriendship = context.Friendships.FirstOrDefault();
            Assert.NotNull(updatedFriendship);
            Assert.Equal(thirdAthlete.IdAthlete, updatedFriendship.FollowerId);
        }*/
    }

    [Fact]
    public void Delete_Friendship_Success()
    {
        // Act
        var follower = new AthleteEntity
        {
            UserName = "follower_user",
            LastName = "Follower",
            FirstName = "User",
            Email = "follower.user@example.com",
            Sexe = 'M',
            Length = 170.0,
            Weight = 70.0f,
            PasswordHash = "followerpassword",
            DateOfBirth = new DateOnly(1990, 1, 1),
            IsCoach = false
        };

        var following = new AthleteEntity
        {
            UserName = "following_user",
            LastName = "Following",
            FirstName = "User",
            Email = "following.user@example.com",
            Sexe = 'F',
            Length = 165.0,
            Weight = 60.0f,
            PasswordHash = "followingpassword",
            DateOfBirth = new DateOnly(1995, 5, 10),
            IsCoach = false
        };

   
        /*
        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.AthletesSet.Add(follower);
            context.AthletesSet.Add(following);
            context.SaveChanges();
            var user1 = context.AthletesSet.FirstOrDefault(a => a.IdAthlete == follower.IdAthlete);
            var user2 = context.AthletesSet.FirstOrDefault(a => a.IdAthlete == following.IdAthlete);
            user1.Followers = user2.IdAthlete;
        }
        
        

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var savedFriendship = context.Friendships.FirstOrDefault();
            context.Friendships.Remove(savedFriendship);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var deletedFriendship = context.Friendships.FirstOrDefault();
            Assert.Null(deletedFriendship);
        }*/
    }
}

