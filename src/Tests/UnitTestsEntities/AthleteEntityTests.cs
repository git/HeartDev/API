﻿namespace UnitTestsEntities;
using Xunit;
using System.Linq;
using Entities;
using Microsoft.EntityFrameworkCore;
using StubbedContextLib;

public class AthleteEntityTests (DatabaseFixture fixture) : IClassFixture<DatabaseFixture>
{
    [Fact]
    public void Add_Athlete_Success()
    {
        var athlete = new AthleteEntity
        {
            UserName = "john_doe",
            LastName = "Doe",
            FirstName = "John",
            Email = "john.doe@example.com",
            Sexe = 'M',
            Length = 180.0,
            Weight = 75.5f,
            PasswordHash = "password",
            DateOfBirth = new DateOnly(1990, 1, 1),
            IsCoach = false, 
            ProfilPicture = "profile.jpg",
            DataSourceId = 1
        };

        
        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.AthletesSet.Add(athlete);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var savedAthlete = context.AthletesSet.First(a => a.UserName == "john_doe");
            Assert.NotNull(savedAthlete);
            Assert.Equal("john_doe", savedAthlete.UserName);
        }
    }

    [Fact]
    public void Update_Athlete_Success()
    {
        var athlete = new AthleteEntity
        {
            UserName = "jane_smith",
            LastName = "Smith",
            FirstName = "Jane",
            Email = "jane.smith@example.com",
            Sexe = 'F',
            Length = 165.0,
            Weight = 60.0f,
            PasswordHash = "password123",
            DateOfBirth = new DateOnly(1995, 5, 10),
            IsCoach = false,
            DataSourceId = 2,
            ProfilPicture = "profile.jpg"
        };

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.AthletesSet.Add(athlete);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var savedAthlete = context.AthletesSet.First(a => a.UserName == "jane_smith");
            savedAthlete.UserName = "jane_doe";
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var updatedAthlete = context.AthletesSet.First(a => a.UserName == "jane_doe");
            Assert.NotNull(updatedAthlete);
            Assert.Equal("jane_doe", updatedAthlete.UserName);
            Assert.Equal("Smith", updatedAthlete.LastName);
        }
    }

    [Fact]
    public void Delete_Athlete_Success()
    {
        var athlete = new AthleteEntity
        {
            UserName = "test_user",
            LastName = "Test",
            FirstName = "User",
            Email = "test.user@example.com",
            Sexe = 'M',
            Length = 170.0,
            Weight = 70.0f,
            PasswordHash = "testpassword",
            DateOfBirth = new DateOnly(1985, 10, 20),
            IsCoach = false,
            DataSourceId = 3
        };

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.AthletesSet.Add(athlete);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var savedAthlete = context.AthletesSet.First(a => a.UserName == "test_user");
            context.AthletesSet.Remove(savedAthlete);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var deletedAthlete = context.AthletesSet.FirstOrDefault(a => a.UserName == "test_user");
            Assert.Null(deletedAthlete);
        }
    }

    [Fact]
    public void Add_Athlete_With_All_Properties_Success()
    {
        var athlete = new AthleteEntity
        {
            UserName = "john_doe",
            LastName = "Doe",
            FirstName = "John",
            Email = "john.doe@example.com",
            Sexe = 'M',
            Length = 180.0,
            Weight = 75.5f,
            PasswordHash = "password",
            DateOfBirth = new DateOnly(1990, 1, 1),
            IsCoach = false,
            DataSourceId = 1
        };

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.AthletesSet.Add(athlete);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var savedAthlete = context.AthletesSet.First(a => a.UserName == "john_doe");
            Assert.NotNull(savedAthlete);
            Assert.Equal("john_doe", savedAthlete.UserName);
            Assert.Equal("Doe", savedAthlete.LastName);
            Assert.Equal("John", savedAthlete.FirstName);
            Assert.Equal("john.doe@example.com", savedAthlete.Email);
            Assert.Equal('M', savedAthlete.Sexe);
            Assert.Equal(180.0, savedAthlete.Length);
            Assert.Equal(75.5f, savedAthlete.Weight);
            Assert.Equal("password", savedAthlete.PasswordHash);
            Assert.Equal(new DateOnly(1990, 1, 1), savedAthlete.DateOfBirth);
            Assert.False(savedAthlete.IsCoach);
        }
    }

    [Fact]
    public void Add_Athlete_With_Null_Values_Fails()
    {
        var athlete = new AthleteEntity
        {
            UserName = null,
            LastName = null,
            FirstName = null,
            Email = null,
            Sexe = 'M',
            Length = 0,
            Weight = 0,
            PasswordHash = null,
            DateOfBirth = default,
            IsCoach = false
        };

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.AthletesSet.Add(athlete);
            Assert.Throws<DbUpdateException>(() => context.SaveChanges());
        }
    }
}

