namespace UnitTestsEntities;
using Xunit;
using System.Linq;
using Entities;
using Microsoft.EntityFrameworkCore;
using StubbedContextLib;
using Microsoft.Data.Sqlite;

public class ActivityEntityTests (DatabaseFixture fixture) : IClassFixture<DatabaseFixture>
{
    [Fact]
    public void Add_Activity_Success()
    {

        var activity = new ActivityEntity
        {
            Type = "Running",
            Date = new DateOnly(2024, 3, 15),
            StartTime = new TimeOnly(9, 0),
            EndTime = new TimeOnly(10, 0),
            EffortFelt = 7,
            Variability = 0.5f,
            Variance = 0.2f,
            StandardDeviation = 0.3f,
            Average = 0.4f,
            Maximum = 200,
            Minimum = 100,
            AverageTemperature = 25.5f,
            HasAutoPause = true,
            DataSourceId = 1,
            AthleteId = 1
        };
        
        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.ActivitiesSet.Add(activity);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var savedActivity = context.ActivitiesSet.First(a => a.Type == "Running");
            Assert.NotNull(savedActivity);
            Assert.Equal("Running", savedActivity.Type );
        }
    }

    [Fact]
    public void Update_Activity_Success()
    {
        var activity = new ActivityEntity
        {
            Type = "Running",
            Date = new DateOnly(2024, 3, 15),
            StartTime = new TimeOnly(9, 0),
            EndTime = new TimeOnly(10, 0),
            EffortFelt = 7,
            Variability = 0.5f,
            Variance = 0.2f,
            StandardDeviation = 0.3f,
            Average = 0.4f,
            Maximum = 200,
            Minimum = 100,
            AverageTemperature = 25.5f,
            HasAutoPause = true,
            DataSourceId = 1,
            AthleteId = 1
        };

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.ActivitiesSet.Add(activity);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var savedActivity = context.ActivitiesSet.FirstOrDefault(a => a.Type == "Running" && a.Maximum == 200);
            savedActivity.Type = "Walking";
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var updatedActivity = context.ActivitiesSet.First(a => a.Type == "Walking" && a.Maximum == 200);
            Assert.NotNull(updatedActivity);
            Assert.Equal("Walking", updatedActivity.Type );
            Assert.Equal(7, updatedActivity.EffortFelt );
        }
    }

    [Fact]
    public void Delete_Activity_Success()
    {
        var activity = new ActivityEntity
        {
            Type = "Running",
            Date = new DateOnly(2024, 3, 15),
            StartTime = new TimeOnly(9, 0),
            EndTime = new TimeOnly(10, 0),
            EffortFelt = 7,
            Variability = 0.5f,
            Variance = 0.2f,
            StandardDeviation = 0.3f,
            Average = 0.4f,
            Maximum = 200,
            Minimum = 100,
            AverageTemperature = 25.5f,
            HasAutoPause = true,
            DataSourceId = 1,
            AthleteId = 1
        };

        
        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.ActivitiesSet.Add(activity);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var savedActivity = context.ActivitiesSet.FirstOrDefault(a => a.Type == "Running" && a.EffortFelt == 7);
            context.ActivitiesSet.Remove(savedActivity);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var deletedActivity = context.ActivitiesSet.FirstOrDefault(a => a.Type == "Running" && a.EffortFelt == 7);
            Assert.Null(deletedActivity);
        }
    }

    [Fact]
    public void Add_Activity_With_All_Properties_Success()
    {
        
        var activity = new ActivityEntity
        {
            Type = "Running",
            Date = new DateOnly(2024, 3, 15),
            StartTime = new TimeOnly(9, 0),
            EndTime = new TimeOnly(10, 0),
            EffortFelt = 7,
            Variability = 0.5f,
            Variance = 0.2f,
            StandardDeviation = 0.3f,
            Average = 0.4f,
            Maximum = 200,
            Minimum = 100,
            AverageTemperature = 25.5f,
            HasAutoPause = true,
            DataSourceId = 1,
            AthleteId = 1
        };

        // Act
        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.ActivitiesSet.Add(activity);
            context.SaveChanges();
        }

        // Assert
        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var savedActivity = context.ActivitiesSet.First(a => a.Type == "Running"  && a.Date == new DateOnly(2024, 3, 15));
            Assert.NotNull(savedActivity);
            Assert.Equal("Running", savedActivity.Type);
            Assert.Equal(new DateOnly(2024, 3, 15), savedActivity.Date);
            Assert.Equal(new TimeOnly(9, 0), savedActivity.StartTime);
            Assert.Equal(new TimeOnly(10, 0), savedActivity.EndTime);
            Assert.Equal(7, savedActivity.EffortFelt);
            Assert.Equal(0.5f, savedActivity.Variability);
            Assert.Equal(0.2f, savedActivity.Variance);
            Assert.Equal(0.3f, savedActivity.StandardDeviation);
            Assert.Equal(0.4f, savedActivity.Average);
            Assert.Equal(200, savedActivity.Maximum);
            Assert.Equal(100, savedActivity.Minimum);
            Assert.Equal(25.5f, savedActivity.AverageTemperature);
            Assert.True(savedActivity.HasAutoPause);
            Assert.Equal(1, savedActivity.DataSourceId);
            Assert.Equal(1, savedActivity.AthleteId);
        }
    }

    // Test for error cases, e.g., null values
    [Fact]
    public void Add_Activity_With_Null_Values_Fails()
    {
        var activity = new ActivityEntity
        {
            Type = null,
            Date = default, 
            StartTime = default, 
            EndTime = default,
            EffortFelt = 0, 
            Variability = 0,
            Variance = 0,
            StandardDeviation = 0,
            Average = 0,
            Maximum = 0, 
            Minimum = 0, 
            AverageTemperature = 0,
            HasAutoPause = false,
            DataSourceId = 0, 
            AthleteId = 0 
        };

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.ActivitiesSet.Add(activity);
            Assert.Throws<DbUpdateException>(() => context.SaveChanges());
        }
    }
}

