﻿namespace UnitTestsEntities;
using Xunit;
using System.Linq;
using Entities;
using Microsoft.EntityFrameworkCore;
using StubbedContextLib;

public class StatisticEntityTests (DatabaseFixture fixture) : IClassFixture<DatabaseFixture>
{
    [Fact]
    public void Add_Statistic_Success()
    {
        var athlete = new AthleteEntity
        {
            UserName = "athlete_user",
            LastName = "Athlete",
            FirstName = "User",
            Email = "athlete.user@example.com",
            Sexe = 'M',
            Length = 170.0,
            Weight = 70.0f,
            PasswordHash = "athletepassword",
            DateOfBirth = new DateOnly(1990, 1, 1),
            IsCoach = false,
            DataSourceId = 1
        };

        var statistic = new StatisticEntity
        {
            Weight = 75.0f,
            AverageHeartRate = 150.0,
            MaximumHeartRate = 180.0,
            AverageCaloriesBurned = 500.0,
            Date = new DateOnly(2024, 3, 15),
            Athlete = athlete
        };

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.AthletesSet.Add(athlete);
            context.StatisticsSet.Add(statistic);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var savedStatistic = context.StatisticsSet.FirstOrDefault(s => s.AverageHeartRate == 150.0 && s.MaximumHeartRate == 180.0);
            Assert.NotNull(savedStatistic);
            Assert.Equal(75.0f, savedStatistic.Weight);
        }
    }

    [Fact]
    public void Update_Statistic_Success()
    {
        var athlete = new AthleteEntity
        {
            UserName = "athlete_user",
            LastName = "Athlete",
            FirstName = "User",
            Email = "athlete.user@example.com",
            Sexe = 'M',
            Length = 170.0,
            Weight = 70.0f,
            PasswordHash = "athletepassword",
            DateOfBirth = new DateOnly(1990, 1, 1),
            IsCoach = false,
            DataSourceId = 1
        };

        var statistic = new StatisticEntity
        {
            Weight = 75.0f,
            AverageHeartRate = 150.0,
            MaximumHeartRate = 180.0,
            AverageCaloriesBurned = 500.0,
            Date = new DateOnly(2024, 3, 15),
            Athlete = athlete
        };

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.AthletesSet.Add(athlete);
            context.StatisticsSet.Add(statistic);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var savedStatistic = context.StatisticsSet.First();
            savedStatistic.Weight = 80.0f;
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var updatedStatistic = context.StatisticsSet.First();
            Assert.NotNull(updatedStatistic);
            Assert.Equal(80.0f, updatedStatistic.Weight);
        }
    }

    [Fact]
    public void Delete_Statistic_Success()
    {
        var athlete = new AthleteEntity
        {
            UserName = "athlete_user",
            LastName = "Athlete",
            FirstName = "User",
            Email = "athlete.user@example.com",
            Sexe = 'M',
            Length = 170.0,
            Weight = 70.0f,
            PasswordHash = "athletepassword",
            DateOfBirth = new DateOnly(1990, 1, 1),
            IsCoach = false,
            DataSourceId = 1
        };

        var statistic = new StatisticEntity
        {
            Weight = 85.0f,
            AverageHeartRate = 150.0,
            MaximumHeartRate = 180.0,
            AverageCaloriesBurned = 500.0,
            Date = new DateOnly(2024, 3, 15),
            Athlete = athlete
        };

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.AthletesSet.Add(athlete);
            context.StatisticsSet.Add(statistic);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var savedStatistic = context.StatisticsSet.FirstOrDefault(s => s.Weight == 85.0 );
            context.StatisticsSet.Remove(savedStatistic);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var deletedStatistic = context.StatisticsSet.FirstOrDefault(s => s.Weight == 85.0 );
            Assert.Null(deletedStatistic);
        }
    }

    [Fact]
    public void Add_Statistic_With_All_Properties_Success()
    {
        
        var athlete = new AthleteEntity
        {
            UserName = "athlete_user",
            LastName = "Athlete",
            FirstName = "User",
            Email = "athlete.user@example.com",
            Sexe = 'M',
            Length = 170.0,
            Weight = 70.0f,
            PasswordHash = "athletepassword",
            DateOfBirth = new DateOnly(1990, 1, 1),
            IsCoach = false,
            DataSourceId = 1
        };

        var statistic = new StatisticEntity
        {
            Weight = 75.0f,
            AverageHeartRate = 150.0,
            MaximumHeartRate = 180.0,
            AverageCaloriesBurned = 500.0,
            Date = new DateOnly(2024, 3, 15),
            Athlete = athlete
        };

        // Act
        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.AthletesSet.Add(athlete);
            context.StatisticsSet.Add(statistic);
            context.SaveChanges();
        }

        // Assert
        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var savedStatistic = context.StatisticsSet.FirstOrDefault(s => s.AverageHeartRate == 150.0 && s.MaximumHeartRate == 180.0);
            Assert.NotNull(savedStatistic);
            Assert.Equal(75.0f, savedStatistic.Weight);
            Assert.Equal(150.0, savedStatistic.AverageHeartRate);
            Assert.Equal(180.0, savedStatistic.MaximumHeartRate);
            Assert.Equal(500.0, savedStatistic.AverageCaloriesBurned);
            Assert.Equal(new DateOnly(2024, 3, 15), savedStatistic.Date);
        }
    }
}

