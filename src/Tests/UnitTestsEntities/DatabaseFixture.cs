﻿using DbContextLib;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using StubbedContextLib;

namespace UnitTestsEntities;

public class DatabaseFixture : IDisposable
{
    public readonly SqliteConnection _connection;
    public readonly DbContextOptions<HeartTrackContext> _options;
    public DatabaseFixture()
    {
        _connection = new SqliteConnection("DataSource=:memory:");
        _connection.Open();

        _options = new DbContextOptionsBuilder<HeartTrackContext>()
            .UseSqlite(_connection)
            .Options;

    }
    public void Dispose()
    {
        _connection.Close();
        _connection.Dispose();
    }
}