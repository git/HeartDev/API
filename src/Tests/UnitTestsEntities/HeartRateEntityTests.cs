﻿namespace UnitTestsEntities;
using Xunit;
using System.Linq;
using Entities;
using Microsoft.EntityFrameworkCore;
using StubbedContextLib;

public class HeartRateEntityTests (DatabaseFixture fixture) : IClassFixture<DatabaseFixture>
{
    [Fact]
    public void Add_HeartRate_Success()
    {
        var activity = new ActivityEntity
        {
            Type = "Running",
            Date = new DateOnly(2024, 3, 15),
            StartTime = new TimeOnly(9, 0),
            EndTime = new TimeOnly(10, 0),
            EffortFelt = 7,
            Variability = 0.5f,
            Variance = 0.2f,
            StandardDeviation = 0.3f,
            Average = 0.4f,
            Maximum = 200,
            Minimum = 100,
            AverageTemperature = 25.5f,
            HasAutoPause = true,
            DataSourceId = 1,
            AthleteId = 1
        };

        var heartRateEntry = new HeartRateEntity
        {
            Altitude = 100.0,
            Time = new TimeOnly(9, 30),
            Temperature = 20.0f,
            Bpm = 150,
            Longitude = 45.12345f,
            Latitude = 35.6789f,
            Activity = activity
        };

        
        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.ActivitiesSet.Add(activity);
            context.HeartRatesSet.Add(heartRateEntry);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var savedHeartRate = context.HeartRatesSet.FirstOrDefault(h => h.Altitude == 100.0 && h.Temperature == 20.0f);
            Assert.NotNull(savedHeartRate);
            Assert.Equal(150, savedHeartRate.Bpm);
        }
    }

    [Fact]
    public void Update_HeartRate_Success()
    {
        var activity = new ActivityEntity
        {
            Type = "Running",
            Date = new DateOnly(2024, 3, 15),
            StartTime = new TimeOnly(9, 0),
            EndTime = new TimeOnly(10, 0),
            EffortFelt = 7,
            Variability = 0.5f,
            Variance = 0.2f,
            StandardDeviation = 0.3f,
            Average = 0.4f,
            Maximum = 200,
            Minimum = 100,
            AverageTemperature = 25.5f,
            HasAutoPause = true,
            DataSourceId = 1,
            AthleteId = 1
        };

        var heartRateEntry = new HeartRateEntity
        {
            Altitude = 100.0,
            Time = new TimeOnly(9, 30),
            Temperature = 20.0f,
            Bpm = 150,
            Longitude = 45.12345f,
            Latitude = 35.6789f,
            Activity = activity
        };

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.ActivitiesSet.Add(activity);
            context.HeartRatesSet.Add(heartRateEntry);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var savedHeartRate = context.HeartRatesSet.First();
            savedHeartRate.Bpm = 160;
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var updatedHeartRate = context.HeartRatesSet.First();
            Assert.NotNull(updatedHeartRate);
            Assert.Equal(160, updatedHeartRate.Bpm);
        }
    }

    [Fact]
    public void Delete_HeartRate_Success()
    {
        var activity = new ActivityEntity
        {
            Type = "Run",
            Date = new DateOnly(2024, 3, 15),
            StartTime = new TimeOnly(9, 0),
            EndTime = new TimeOnly(10, 0),
            EffortFelt = 5,
            Variability = 0.5f,
            Variance = 0.2f,
            StandardDeviation = 0.3f,
            Average = 0.4f,
            Maximum = 200,
            Minimum = 100,
            AverageTemperature = 25.5f,
            HasAutoPause = true,
            DataSourceId = 1,
            AthleteId = 1
        };

        var heartRateEntry = new HeartRateEntity
        {
            Altitude = 105.0,
            Time = new TimeOnly(9, 30),
            Temperature = 20.0f,
            Bpm = 150,
            Longitude = 45.12345f,
            Latitude = 35.6789f,
            ActivityId = 1
        };

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.ActivitiesSet.Add(activity);
            context.HeartRatesSet.Add(heartRateEntry);
            context.SaveChanges();
        }
        // Ne veut pas passer, si quelqu'un sait pourquoi ... Erreur : System.ArgumentNullException : Value cannot be null. (Parameter 'entity')
        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var savedHeartRate = context.HeartRatesSet.First(h => h.Altitude == 105.0);
            var savedActivity = context.ActivitiesSet.First(a => a.Type == "Run");
            
            Assert.NotNull(savedHeartRate);
            if (savedHeartRate != null)
            {
                context.HeartRatesSet.Remove(savedHeartRate);
            }
            Assert.NotNull(savedActivity);
            if (savedActivity != null)
            {
                context.ActivitiesSet.Remove(savedActivity);
            }
            
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var deletedHeartRate = context.HeartRatesSet.FirstOrDefault(h => h.Altitude == 105.0);
            Assert.Null(deletedHeartRate);
        }
    }

    [Fact]
    public void Add_HeartRate_With_All_Properties_Success()
    {
        
        var activity = new ActivityEntity
        {
            Type = "Running",
            Date = new DateOnly(2024, 3, 15),
            StartTime = new TimeOnly(9, 0),
            EndTime = new TimeOnly(10, 0),
            EffortFelt = 7,
            Variability = 0.5f,
            Variance = 0.2f,
            StandardDeviation = 0.3f,
            Average = 0.4f,
            Maximum = 200,
            Minimum = 100,
            AverageTemperature = 25.5f,
            HasAutoPause = true,
            DataSourceId = 1,
            AthleteId = 1
        };

        var heartRateEntry = new HeartRateEntity
        {
            Altitude = 100.0,
            Time = new TimeOnly(9, 30),
            Temperature = 20.0f,
            Bpm = 150,
            Longitude = 45.12345f,
            Latitude = 35.6789f,
            Activity = activity
        };

        // Act
        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.ActivitiesSet.Add(activity);
            context.HeartRatesSet.Add(heartRateEntry);
            context.SaveChanges();
        }

        // Assert
        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var savedHeartRate = context.HeartRatesSet.FirstOrDefault(h => h.Altitude == 100.0 && h.Temperature == 20.0f);
            Assert.NotNull(savedHeartRate);
            Assert.Equal(100.0, savedHeartRate.Altitude);
            Assert.Equal(new TimeOnly(9, 30), savedHeartRate.Time);
            Assert.Equal(20.0f, savedHeartRate.Temperature);
            Assert.Equal(150, savedHeartRate.Bpm);
            Assert.Equal(45.12345f, savedHeartRate.Longitude);
            Assert.Equal(35.6789f, savedHeartRate.Latitude);
        }
    }
}

