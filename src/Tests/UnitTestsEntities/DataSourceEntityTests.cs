﻿namespace UnitTestsEntities;
using Xunit;
using System.Linq;
using Entities;
using Microsoft.EntityFrameworkCore;
using StubbedContextLib;

public class DataSourceEntityTests (DatabaseFixture fixture) : IClassFixture<DatabaseFixture>
{
    [Fact]
    public void Add_DataSource_Success()
    {
        var dataSource = new DataSourceEntity
        {
            Type = "GPS",
            Model = "Garmin Forerunner 945",
            Precision = 0.1f
        };

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.DataSourcesSet.Add(dataSource);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var savedDataSource = context.DataSourcesSet.First(d => d.Type == "GPS");
            Assert.NotNull(savedDataSource);
            Assert.Equal("Garmin Forerunner 945", savedDataSource.Model);
        }
    }

    [Fact]
    public void Update_DataSource_Success()
    {
        var dataSource = new DataSourceEntity
        {
            Type = "Heart Rate Monitor",
            Model = "Polar H10",
            Precision = 0.2f
        };

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.DataSourcesSet.Add(dataSource);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var savedDataSource = context.DataSourcesSet.First(d => d.Type == "Heart Rate Monitor");
            savedDataSource.Model = "Polar H9";
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var updatedDataSource = context.DataSourcesSet.First(d => d.Model == "Polar H9");
            Assert.NotNull(updatedDataSource);
            Assert.Equal("Heart Rate Monitor", updatedDataSource.Type);
            Assert.Equal(0.2f, updatedDataSource.Precision);
        }
    }

    [Fact]
    public void Delete_DataSource_Success()
    {
        var dataSource = new DataSourceEntity
        {
            Type = "Smartwatch",
            Model = "Apple Watch Series 6",
            Precision = 0.05f
        };

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.DataSourcesSet.Add(dataSource);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var savedDataSource = context.DataSourcesSet.First(d => d.Type == "Smartwatch" && d.Model == "Apple Watch Series 6");
            context.DataSourcesSet.Remove(savedDataSource);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var deletedDataSource = context.DataSourcesSet.FirstOrDefault(d => d.Type == "Smartwatch" && d.Model == "Apple Watch Series 6");
            Assert.Null(deletedDataSource);
        }
    }

    [Fact]
    public void Add_DataSource_With_All_Properties_Success()
    {
        var dataSource = new DataSourceEntity
        {
            Type = "GPS",
            Model = "Garmin Forerunner 945",
            Precision = 0.1f
        };

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.DataSourcesSet.Add(dataSource);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var savedDataSource = context.DataSourcesSet.First(d => d.Type == "GPS");
            Assert.NotNull(savedDataSource);
            Assert.Equal("GPS", savedDataSource.Type);
            Assert.Equal("Garmin Forerunner 945", savedDataSource.Model);
            Assert.Equal(0.1f, savedDataSource.Precision);
        }
    }

    [Fact]
    public void Add_DataSource_With_Null_Values_Fails()
    {
        var dataSource = new DataSourceEntity
        {
            Type = null,
            Model = null,
            Precision = 0
        };

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.DataSourcesSet.Add(dataSource);
            Assert.Throws<DbUpdateException>(() => context.SaveChanges());
        }
    }
}

