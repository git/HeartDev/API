﻿namespace UnitTestsEntities;
using Xunit;
using System.Linq;
using Entities;
using Microsoft.EntityFrameworkCore;
using StubbedContextLib;

public class NotificationEntityTests (DatabaseFixture fixture) : IClassFixture<DatabaseFixture>
{
    [Fact]
    public void Add_Notification_Success()
    {
        var sender = new AthleteEntity
        {
            UserName = "sender_user",
            LastName = "Sender",
            FirstName = "User",
            Email = "sender.user@example.com",
            Sexe = 'M',
            Length = 170.0,
            Weight = 70.0f,
            PasswordHash = "senderpassword",
            DateOfBirth = new DateOnly(1990, 1, 1),
            IsCoach = false,
            DataSourceId = 1
        };

        var notification = new NotificationEntity
        {
            Message = "Test notification",
            Date = DateTime.Now,
            Statut = false,
            Urgence = "High",
            Sender = sender
        };

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.AthletesSet.Add(sender);
            context.NotificationsSet.Add(notification);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var savedNotification = context.NotificationsSet.FirstOrDefault(n => n.Message == "Test notification" && n.Statut == false && n.Urgence == "High");
            Assert.NotNull(savedNotification);
            Assert.Equal("Test notification", savedNotification.Message);
        }
    }

    [Fact]
    public void Update_Notification_Success()
    {
        var sender = new AthleteEntity
        {
            UserName = "sender_user",
            LastName = "Sender",
            FirstName = "User",
            Email = "sender.user@example.com",
            Sexe = 'M',
            Length = 170.0,
            Weight = 70.0f,
            PasswordHash = "senderpassword",
            DateOfBirth = new DateOnly(1990, 1, 1),
            IsCoach = false,
            DataSourceId = 1
        };

        var notification = new NotificationEntity
        {
            Message = "Test notification",
            Date = DateTime.Now,
            Statut = false,
            Urgence = "High",
            Sender = sender
        };

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.AthletesSet.Add(sender);
            context.NotificationsSet.Add(notification);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var savedNotification = context.NotificationsSet.First();
            savedNotification.Message = "Updated message";
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var updatedNotification = context.NotificationsSet.First();
            Assert.NotNull(updatedNotification);
            Assert.Equal("Updated message", updatedNotification.Message);
        }
    }

    [Fact]
    public void Delete_Notification_Success()
    {
        var sender = new AthleteEntity
        {
            UserName = "sender_user",
            LastName = "Sender",
            FirstName = "User",
            Email = "sender.user@example.com",
            Sexe = 'M',
            Length = 170.0,
            Weight = 70.0f,
            PasswordHash = "senderpassword",
            DateOfBirth = new DateOnly(1990, 1, 1),
            IsCoach = false,
            DataSourceId = 3
        };

        var notification = new NotificationEntity
        {
            Message = "Test notification Suppression",
            Date = DateTime.Now,
            Statut = false,
            Urgence = "High",
            Sender = sender
        };

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.AthletesSet.Add(sender);
            context.NotificationsSet.Add(notification);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var savedNotification = context.NotificationsSet.FirstOrDefault(n => n.Message == "Test notification Suppression");
            context.NotificationsSet.Remove(savedNotification);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var deletedNotification = context.NotificationsSet.FirstOrDefault(n => n.Message == "Test notification Suppression");
            Assert.Null(deletedNotification);
        }
    }

    [Fact]
    public void Add_Notification_With_All_Properties_Success()
    {
        var sender = new AthleteEntity
        {
            UserName = "sender_user",
            LastName = "Sender",
            FirstName = "User",
            Email = "sender.user@example.com",
            Sexe = 'M',
            Length = 170.0,
            Weight = 70.0f,
            PasswordHash = "senderpassword",
            DateOfBirth = new DateOnly(1990, 1, 1),
            IsCoach = false,
            DataSourceId = 3
        };

        var notification = new NotificationEntity
        {
            Message = "Test notification",
            Date = DateTime.Now,
            Statut = false,
            Urgence = "High",
            Sender = sender
        };

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.AthletesSet.Add(sender);
            context.NotificationsSet.Add(notification);
            context.SaveChanges();
        }

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            var savedNotification = context.NotificationsSet.FirstOrDefault(n => n.Message == "Test notification" && n.Statut == false && n.Urgence == "High");
            Assert.NotNull(savedNotification);
            Assert.Equal("Test notification", savedNotification.Message);
            Assert.Equal(DateTime.Today, savedNotification.Date.Date);
            Assert.False(savedNotification.Statut);
            Assert.Equal("High", savedNotification.Urgence);
        }
    }

    [Fact]
    public void Add_Notification_With_Null_Values_Fails()
    {
        var notification = new NotificationEntity
        {
            Message = null,
            Date = default,
            Statut = false,
            Urgence = null,
            Sender = null
        };

        using (var context = new TrainingStubbedContext(fixture._options))
        {
            context.Database.EnsureCreated();
            context.NotificationsSet.Add(notification);
            Assert.Throws<DbUpdateException>(() => context.SaveChanges());
        }
    }
}

