using Xunit;
using Model2Entities;
using Microsoft.EntityFrameworkCore;
using DbContextLib;
using StubbedContextLib;
using System.Linq;
using Microsoft.Data.Sqlite;
using System;
using EFMappers;
using Shared;
using Model;
using Moq;
using Microsoft.Extensions.Logging;
using Entities;
/*
namespace UnitTestsEntities
{
    public class ActivityRepositoryTests : IClassFixture<DatabaseFixture>
    {
        private readonly DatabaseFixture _fixture;

        public ActivityRepositoryTests(DatabaseFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public async Task GetActivities_ReturnsActivities()
        {
            var options = new DbContextOptionsBuilder<HeartTrackContext>()
                .UseSqlite(_fixture._connection)
                .Options;

            using (var context = new HeartTrackContext(options))
            {
                context.Database.EnsureCreated();
            }

            using (var context = new HeartTrackContext(options))
            {
                var repository = new DbDataManager.ActivityRepository(new DbDataManager(context), null);
                var activities = await repository.GetActivities(0, 10, ActivityOrderCriteria.None);

                Assert.NotNull(activities);
                Assert.Equal(10, activities.Count());
            }
        }
        [Fact]
        public async Task GetActivityByIdAsync_ReturnsCorrectActivity_WhenIdExists()
        {
            // Arrange
            var activityId = 1;
            var expectedActivity = new Activity { Id = activityId, Type = "Running" };

            var mockDataManager = new Mock<DbDataManager>();
            mockDataManager.Setup(dm => dm.DbContext.ActivitiesSet.SingleOrDefaultAsync(a => a.IdActivity == activityId))
                           .ReturnsAsync(expectedActivity.ToEntity());

            var loggerMock = new Mock<ILogger<DbDataManager>>();
            var activityRepository = new DbDataManager.ActivityRepository(mockDataManager.Object, loggerMock.Object);

            // Act
            var result = await activityRepository.GetActivityByIdAsync(activityId);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(expectedActivity.Id, result.Id);
            Assert.Equal(expectedActivity.Type, result.Type);
        }

        [Fact]
        public async Task GetActivityByIdAsync_ReturnsNull_WhenIdDoesNotExist()
        {
            // Arrange
            var activityId = 999;

            var mockDataManager = new Mock<DbDataManager>();
            mockDataManager.Setup(dm => dm.DbContext.ActivitiesSet.SingleOrDefaultAsync(a => a.IdActivity == activityId))
                           .ReturnsAsync((ActivityEntity)null);

            var loggerMock = new Mock<ILogger<DbDataManager>>();
            var activityRepository = new DbDataManager.ActivityRepository(mockDataManager.Object, loggerMock.Object);

            // Act
            var result = await activityRepository.GetActivityByIdAsync(activityId);

            // Assert
            Assert.Null(result);
        }

        [Fact]
        public async Task AddActivity_SuccessfullyAddsNewActivity()
        {
            // Arrange
            var newActivity = new Activity { Type = "Walking" };

            var mockDataManager = new Mock<DbDataManager>();    
            mockDataManager.Setup(dm => dm.DbContext.AddItem(It.IsAny<ActivityEntity>()))
                           .ReturnsAsync(newActivity.ToEntity());

            var loggerMock = new Mock<ILogger<DbDataManager>>();
            var activityRepository = new DbDataManager.ActivityRepository(mockDataManager.Object, loggerMock.Object);

            // Act
            var result = await activityRepository.AddActivity(newActivity);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(newActivity.Type, result.Type);
        }

    }
}
*/
