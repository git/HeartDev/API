﻿using DbContextLib;
using StubbedContextLib;
using Entities;
namespace ConsoleTestEntities;

class Program
{
    static void Main(string[] args)
    {

        try {
            using (HeartTrackContext db = new TrainingStubbedContext())
            {
                AthletesTests(db);

                ActivityTests(db);

                DataSourceTests(db);

                HeartRateTests(db);

                NotificationTests(db);

                StatisticTests(db);

                TrainingTests(db);

                AddUpdateDeleteAthlete(db);

                AddUpdateDeleteActivity(db);

                AddUpdateDeleteDataSource(db);

                AddUpdateDeleteHeartRate(db);

                AddUpdateDeleteNotification(db);

                AddUpdateDeleteStatistic(db);

                AddUpdateDeleteTraining(db);
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Une erreur s'est produite : {ex.Message}");
        }
    }
    
    static void AthletesTests(HeartTrackContext db)
    {
        Console.WriteLine("Accès à tous les athletes :");

        // Affichage des athletes
        Console.WriteLine("Athletes :");
        Console.WriteLine("---------------------------------");

        foreach (var athlete in db.AthletesSet)
        {
            Console.WriteLine($"\t{athlete.Id} - {athlete.UserName}, {athlete.LastName}, {athlete.FirstName}, {athlete.Email}, {athlete.Sexe}, {athlete.Length}, {athlete.Weight}, {athlete.DateOfBirth}, {athlete.IsCoach}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à l'athlete d'id '2' :");
        Console.WriteLine("---------------------------------");
        foreach (var athlete in db.AthletesSet.Where(a => a.Id == 2))
        {
            Console.WriteLine($"\t{athlete.Id} - {athlete.UserName}, {athlete.LastName}, {athlete.FirstName}, {athlete.Email}, {athlete.Sexe}, {athlete.Length}, {athlete.Weight}, {athlete.DateOfBirth}, {athlete.IsCoach}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à l'athlete de username 'Doe' :");
        Console.WriteLine("---------------------------------");
        foreach (var athlete in db.AthletesSet.Where(a => a.UserName == "Doe"))
        {
            Console.WriteLine($"\t{athlete.Id} - {athlete.UserName}, {athlete.LastName}, {athlete.FirstName}, {athlete.Email}, {athlete.Sexe}, {athlete.Length}, {athlete.Weight}, {athlete.DateOfBirth}, {athlete.IsCoach}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à l'athlete de sexe 'F' :");
        Console.WriteLine("---------------------------------");
        foreach (var athlete in db.AthletesSet.Where(a => a.Sexe == 'F'))
        {
            Console.WriteLine($"\t{athlete.Id} - {athlete.UserName}, {athlete.LastName}, {athlete.FirstName}, {athlete.Email}, {athlete.Sexe}, {athlete.Length}, {athlete.Weight}, {athlete.DateOfBirth}, {athlete.IsCoach}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à l'athlete de email 'bruce.lee@example.com' :");
        Console.WriteLine("---------------------------------");
        foreach (var athlete in db.AthletesSet.Where(a => a.Email == "bruce.lee@example.com"))
        {
            Console.WriteLine($"\t{athlete.Id} - {athlete.UserName}, {athlete.LastName}, {athlete.FirstName}, {athlete.Email}, {athlete.Sexe}, {athlete.Length}, {athlete.Weight}, {athlete.DateOfBirth}, {athlete.IsCoach}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à l'athlete de poids '90' :");
        Console.WriteLine("---------------------------------");
        foreach (var athlete in db.AthletesSet.Where(a => a.Weight == 90))
        {
            Console.WriteLine($"\t{athlete.Id} - {athlete.UserName}, {athlete.LastName}, {athlete.FirstName}, {athlete.Email}, {athlete.Sexe}, {athlete.Length}, {athlete.Weight}, {athlete.DateOfBirth}, {athlete.IsCoach}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à l'athlete de taille '1.80' :");
        Console.WriteLine("---------------------------------");
        foreach (var athlete in db.AthletesSet.Where(a => a.Length == 1.80))
        {
            Console.WriteLine($"\t{athlete.Id} - {athlete.UserName}, {athlete.LastName}, {athlete.FirstName}, {athlete.Email}, {athlete.Sexe}, {athlete.Length}, {athlete.Weight}, {athlete.DateOfBirth}, {athlete.IsCoach}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à l'athlete de date de naissance '01/01/1990' :");
        Console.WriteLine("---------------------------------");
        foreach (var athlete in db.AthletesSet.Where(a => a.DateOfBirth == new DateOnly(1990, 01, 01)))
        {
            Console.WriteLine($"\t{athlete.Id} - {athlete.UserName}, {athlete.LastName}, {athlete.FirstName}, {athlete.Email}, {athlete.Sexe}, {athlete.Length}, {athlete.Weight}, {athlete.DateOfBirth}, {athlete.IsCoach}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à l'athlete de nom 'Martin' :");
        Console.WriteLine("---------------------------------");
        foreach (var athlete in db.AthletesSet.Where(a => a.LastName == "Martin"))
        {
            Console.WriteLine($"\t{athlete.Id} - {athlete.UserName}, {athlete.LastName}, {athlete.FirstName}, {athlete.Email}, {athlete.Sexe}, {athlete.Length}, {athlete.Weight}, {athlete.DateOfBirth}, {athlete.IsCoach}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à l'athlete de prénom 'Anna' :");
        Console.WriteLine("---------------------------------");
        foreach (var athlete in db.AthletesSet.Where(a => a.FirstName == "Anna"))
        {
            Console.WriteLine($"\t{athlete.Id} - {athlete.UserName}, {athlete.LastName}, {athlete.FirstName}, {athlete.Email}, {athlete.Sexe}, {athlete.Length}, {athlete.Weight}, {athlete.DateOfBirth}, {athlete.IsCoach}");
        }

        Console.WriteLine("---------------------------------\n");
        
        Console.WriteLine("Accès à l'athlete de nom 'Brown' et de prénom 'Anna' :");
        Console.WriteLine("---------------------------------");
        foreach (var athlete in db.AthletesSet.Where(a => a.LastName == "Brown" && a.FirstName == "Anna"))
        {
            Console.WriteLine($"\t{athlete.Id} - {athlete.UserName}, {athlete.LastName}, {athlete.FirstName}, {athlete.Email}, {athlete.Sexe}, {athlete.Length}, {athlete.Weight}, {athlete.DateOfBirth}, {athlete.IsCoach}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès au coachs :");
        Console.WriteLine("---------------------------------");
        foreach (var athlete in db.AthletesSet.Where(a => a.IsCoach == true))
        {
            Console.WriteLine($"\t{athlete.Id} - {athlete.UserName}, {athlete.LastName}, {athlete.FirstName}, {athlete.Email}, {athlete.Sexe}, {athlete.Length}, {athlete.Weight}, {athlete.DateOfBirth}, {athlete.IsCoach}");
        }

        Console.WriteLine("---------------------------------\n");
    }
    
    static void ActivityTests(HeartTrackContext db)
    {
        Console.WriteLine("Accès à toutes les activités :");

        Console.WriteLine("Activités :");
        Console.WriteLine("---------------------------------");

        foreach (var activity in db.ActivitiesSet)
        {
            Console.WriteLine($"\t{activity.IdActivity} - {activity.Type}, {activity.Date}, {activity.StartTime}, {activity.EndTime}, {activity.EffortFelt}, {activity.Variability}, {activity.Variance}, {activity.StandardDeviation}, {activity.Average}, {activity.Maximum}, {activity.Minimum}, {activity.AverageTemperature}, {activity.HasAutoPause}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à l'activité d'id '2' :");
        Console.WriteLine("---------------------------------");

        foreach (var activity in db.ActivitiesSet.Where(a => a.IdActivity == 2))
        {
            Console.WriteLine($"\t{activity.IdActivity} - {activity.Type}, {activity.Date}, {activity.StartTime}, {activity.EndTime}, {activity.EffortFelt}, {activity.Variability}, {activity.Variance}, {activity.StandardDeviation}, {activity.Average}, {activity.Maximum}, {activity.Minimum}, {activity.AverageTemperature}, {activity.HasAutoPause}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à l'activité de type 'Running' :");
        Console.WriteLine("---------------------------------");

        foreach (var activity in db.ActivitiesSet.Where(a => a.Type == "Running"))
        {
            Console.WriteLine($"\t{activity.IdActivity} - {activity.Type}, {activity.Date}, {activity.StartTime}, {activity.EndTime}, {activity.EffortFelt}, {activity.Variability}, {activity.Variance}, {activity.StandardDeviation}, {activity.Average}, {activity.Maximum}, {activity.Minimum}, {activity.AverageTemperature}, {activity.HasAutoPause}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à l'activité de date '10/01/2023' :");
        Console.WriteLine("---------------------------------");

        foreach (var activity in db.ActivitiesSet.Where(a => a.Date == new DateOnly(2023, 01, 10)))
        {
            Console.WriteLine($"\t{activity.IdActivity} - {activity.Type}, {activity.Date}, {activity.StartTime}, {activity.EndTime}, {activity.EffortFelt}, {activity.Variability}, {activity.Variance}, {activity.StandardDeviation}, {activity.Average}, {activity.Maximum}, {activity.Minimum}, {activity.AverageTemperature}, {activity.HasAutoPause}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à l'activité de temps '13:00:34' :");
        Console.WriteLine("---------------------------------");

        foreach (var activity in db.ActivitiesSet.Where(a => a.StartTime == new TimeOnly(13, 00, 34)))
        {
            Console.WriteLine($"\t{activity.IdActivity} - {activity.Type}, {activity.Date}, {activity.StartTime}, {activity.EndTime}, {activity.EffortFelt}, {activity.Variability}, {activity.Variance}, {activity.StandardDeviation}, {activity.Average}, {activity.Maximum}, {activity.Minimum}, {activity.AverageTemperature}, {activity.HasAutoPause}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à l'activité de temps '13:00:34' et de type 'Running' :");
        Console.WriteLine("---------------------------------");

        foreach (var activity in db.ActivitiesSet.Where(a => a.StartTime == new TimeOnly(13, 00, 34) && a.Type == "Running"))
        {
            Console.WriteLine($"\t{activity.IdActivity} - {activity.Type}, {activity.Date}, {activity.StartTime}, {activity.EndTime}, {activity.EffortFelt}, {activity.Variability}, {activity.Variance}, {activity.StandardDeviation}, {activity.Average}, {activity.Maximum}, {activity.Minimum}, {activity.AverageTemperature}, {activity.HasAutoPause}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à l'activité de temps '13:00:34' et de type 'Running' et de date '10/01/2023' :");
        Console.WriteLine("---------------------------------");

        foreach (var activity in db.ActivitiesSet.Where(a => a.StartTime == new TimeOnly(13, 00, 34) && a.Type == "Running" && a.Date == new DateOnly(2023, 01, 10)))
        {
            Console.WriteLine($"\t{activity.IdActivity} - {activity.Type}, {activity.Date}, {activity.StartTime}, {activity.EndTime}, {activity.EffortFelt}, {activity.Variability}, {activity.Variance}, {activity.StandardDeviation}, {activity.Average}, {activity.Maximum}, {activity.Minimum}, {activity.AverageTemperature}, {activity.HasAutoPause}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à l'activité de temps '13:00:34' et de type 'Running' et de date '10/01/2023' et de temps de fin '14:00:22' :");
        Console.WriteLine("---------------------------------");

        foreach (var activity in db.ActivitiesSet.Where(a => a.StartTime == new TimeOnly(13, 00, 34) && a.Type == "Running" && a.Date == new DateOnly(2023, 01, 10) && a.EndTime == new TimeOnly(14, 00, 22)))
        {
            Console.WriteLine($"\t{activity.IdActivity} - {activity.Type}, {activity.Date}, {activity.StartTime}, {activity.EndTime}, {activity.EffortFelt}, {activity.Variability}, {activity.Variance}, {activity.StandardDeviation}, {activity.Average}, {activity.Maximum}, {activity.Minimum}, {activity.AverageTemperature}, {activity.HasAutoPause}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à l'activité de temps '13:00:34' et de type 'Running' et de date '10/01/2023' et de temps de fin '14:00:22' et de ressenti d'effort '5' :");
        Console.WriteLine("---------------------------------");

        foreach (var activity in db.ActivitiesSet.Where(a => a.StartTime == new TimeOnly(13, 00, 34) && a.Type == "Running" && a.Date == new DateOnly(2023, 01, 10) && a.EndTime == new TimeOnly(14, 00, 22) && a.EffortFelt == 5))
        {
            Console.WriteLine($"\t{activity.IdActivity} - {activity.Type}, {activity.Date}, {activity.StartTime}, {activity.EndTime}, {activity.EffortFelt}, {activity.Variability}, {activity.Variance}, {activity.StandardDeviation}, {activity.Average}, {activity.Maximum}, {activity.Minimum}, {activity.AverageTemperature}, {activity.HasAutoPause}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à l'activité de temps '13:00:34' et de type 'Running' et de date '10/01/2023' et de temps de fin '14:00:22' et de ressenti d'effort '5' et de variabilité '0.5' :");
        Console.WriteLine("---------------------------------");

        foreach (var activity in db.ActivitiesSet.Where(a => a.StartTime == new TimeOnly(13, 00, 34) && a.Type == "Running" && a.Date == new DateOnly(2023, 01, 10) && a.EndTime == new TimeOnly(14, 00, 22) && a.EffortFelt == 5 && a.Variability == 0.5F))
        {
            Console.WriteLine($"\t{activity.IdActivity} - {activity.Type}, {activity.Date}, {activity.StartTime}, {activity.EndTime}, {activity.EffortFelt}, {activity.Variability}, {activity.Variance}, {activity.StandardDeviation}, {activity.Average}, {activity.Maximum}, {activity.Minimum}, {activity.AverageTemperature}, {activity.HasAutoPause}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à l'activité de temps '13:00:34' et de type 'Running' et de date '10/01/2023' et de temps de fin '14:00:22' et de ressenti d'effort '5' et de variabilité '0.5' et de variance '0.5' :");
        Console.WriteLine("---------------------------------");

        foreach (var activity in db.ActivitiesSet.Where(a => a.StartTime == new TimeOnly(13, 00, 34) && a.Type == "Running" && a.Date == new DateOnly(2023, 01, 10) && a.EndTime == new TimeOnly(14, 00, 22) && a.EffortFelt == 5 && a.Variability == 0.5F && a.Variance == 0.5F))
        {
            Console.WriteLine($"\t{activity.IdActivity} - {activity.Type}, {activity.Date}, {activity.StartTime}, {activity.EndTime}, {activity.EffortFelt}, {activity.Variability}, {activity.Variance}, {activity.StandardDeviation}, {activity.Average}, {activity.Maximum}, {activity.Minimum}, {activity.AverageTemperature}, {activity.HasAutoPause}");
        }

        Console.WriteLine("---------------------------------\n");
    }

    static void DataSourceTests(HeartTrackContext db)
    {
        Console.WriteLine("Accès à toutes les sources de données :");

        Console.WriteLine("Sources de données :");
        Console.WriteLine("---------------------------------");

        foreach (var dataSource in db.DataSourcesSet)
        {
            Console.WriteLine($"\t{dataSource.IdSource} - {dataSource.Type}, {dataSource.Model}, {dataSource.Precision}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à la source de données d'id '2' :");
        Console.WriteLine("---------------------------------");

        foreach (var dataSource in db.DataSourcesSet.Where(d => d.IdSource == 2))
        {
            Console.WriteLine($"\t{dataSource.IdSource} - {dataSource.Type}, {dataSource.Model}, {dataSource.Precision}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à la source de données de type 'Smartwatch' :");
        Console.WriteLine("---------------------------------");

        foreach (var dataSource in db.DataSourcesSet.Where(d => d.Type == "Smartwatch"))
        {
            Console.WriteLine($"\t{dataSource.IdSource} - {dataSource.Type}, {dataSource.Model}, {dataSource.Precision}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à la source de données de modèle 'Garmin' :");
        Console.WriteLine("---------------------------------");

        foreach (var dataSource in db.DataSourcesSet.Where(d => d.Model == "Garmin"))
        {
            Console.WriteLine($"\t{dataSource.IdSource} - {dataSource.Type}, {dataSource.Model}, {dataSource.Precision}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à la source de données de précision '0.5' :");
        Console.WriteLine("---------------------------------");

        foreach (var dataSource in db.DataSourcesSet.Where(d => d.Precision == 0.5f))
        {
            Console.WriteLine($"\t{dataSource.IdSource} - {dataSource.Type}, {dataSource.Model}, {dataSource.Precision}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à la source de données de type 'Smartwatch' et de modèle 'Garmin' :");
        Console.WriteLine("---------------------------------");

        foreach (var dataSource in db.DataSourcesSet.Where(d => d.Type == "Smartwatch" && d.Model == "Garmin"))
        {
            Console.WriteLine($"\t{dataSource.IdSource} - {dataSource.Type}, {dataSource.Model}, {dataSource.Precision}");
        }

        Console.WriteLine("---------------------------------\n");
    }
    
    static void HeartRateTests(HeartTrackContext db)
    {
        Console.WriteLine("Accès à toutes les fréquences cardiaques :");

        Console.WriteLine("Fréquences cardiaques :");
        Console.WriteLine("---------------------------------");

        foreach (var heartRate in db.HeartRatesSet)
        {
            Console.WriteLine($"\t{heartRate.IdHeartRate} - {heartRate.Altitude}, {heartRate.Time}, {heartRate.Temperature}, {heartRate.Bpm}, {heartRate.Longitude}, {heartRate.Latitude}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à la fréquence cardiaque d'id '2' :");
        Console.WriteLine("---------------------------------");

        foreach (var heartRate in db.HeartRatesSet.Where(h => h.IdHeartRate == 2))
        {
            Console.WriteLine($"\t{heartRate.IdHeartRate} - {heartRate.Altitude}, {heartRate.Time}, {heartRate.Temperature}, {heartRate.Bpm}, {heartRate.Longitude}, {heartRate.Latitude}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à la fréquence cardiaque d'altitude '10' :");
        Console.WriteLine("---------------------------------");

        foreach (var heartRate in db.HeartRatesSet.Where(h => h.Altitude == 10))
        {
            Console.WriteLine($"\t{heartRate.IdHeartRate} - {heartRate.Altitude}, {heartRate.Time}, {heartRate.Temperature}, {heartRate.Bpm}, {heartRate.Longitude}, {heartRate.Latitude}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à la fréquence cardiaque de température '20.5' :");
        Console.WriteLine("---------------------------------");

        foreach (var heartRate in db.HeartRatesSet.Where(h => h.Temperature == 20.5f))
        {
            Console.WriteLine($"\t{heartRate.IdHeartRate} - {heartRate.Altitude}, {heartRate.Time}, {heartRate.Temperature}, {heartRate.Bpm}, {heartRate.Longitude}, {heartRate.Latitude}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à la fréquence cardiaque de bpm '65' :");
        Console.WriteLine("---------------------------------");

        foreach (var heartRate in db.HeartRatesSet.Where(h => h.Bpm == 65))
        {
            Console.WriteLine($"\t{heartRate.IdHeartRate} - {heartRate.Altitude}, {heartRate.Time}, {heartRate.Temperature}, {heartRate.Bpm}, {heartRate.Longitude}, {heartRate.Latitude}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à la fréquence cardiaque de longitude '35' :");
        Console.WriteLine("---------------------------------");

        foreach (var heartRate in db.HeartRatesSet.Where(h => h.Longitude == 35))
        {
            Console.WriteLine($"\t{heartRate.IdHeartRate} - {heartRate.Altitude}, {heartRate.Time}, {heartRate.Temperature}, {heartRate.Bpm}, {heartRate.Longitude}, {heartRate.Latitude}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à la fréquence cardiaque de latitude '66' :");
        Console.WriteLine("---------------------------------");

        foreach (var heartRate in db.HeartRatesSet.Where(h => h.Latitude == 66))
        {
            Console.WriteLine($"\t{heartRate.IdHeartRate} - {heartRate.Altitude}, {heartRate.Time}, {heartRate.Temperature}, {heartRate.Bpm}, {heartRate.Longitude}, {heartRate.Latitude}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à la fréquence cardiaque d'altitude '10' et de température '20.5' :");
        Console.WriteLine("---------------------------------");

        foreach (var heartRate in db.HeartRatesSet.Where(h => h.Altitude == 10 && h.Temperature == 20.5f))
        {
            Console.WriteLine($"\t{heartRate.IdHeartRate} - {heartRate.Altitude}, {heartRate.Time}, {heartRate.Temperature}, {heartRate.Bpm}, {heartRate.Longitude}, {heartRate.Latitude}");
        }

        Console.WriteLine("---------------------------------\n");
    }

    static void NotificationTests(HeartTrackContext db)
    {
        Console.WriteLine("Accès à toutes les notifications :");

        Console.WriteLine("Notifications :");
        Console.WriteLine("---------------------------------");

        foreach (var notification in db.NotificationsSet)
        {
            Console.WriteLine($"\t{notification.IdNotif} - {notification.Message}, {notification.Date}, {notification.Statut}, {notification.Urgence}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à la notification d'id '2' :");
        Console.WriteLine("---------------------------------");

        foreach (var notification in db.NotificationsSet.Where(n => n.IdNotif == 2))
        {
            Console.WriteLine($"\t{notification.IdNotif} - {notification.Message}, {notification.Date}, {notification.Statut}, {notification.Urgence}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à la notification de message 'You have a new activity to check' :");
        Console.WriteLine("---------------------------------");

        foreach (var notification in db.NotificationsSet.Where(n => n.Message == "You have a new activity to check"))
        {
            Console.WriteLine($"\t{notification.IdNotif} - {notification.Message}, {notification.Date}, {notification.Statut}, {notification.Urgence}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à la notification de date '25/12/2023' :");
        Console.WriteLine("---------------------------------");

        foreach (var notification in db.NotificationsSet.Where(n => n.Date == new DateTime(2023, 12, 25, 13, 00, 40)))
        {
            Console.WriteLine($"\t{notification.IdNotif} - {notification.Message}, {notification.Date}, {notification.Statut}, {notification.Urgence}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à la notification de statut 'true' :");
        Console.WriteLine("---------------------------------");

        foreach (var notification in db.NotificationsSet.Where(n => n.Statut == true))
        {
            Console.WriteLine($"\t{notification.IdNotif} - {notification.Message}, {notification.Date}, {notification.Statut}, {notification.Urgence}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à la notification d'urgence 'A' :");
        Console.WriteLine("---------------------------------");

        foreach (var notification in db.NotificationsSet.Where(n => n.Urgence == "A"))
        {
            Console.WriteLine($"\t{notification.IdNotif} - {notification.Message}, {notification.Date}, {notification.Statut}, {notification.Urgence}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à la notification de message 'You have a new activity to check' et de date '25/12/2023' :");
        Console.WriteLine("---------------------------------");

        foreach (var notification in db.NotificationsSet.Where(n => n.Message == "You have a new activity to check" && n.Date == new DateTime(2023, 12, 25, 13, 00, 40)))
        {
            Console.WriteLine($"\t{notification.IdNotif} - {notification.Message}, {notification.Date}, {notification.Statut}, {notification.Urgence}");
        }

        Console.WriteLine("---------------------------------\n");
    }

    static void StatisticTests(HeartTrackContext db)
    {
        Console.WriteLine("Accès à toutes les statistiques :");

        Console.WriteLine("Statistiques :");
        Console.WriteLine("---------------------------------");

        foreach (var statistic in db.StatisticsSet)
        {
            Console.WriteLine($"\t{statistic.IdStatistic} - {statistic.Weight}, {statistic.AverageHeartRate}, {statistic.MaximumHeartRate}, {statistic.AverageCaloriesBurned}, {statistic.Date}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à la statistique d'id '2' :");
        Console.WriteLine("---------------------------------");

        foreach (var statistic in db.StatisticsSet.Where(s => s.IdStatistic == 2))
        {
            Console.WriteLine($"\t{statistic.IdStatistic} - {statistic.Weight}, {statistic.AverageHeartRate}, {statistic.MaximumHeartRate}, {statistic.AverageCaloriesBurned}, {statistic.Date}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à la statistique de poids '60' :");
        Console.WriteLine("---------------------------------");

        foreach (var statistic in db.StatisticsSet.Where(s => s.Weight == 60))
        {
            Console.WriteLine($"\t{statistic.IdStatistic} - {statistic.Weight}, {statistic.AverageHeartRate}, {statistic.MaximumHeartRate}, {statistic.AverageCaloriesBurned}, {statistic.Date}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à la statistique de fréquence cardiaque moyenne '130' :");
        Console.WriteLine("---------------------------------");

        foreach (var statistic in db.StatisticsSet.Where(s => s.AverageHeartRate == 130))
        {
            Console.WriteLine($"\t{statistic.IdStatistic} - {statistic.Weight}, {statistic.AverageHeartRate}, {statistic.MaximumHeartRate}, {statistic.AverageCaloriesBurned}, {statistic.Date}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à la statistique de fréquence cardiaque maximale '190' :");
        Console.WriteLine("---------------------------------");

        foreach (var statistic in db.StatisticsSet.Where(s => s.MaximumHeartRate == 190))
        {
            Console.WriteLine($"\t{statistic.IdStatistic} - {statistic.Weight}, {statistic.AverageHeartRate}, {statistic.MaximumHeartRate}, {statistic.AverageCaloriesBurned}, {statistic.Date}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à la statistique de calories brûlées en moyenne '550' :");
        Console.WriteLine("---------------------------------");

        foreach (var statistic in db.StatisticsSet.Where(s => s.AverageCaloriesBurned == 550))
        {
            Console.WriteLine($"\t{statistic.IdStatistic} - {statistic.Weight}, {statistic.AverageHeartRate}, {statistic.MaximumHeartRate}, {statistic.AverageCaloriesBurned}, {statistic.Date}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à la statistique de date '30/12/2022' :");
        Console.WriteLine("---------------------------------");

        foreach (var statistic in db.StatisticsSet.Where(s => s.Date == new DateOnly(2022, 12, 30)))
        {
            Console.WriteLine($"\t{statistic.IdStatistic} - {statistic.Weight}, {statistic.AverageHeartRate}, {statistic.MaximumHeartRate}, {statistic.AverageCaloriesBurned}, {statistic.Date}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à la statistique de poids '60' et de fréquence cardiaque moyenne '130' :");
        Console.WriteLine("---------------------------------");

        foreach (var statistic in db.StatisticsSet.Where(s => s.Weight == 60 && s.AverageHeartRate == 130))
        {
            Console.WriteLine($"\t{statistic.IdStatistic} - {statistic.Weight}, {statistic.AverageHeartRate}, {statistic.MaximumHeartRate}, {statistic.AverageCaloriesBurned}, {statistic.Date}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à la statistique de poids '60' et de fréquence cardiaque moyenne '130' et de fréquence cardiaque maximale '190' :");
        Console.WriteLine("---------------------------------");

        foreach (var statistic in db.StatisticsSet.Where(s => s.Weight == 60 && s.AverageHeartRate == 130 && s.MaximumHeartRate == 190))
        {
            Console.WriteLine($"\t{statistic.IdStatistic} - {statistic.Weight}, {statistic.AverageHeartRate}, {statistic.MaximumHeartRate}, {statistic.AverageCaloriesBurned}, {statistic.Date}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à la statistique de poids '60' et de fréquence cardiaque moyenne '130' et de fréquence cardiaque maximale '190' et de calories brûlées en moyenne '600' :");
        Console.WriteLine("---------------------------------");

        foreach (var statistic in db.StatisticsSet.Where(s => s.Weight == 60 && s.AverageHeartRate == 130 && s.MaximumHeartRate == 190 && s.AverageCaloriesBurned == 600))
        {
            Console.WriteLine($"\t{statistic.IdStatistic} - {statistic.Weight}, {statistic.AverageHeartRate}, {statistic.MaximumHeartRate}, {statistic.AverageCaloriesBurned}, {statistic.Date}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à la statistique de poids '60' et de fréquence cardiaque moyenne '130' et de fréquence cardiaque maximale '190' et de calories brûlées en moyenne '600' et de date '11/01/2021' :");
        Console.WriteLine("---------------------------------");

        foreach (var statistic in db.StatisticsSet.Where(s => s.Weight == 60 && s.AverageHeartRate == 130 && s.MaximumHeartRate == 190 && s.AverageCaloriesBurned == 600 && s.Date == new DateOnly(2021, 01, 11)))
        {
            Console.WriteLine($"\t{statistic.IdStatistic} - {statistic.Weight}, {statistic.AverageHeartRate}, {statistic.MaximumHeartRate}, {statistic.AverageCaloriesBurned}, {statistic.Date}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à la statistique de poids '60' et de fréquence cardiaque moyenne '130' et de fréquence cardiaque maximale '190' et de calories brûlées en moyenne '600' et de date '11/01/2021' :");
        Console.WriteLine("---------------------------------");

        foreach (var statistic in db.StatisticsSet.Where(s => s.Weight == 60 && s.AverageHeartRate == 130 && s.MaximumHeartRate == 190 && s.AverageCaloriesBurned == 600 && s.Date == new DateOnly(2021, 01, 11)))
        {
            Console.WriteLine($"\t{statistic.IdStatistic} - {statistic.Weight}, {statistic.AverageHeartRate}, {statistic.MaximumHeartRate}, {statistic.AverageCaloriesBurned}, {statistic.Date}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à la statistique de poids '60' et de fréquence cardiaque moyenne '130' et de fréquence cardiaque maximale '190' et de calories brûlées en moyenne '600' et de date '11/01/2021' :");
        Console.WriteLine("---------------------------------");

        foreach (var statistic in db.StatisticsSet.Where(s => s.Weight == 60 && s.AverageHeartRate == 130 && s.MaximumHeartRate == 190 && s.AverageCaloriesBurned == 600 && s.Date == new DateOnly(2021, 01, 11)))
        {
            Console.WriteLine($"\t{statistic.IdStatistic} - {statistic.Weight}, {statistic.AverageHeartRate}, {statistic.MaximumHeartRate}, {statistic.AverageCaloriesBurned}, {statistic.Date}");
        }

        Console.WriteLine("---------------------------------\n");
    }
    static void TrainingTests(HeartTrackContext db)
    {
        Console.WriteLine("Accès à tout les entrainements :");

        Console.WriteLine("Entrainements :");
        Console.WriteLine("---------------------------------");

        foreach (var training in db.TrainingsSet)
        {
            Console.WriteLine($"\t{training.IdTraining} - {training.Date}, {training.Description}, {training.Latitude}, {training.Longitude}, {training.FeedBack}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à l'entrainement d'id '2' :");
        Console.WriteLine("---------------------------------");

        foreach (var training in db.TrainingsSet.Where(t => t.IdTraining == 2))
        {
            Console.WriteLine($"\t{training.IdTraining} - {training.Date}, {training.Description}, {training.Latitude}, {training.Longitude}, {training.FeedBack}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à l'entrainement de date '21/02/2024' :");
        Console.WriteLine("---------------------------------");

        foreach (var training in db.TrainingsSet.Where(t => t.Date == new DateOnly(2024, 02, 21)))
        {
            Console.WriteLine($"\t{training.IdTraining} - {training.Date}, {training.Description}, {training.Latitude}, {training.Longitude}, {training.FeedBack}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à l'entrainement de description 'Running' :");
        Console.WriteLine("---------------------------------");

        foreach (var training in db.TrainingsSet.Where(t => t.Description == "Running"))
        {
            Console.WriteLine($"\t{training.IdTraining} - {training.Date}, {training.Description}, {training.Latitude}, {training.Longitude}, {training.FeedBack}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à l'entrainement de latitude '48.8566f' :");
        Console.WriteLine("---------------------------------");

        foreach (var training in db.TrainingsSet.Where(t => t.Latitude == 48.8566f))
        {
            Console.WriteLine($"\t{training.IdTraining} - {training.Date}, {training.Description}, {training.Latitude}, {training.Longitude}, {training.FeedBack}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à l'entrainement de longitude '2.3522f' :");
        Console.WriteLine("---------------------------------");

        foreach (var training in db.TrainingsSet.Where(t => t.Longitude == 2.3522f))
        {
            Console.WriteLine($"\t{training.IdTraining} - {training.Date}, {training.Description}, {training.Latitude}, {training.Longitude}, {training.FeedBack}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à l'entrainement de feedback 'Good' :");
        Console.WriteLine("---------------------------------");

        foreach (var training in db.TrainingsSet.Where(t => t.FeedBack == "Good"))
        {
            Console.WriteLine($"\t{training.IdTraining} - {training.Date}, {training.Description}, {training.Latitude}, {training.Longitude}, {training.FeedBack}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à l'entrainement de date '20/02/2024' et de description 'Cycling' :");
        Console.WriteLine("---------------------------------");

        foreach (var training in db.TrainingsSet.Where(t => t.Date == new DateOnly(2024, 02, 20) && t.Description == "Cycling"))
        {
            Console.WriteLine($"\t{training.IdTraining} - {training.Date}, {training.Description}, {training.Latitude}, {training.Longitude}, {training.FeedBack}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à l'entrainement de date '22/02/2024' et de description 'Running' et de latitude '48.8566f' :");
        Console.WriteLine("---------------------------------");

        foreach (var training in db.TrainingsSet.Where(t => t.Date == new DateOnly(2024, 02, 22) && t.Description == "Running" && t.Latitude == 48.8566f))
        {
            Console.WriteLine($"\t{training.IdTraining} - {training.Date}, {training.Description}, {training.Latitude}, {training.Longitude}, {training.FeedBack}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à l'entrainement de date '23/02/2024' et de description 'Cycling' et de latitude '48.8566f' et de longitude '2.3522f' :");
        Console.WriteLine("---------------------------------");

        foreach (var training in db.TrainingsSet.Where(t => t.Date == new DateOnly(2024, 02, 23) && t.Description == "Cycling" && t.Latitude == 48.8566f && t.Longitude == 2.3522f))
        {
            Console.WriteLine($"\t{training.IdTraining} - {training.Date}, {training.Description}, {training.Latitude}, {training.Longitude}, {training.FeedBack}");
        }

        Console.WriteLine("---------------------------------\n");

        Console.WriteLine("Accès à l'entrainement de date '19/01/2024' et de description 'Running' et de latitude '48.8566f' et de longitude '2.3522f' et de feedback 'Good' :");
        Console.WriteLine("---------------------------------");

        foreach (var training in db.TrainingsSet.Where(t => t.Date == new DateOnly(2024, 01, 19) && t.Description == "Running" && t.Latitude == 48.8566f && t.Longitude == 2.3522f && t.FeedBack == "Good"))
        {
            Console.WriteLine($"\t{training.IdTraining} - {training.Date}, {training.Description}, {training.Latitude}, {training.Longitude}, {training.FeedBack}");
        }

        Console.WriteLine("---------------------------------\n");
    }

    static void AddUpdateDeleteAthlete(HeartTrackContext db)
    {
        Console.WriteLine("Test d'ajout, de modification et de suppression des athletes :");
        var picture = "https://davidalmeida.site/assets/me_avatar.f77af006.png";
        // Ajout d'un nouveau livre
        var newAthlete = new AthleteEntity { UserName = "Doe", LastName = "Doe",ProfilPicture = picture,FirstName = "John", Email = "essaie.example.com", PasswordHash = "TheNewPassword", Sexe = 'M', Length = 1.80, Weight = 90, DateOfBirth = new DateOnly(2024, 02, 22), IsCoach = false };
        db.AthletesSet.Add(newAthlete);
        db.SaveChanges();

        // Affichage des livres après ajout
        Console.WriteLine("Athlete après ajout :");
        foreach (var athlete in db.AthletesSet)
        {
            Console.WriteLine($"\t{athlete.Id} - {athlete.UserName}, {athlete.LastName}, {athlete.FirstName}, {athlete.Email}, {athlete.Sexe}, {athlete.Length}, {athlete.Weight}, {athlete.DateOfBirth}, {athlete.IsCoach}");
        }

        // Modification du titre du nouveau livre
        newAthlete.Email = "email.example@exemple.com";
        db.SaveChanges();

        // Affichage des livres après modification
        Console.WriteLine("Livres après modification :");
        foreach (var athlete in db.AthletesSet)
        {
            Console.WriteLine($"\t{athlete.Id} - {athlete.UserName}, {athlete.LastName}, {athlete.FirstName}, {athlete.Email}, {athlete.Sexe}, {athlete.Length}, {athlete.Weight}, {athlete.DateOfBirth}, {athlete.IsCoach}");
        }

        // Suppression du nouveau livre
        db.AthletesSet.Remove(newAthlete);
        db.SaveChanges();

        // Affichage des livres après suppression
        Console.WriteLine("Livres après suppression :");
        foreach (var athlete in db.AthletesSet)
        {
            Console.WriteLine($"\t{athlete.Id} - {athlete.UserName}, {athlete.LastName}, {athlete.FirstName}, {athlete.Email}, {athlete.Sexe}, {athlete.Length}, {athlete.Weight}, {athlete.DateOfBirth}, {athlete.IsCoach}");
        }
    }

    static void AddUpdateDeleteActivity(HeartTrackContext db)
    {
        Console.WriteLine("Test d'ajout, de modification et de suppression des activités :");

        var newActivity = new ActivityEntity { Type = "Running", Date = new DateOnly(2022, 02, 22), StartTime = new TimeOnly(12, 01, 38), EndTime = new TimeOnly(13, 45, 58), EffortFelt = 5, Variability = 10, Variance = 20, StandardDeviation = 30, Average = 40, Maximum = 50, Minimum = 60, AverageTemperature = 70, HasAutoPause = false };
        db.ActivitiesSet.Add(newActivity);
        db.SaveChanges();

        Console.WriteLine("Activité après ajout :");
        foreach (var activity in db.ActivitiesSet)
        {
            Console.WriteLine($"\t{activity.IdActivity} - {activity.Type}, {activity.Date}, {activity.StartTime}, {activity.EndTime}, {activity.EffortFelt}, {activity.Variability}, {activity.Variance}, {activity.StandardDeviation}, {activity.Average}, {activity.Maximum}, {activity.Minimum}, {activity.AverageTemperature}, {activity.HasAutoPause}");
        }

        newActivity.Type = "Cycling";
        db.SaveChanges();

        Console.WriteLine("Activité après modification :");
        foreach (var activity in db.ActivitiesSet)
        {
            Console.WriteLine($"\t{activity.IdActivity} - {activity.Type}, {activity.Date}, {activity.StartTime}, {activity.EndTime}, {activity.EffortFelt}, {activity.Variability}, {activity.Variance}, {activity.StandardDeviation}, {activity.Average}, {activity.Maximum}, {activity.Minimum}, {activity.AverageTemperature}, {activity.HasAutoPause}");
        }

        db.ActivitiesSet.Remove(newActivity);
        db.SaveChanges();

        Console.WriteLine("Activité après suppression :");
        foreach (var activity in db.ActivitiesSet)
        {
            Console.WriteLine($"\t{activity.IdActivity} - {activity.Type}, {activity.Date}, {activity.StartTime}, {activity.EndTime}, {activity.EffortFelt}, {activity.Variability}, {activity.Variance}, {activity.StandardDeviation}, {activity.Average}, {activity.Maximum}, {activity.Minimum}, {activity.AverageTemperature}, {activity.HasAutoPause}");
        }

    }

    static void AddUpdateDeleteDataSource(HeartTrackContext db)
    {
        Console.WriteLine("Test d'ajout, de modification et de suppression des sources de données :");

        var newDataSource = new DataSourceEntity { Type = "Polar", Model = "Polar Vantage V2", Precision = 0.5F };
        db.DataSourcesSet.Add(newDataSource);
        db.SaveChanges();

        Console.WriteLine("Source de données après ajout :");
        foreach (var dataSource in db.DataSourcesSet)
        {
            Console.WriteLine($"\t{dataSource.IdSource} - {dataSource.Type}, {dataSource.Model}, {dataSource.Precision}");
        }

        newDataSource.Type = "Garmin";
        db.SaveChanges();

        Console.WriteLine("Source de données après modification :");
        foreach (var dataSource in db.DataSourcesSet)
        {
            Console.WriteLine($"\t{dataSource.IdSource} - {dataSource.Type}, {dataSource.Model}, {dataSource.Precision}");
        }

        db.DataSourcesSet.Remove(newDataSource);
        db.SaveChanges();

        Console.WriteLine("Source de données après suppression :");
        foreach (var dataSource in db.DataSourcesSet)
        {
            Console.WriteLine($"\t{dataSource.IdSource} - {dataSource.Type}, {dataSource.Model}, {dataSource.Precision}");
        }
    }

    static void AddUpdateDeleteHeartRate(HeartTrackContext db)
    {
        Console.WriteLine("Test d'ajout, de modification et de suppression des fréquences cardiaques :");

        var newHeartRate = new HeartRateEntity { Altitude = 100, Time = new TimeOnly(12, 00, 00), Temperature = 20, Bpm = 150, Longitude = 0, Latitude = 0 };
        db.HeartRatesSet.Add(newHeartRate);
        db.SaveChanges();

        Console.WriteLine("Fréquence cardiaque après ajout :");
        foreach (var heartRate in db.HeartRatesSet)
        {
            Console.WriteLine($"\t{heartRate.IdHeartRate} - {heartRate.Altitude}, {heartRate.Time}, {heartRate.Temperature}, {heartRate.Bpm}, {heartRate.Longitude}, {heartRate.Latitude}");
        }

        newHeartRate.Altitude = 200;
        db.SaveChanges();

        Console.WriteLine("Fréquence cardiaque après modification :");
        foreach (var heartRate in db.HeartRatesSet)
        {
            Console.WriteLine($"\t{heartRate.IdHeartRate} - {heartRate.Altitude}, {heartRate.Time}, {heartRate.Temperature}, {heartRate.Bpm}, {heartRate.Longitude}, {heartRate.Latitude}");
        }

        db.HeartRatesSet.Remove(newHeartRate);
        db.SaveChanges();

        Console.WriteLine("Fréquence cardiaque après suppression :");
        foreach (var heartRate in db.HeartRatesSet)
        {
            Console.WriteLine($"\t{heartRate.IdHeartRate} - {heartRate.Altitude}, {heartRate.Time}, {heartRate.Temperature}, {heartRate.Bpm}, {heartRate.Longitude}, {heartRate.Latitude}");
        }
    }

    static void AddUpdateDeleteNotification(HeartTrackContext db)
    {
        Console.WriteLine("Test d'ajout, de modification et de suppression des notifications :");

        var newNotification = new NotificationEntity { Message = "Message de test", Date = new DateTime(2022, 01, 01), Statut = false, Urgence = "Urgent" };
        db.NotificationsSet.Add(newNotification);
        db.SaveChanges();

        Console.WriteLine("Notification après ajout :");
        foreach (var notification in db.NotificationsSet)
        {
            Console.WriteLine($"\t{notification.IdNotif} - {notification.Message}, {notification.Date}, {notification.Statut}, {notification.Urgence}");
        }

        newNotification.Message = "Nouveau message de test";
        db.SaveChanges();

        Console.WriteLine("Notification après modification :");
        foreach (var notification in db.NotificationsSet)
        {
            Console.WriteLine($"\t{notification.IdNotif} - {notification.Message}, {notification.Date}, {notification.Statut}, {notification.Urgence}");
        }

        db.NotificationsSet.Remove(newNotification);
        db.SaveChanges();

        Console.WriteLine("Notification après suppression :");
        foreach (var notification in db.NotificationsSet)
        {
            Console.WriteLine($"\t{notification.IdNotif} - {notification.Message}, {notification.Date}, {notification.Statut}, {notification.Urgence}");
        }
    }

    static void AddUpdateDeleteStatistic(HeartTrackContext db)
    {
        Console.WriteLine("Test d'ajout, de modification et de suppression des statistiques :");

        var newStatistic = new StatisticEntity { Weight = 80, AverageHeartRate = 150, MaximumHeartRate = 180, AverageCaloriesBurned = 500, Date = new DateOnly(2022, 01, 01) };
        db.StatisticsSet.Add(newStatistic);
        db.SaveChanges();

        Console.WriteLine("Statistique après ajout :");
        foreach (var statistic in db.StatisticsSet)
        {
            Console.WriteLine($"\t{statistic.IdStatistic} - {statistic.Weight}, {statistic.AverageHeartRate}, {statistic.MaximumHeartRate}, {statistic.AverageCaloriesBurned}, {statistic.Date}");
        }

        newStatistic.Weight = 90;
        db.SaveChanges();

        Console.WriteLine("Statistique après modification :");
        foreach (var statistic in db.StatisticsSet)
        {
            Console.WriteLine($"\t{statistic.IdStatistic} - {statistic.Weight}, {statistic.AverageHeartRate}, {statistic.MaximumHeartRate}, {statistic.AverageCaloriesBurned}, {statistic.Date}");
        }

        db.StatisticsSet.Remove(newStatistic);
        db.SaveChanges();

        Console.WriteLine("Statistique après suppression :");
        foreach (var statistic in db.StatisticsSet)
        {
            Console.WriteLine($"\t{statistic.IdStatistic} - {statistic.Weight}, {statistic.AverageHeartRate}, {statistic.MaximumHeartRate}, {statistic.AverageCaloriesBurned}, {statistic.Date}");
        }
    }

    static void AddUpdateDeleteTraining(HeartTrackContext db)
    {
        Console.WriteLine("Test d'ajout, de modification et de suppression des entrainements :");

        var newTraining = new TrainingEntity { Date = new DateOnly(2022, 01, 01), Description = "Entrainement de test", Latitude = 0, Longitude = 0, FeedBack = "Bon entrainement" };
        db.TrainingsSet.Add(newTraining);
        db.SaveChanges();

        Console.WriteLine("Entrainement après ajout :");
        foreach (var training in db.TrainingsSet)
        {
            Console.WriteLine($"\t{training.IdTraining} - {training.Date}, {training.Description}, {training.Latitude}, {training.Longitude}, {training.FeedBack}");
        }

        newTraining.Description = "Nouvel entrainement de test";
        db.SaveChanges();

        Console.WriteLine("Entrainement après modification :");
        foreach (var training in db.TrainingsSet)
        {
            Console.WriteLine($"\t{training.IdTraining} - {training.Date}, {training.Description}, {training.Latitude}, {training.Longitude}, {training.FeedBack}");
        }

        db.TrainingsSet.Remove(newTraining);
        db.SaveChanges();

        Console.WriteLine("Entrainement après suppression :");
        foreach (var training in db.TrainingsSet)
        {
            Console.WriteLine($"\t{training.IdTraining} - {training.Date}, {training.Description}, {training.Latitude}, {training.Longitude}, {training.FeedBack}");
        }
    }
}