﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Model;
using Shared;
using StubbedContextLib;
using static Model2Entities.DbDataManager;

namespace ConsoleTestEFMapper;

    
    static class Program
    {
        
        static async Task Main(string[] args)
        {
            // Instanciation de DbDataManager et ActivityRepository
           Console.WriteLine("");
            
        }
        /*
        static async Task ActivitiesTestAsync(DbDataManager dataManager, ILogger<DbDataManager> logger)
        {
            var activityRepository = new ActivityRepository(dataManager, logger);

            // Test de la méthode GetActivities
            Console.WriteLine("Testing GetActivities method...");
            var activities = await activityRepository.GetActivities(0, 10, ActivityOrderCriteria.ByType, true);
            foreach (var activity in activities)
            {
                Console.WriteLine($"Activity ID: {activity.Id}, Name: {activity.Type}, Date: {activity.Date}, Start Time: {activity.StartTime}, End Time: {activity.EndTime}");
            }
            Console.WriteLine();

            // Test de la méthode GetActivityByIdAsync
            Console.WriteLine("Testing GetActivityByIdAsync method...");
            var activityById = await activityRepository.GetActivityByIdAsync(2);
            if (activityById != null)
            {
                Console.WriteLine($"Activity found: ID: {activityById.Id}, Name: {activityById.Type}, Date: {activityById.Date}, Start Time: {activityById.StartTime}, End Time: {activityById.EndTime}");
            }
            else
            {
                Console.WriteLine("No activity found with the specified ID.");
            }
            Console.WriteLine();

            // Test de la méthode AddActivity
            Console.WriteLine("Testing AddActivity method...");
            var user = new User
            {
                Id = 1, Username = "DoeDoe",
                ProfilePicture =
                    "https://images.unsplash.com/photo-1682687982134-2ac563b2228b?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
                FirstName = "John", LastName = "Doe",
                Sexe = "M", Lenght = 180, Weight = 70, DateOfBirth = new DateTime(1990, 1, 1),
                Email = "john.doe@example.com", Role = new Athlete()
            };
            var dataSource = new DataSource(1, "Polar", "Vantage V2", 0.5f, new List<User> {user}, new List<Activity>());
            
            var newActivity = new Activity(10, "New Activity", new DateTime(2021, 10, 10),
                new DateTime(10, 10, 10, 10, 10, 10), new DateTime(10, 10, 10, 12, 12, 12), 5, 5, 5, 5, 5, 5, 5, 5,
                false, user)
            {
                DataSource = dataSource
            };
            var HeartRates = new List<HeartRate>
            {
                new HeartRate(1, 60, new TimeOnly(10, 10, 10), newActivity, 5, 5, 5, 5, 5, 5, 5, 5),
            };
            newActivity.HeartRates = HeartRates;
            var addedActivity = await activityRepository.AddActivity(newActivity);
            if (addedActivity != null)
            {
                Console.WriteLine($"New activity added successfully: ID: {addedActivity.Id}, Name: {addedActivity.Type}, Date: {addedActivity.Date}, Start Time: {addedActivity.StartTime}, End Time: {addedActivity.EndTime}");
            }
            else
            {
                Console.WriteLine("Failed to add new activity.");
            }
            Console.WriteLine();

            // Test de la méthode UpdateActivity
            Console.WriteLine("Testing UpdateActivity method...");
            var updatedActivity = await activityRepository.UpdateActivity(6, new Activity { Id = 6, Type = "Updated Activity", Date = new DateTime(2021, 10, 11), StartTime = new DateTime(10, 10, 10, 10, 10, 10), EndTime = new DateTime(10, 10, 10, 12, 12, 12) });
            if (updatedActivity != null)
            {
                Console.WriteLine($"Activity updated successfully: ID: {updatedActivity.Id}, Name: {updatedActivity.Type}, Date: {updatedActivity.Date}, Start Time: {updatedActivity.StartTime}, End Time: {updatedActivity.EndTime}");
            }
            else
            {
                Console.WriteLine("Failed to update activity.");
            }
            Console.WriteLine();

            // Test de la méthode DeleteActivity
            Console.WriteLine("Testing DeleteActivity method...");
            var isDeleted = await activityRepository.DeleteActivity(1);
            if (isDeleted)
            {
                Console.WriteLine("Activity deleted successfully.");
            } else
            {
                Console.WriteLine("Failed to delete activity.");
            }

            // Test de la méthode GetNbItems
            Console.WriteLine("Testing GetNbItems method...");
            var itemCount = await activityRepository.GetNbItems();
            Console.WriteLine($"Total number of activities: {itemCount}");
        }
        static void UsersTest()
        {}*/
    }
