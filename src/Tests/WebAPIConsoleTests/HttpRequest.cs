/*!
 * \file HttpRequest.cs
 * \brief Fichier contenant la classe HttpRequest.
 */

using System.Diagnostics;
using System.Net.Http.Json;
using Dto;
using Shared;

namespace WebAPIConsoleTests;

/*!
 * \brief Classe représentant un client HTTP pour les requêtes vers un service de gestion de elément.
 */
public class HttpRequest<T> where T : class
{
    private HttpClient _httpClient { get; } = new HttpClient();
    public HttpClient HttpClient => _httpClient;

    /*!
     * \brief Récupère tous les activitée de manière asynchrone.
     * \return Une tâche représentant l'opération asynchrone qui retourne une liste de T.
     */
    public async Task<List<T>> GetAllAsync()
    {
        return await _httpClient.GetFromJsonAsync<List<T>>("");
    }

    /*!
     * \brief Récupère les élements par index et compte de manière asynchrone.
     * \param index L'index de départ pour la pagination.
     * \param count Le nombre d'éléments à récupérer.
     * \return Une tâche représentant l'opération asynchrone qui retourne une liste de T.
     */ 
    // [TODO] enum
    public async Task<List<T>> GetAsync(Enum criteria, bool descending, int index, int count)
    {
        return await _httpClient.GetFromJsonAsync<List<T>>($"?OrderingPropertyName={criteria}&Descending={descending}&Index={index}&Count={count}");
    }

    /*!
     * \brief Récupère un elément par son identifiant de manière asynchrone.
     * \param id L'identifiant du elément à récupérer.
     * \return Une tâche représentant l'opération asynchrone qui retourne une liste de T.
     */
    public async Task<T?> GetByIdAsync(int id)
    {
        return await _httpClient.GetFromJsonAsync<T>($"{id}");
    }

    public Task<int> GetNbItems()
    {
        return _httpClient.GetFromJsonAsync<int>("count");
    }

    public Task<IEnumerable<T>?> GetActivitiesByUser(int userId, int index, int count, ActivityOrderCriteria orderCriteria, bool descending = false)
    {
        return _httpClient.GetFromJsonAsync<IEnumerable<T>?>($"?userId={userId}&index={index}&count={count}&orderCriteria={orderCriteria}&descending={descending}");
    }

    public Task<int> GetNbActivitiesByUser(int userId)
    {
        return _httpClient.GetFromJsonAsync<int>($"count?userId={userId}");
    }

    /*!
     * \brief Ajoute une activity de manière asynchrone.
     * \param book Le elément à ajouter.
     * \return Une tâche représentant l'opération asynchrone qui retourne le activity ajouté (T).
     */
    public async Task<T> PostAsync(T activity)
    {
        var response = await _httpClient.PostAsJsonAsync("", activity);

        return await response.Content.ReadFromJsonAsync<T>();
    }

    /*!
     * \brief Met à jour un elément de manière asynchrone.
     * \param id L'identifiant du elément à mettre à jour.
     * \param book Les nouvelles données du elément à mettre à jour.
     * \return Une tâche représentant l'opération asynchrone qui retourne le elément mis à jour (T).
     */
    public async Task<T> PutAsync(int id, T activity)
    {
        var response = await _httpClient.PutAsJsonAsync($"{id}", activity);

        return await response.Content.ReadFromJsonAsync<T>();
    }

    /*!
     * \brief Supprime un elément de manière asynchrone.
     * \param id L'identifiant du elément à supprimer.
     * \return Une tâche représentant l'opération asynchrone.
     */
    public async Task DeleteAsync(int id)
    {
        await _httpClient.DeleteAsync($"{id}");
    }
}