﻿using APIMappers;
using Dto;
using Model;
using Model.Repository;
using Shared;
using WebAPIConsoleTests;
Console.WriteLine("");
/*
IActivityRepository myConsoleTest = new ActivityServiceAPI();

// defini un delais d'attente du déploiement de l'API
await Task.Delay(5000);

// Affiche toutes les activités
Console.WriteLine("Affichage de toutes les Activités : ");
var res = await myConsoleTest.GetActivities(0, 10, ActivityOrderCriteria.ByAthleteId, false);
foreach(var myActivity in res)
{
    Console.WriteLine(myActivity.Id + ", " + myActivity.Type + ", " + myActivity.StartTime + ", " + myActivity.EndTime + ", " + myActivity.DataSource + ", " + myActivity.Athlete);
}

// Affiche les activités par id
Console.WriteLine("Affichage du livre d'id 1 : ");
res = (IEnumerable<Model.Activity>)await myConsoleTest.GetActivityByIdAsync(1);
foreach (var myActivity in res)
{
    Console.WriteLine(myActivity.Id + ", " + myActivity.Type + ", " + myActivity.StartTime + ", " + myActivity.EndTime + ", " + myActivity.DataSource + ", " + myActivity.Athlete);
}

// Ajouter une nouvelle activité
Console.WriteLine("Ajout d'un nouveau livre : ");
var newActivity = new ActivityDto { Type = "New Activity", StartTime = DateTime.Now, EndTime = DateTime.Now, DataSource = new DataSourceDto{}, Athlete = new UserDto{ Username = "Hello", FirstName = "feee", Email = "exemple.com", LastName = "dddd", Sexe = "M" } };
var addedActivity = await myConsoleTest.AddActivity(newActivity.ToModel());
Console.WriteLine($"Id: {addedActivity.Id}, Type: {addedActivity.Type}, StartTime: {addedActivity.StartTime}, EndTime: {addedActivity.EndTime}, DataSource: {addedActivity.DataSource}, Athlete: {addedActivity.Athlete}");

// Mettre à jour l'activity ajouté
Console.WriteLine("Mise à jour du livre ajouté : ");
var activity = await myConsoleTest.UpdateActivity(1, new ActivityDto { Id = 1, Type = "Updated Activity", StartTime = DateTime.Now, EndTime = DateTime.Now, DataSource = new DataSourceDto{}, Athlete = new UserDto{ Username = "Hello", FirstName = "feee", Email = "exemple.com", LastName = "dddd", Sexe = "M" } }.ToModel());
Console.WriteLine($"Id: {activity.Id}, Type: {activity.Type}, StartTime: {activity.StartTime}, EndTime: {activity.EndTime}, DataSource: {activity.DataSource}, Athlete: {activity.Athlete}");

// Supprimer l'activity ajouté
Console.WriteLine("Suppression du livre ajouté : ");
await myConsoleTest.DeleteActivity(newActivity.Id);
res = await myConsoleTest.GetActivities(0, 10, ActivityOrderCriteria.ByAthleteId, false);
foreach (var activity1 in res)
{
    Console.WriteLine(activity1.Id + ", " + activity1.Type + ", " + activity1.StartTime + ", " + activity1.EndTime + ", " + activity1.DataSource + ", " + activity1.Athlete);
}

// Affiche le nombre d'activités
Console.WriteLine("Affichage du nombre d'activités : ");
var nb = await myConsoleTest.GetNbItems();
Console.WriteLine(nb);

// Affiche les activités par utilisateur
Console.WriteLine("Affichage des activités par utilisateur : ");
res = await myConsoleTest.GetActivitiesByUser(1, 0, 10, ActivityOrderCriteria.ByAthleteId, false);
foreach (var activity3 in res)
{
    Console.WriteLine(activity3.Id + ", " + activity3.Type + ", " + activity3.StartTime + ", " + activity3.EndTime + ", " + activity3.DataSource + ", " + activity3.Athlete);
}

// Affiche le nombre d'activités par utilisateur
Console.WriteLine("Affichage du nombre d'activités par utilisateur : ");
nb = await myConsoleTest.GetNbActivitiesByUser(1);
Console.WriteLine(nb);*/