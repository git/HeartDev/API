/*/*!
 * \file BookDataServiceAPI.cs
 * \author HeartTeam
 * \brief Fichier contenant la classe BookDataServiceAPI.
 #1#

using System.Diagnostics;
using Dto;
using Model.Repository;
using Shared;
using APIMappers;
using Dto.Tiny;
using Model;

namespace WebAPIConsoleTests;

/*!
 * \brief Implémentation de l'interface IActivityRepository pour récupérer des activités via un service HTTP.
 #1#
public class ActivityServiceAPI : IActivityRepository
{
    private HttpRequest<ActivityDto> myRequest = new HttpRequest<ActivityDto>();

    /*!
     * \brief Constructeur de la classe ActivityServiceAPI.
     * Initialise l'adresse de base du client HTTP.
     #1#
    public ActivityServiceAPI()
    {
        myRequest.HttpClient.BaseAddress = new Uri("http://localhost:5030/api/v1/Activity/");
    }

    /*!
     * \brief Récupère toutes les Activités de manière asynchrone.
     * \return Une tâche représentant l'opération asynchrone qui retourne une liste d'Activity.
     #1#
    public async Task<IEnumerable<Model.Activity>?> GetActivities(int index, int count, ActivityOrderCriteria criteria, bool descending = false)
    {
        var activityDtos = await myRequest.GetAllAsync();
        return activityDtos?.ToModels();
    }

    /*!
     * \brief Récupère les activités par index et compte de manière asynchrone.
     * \param index L'index de départ pour la pagination.
     * \param count Le nombre d'éléments à récupérer.
     * \return Une tâche représentant l'opération asynchrone qui retourne une liste d'Activity.
     #1#
    public async Task<List<Model.Activity>> GetBooksAsync(ActivityOrderCriteria criteria, bool descending, int index, int count)
    {
        var activityDtos = await myRequest.GetAsync(criteria, descending, index, count);
        return (List<Model.Activity>)activityDtos.ToModels();
    }

    /*!
     * \brief Récupère une activité par son identifiant de manière asynchrone.
     * \param id L'identifiant du livre à récupérer.
     * \return Une tâche représentant l'opération asynchrone qui retourne une liste d'Activity.
     #1#
    async Task<IEnumerable<ActivityTinyDto>?> IActivityRepository.GetActivities(int index, int count, ActivityOrderCriteria criteria, bool descending)
    {
        throw new NotImplementedException();
    }

    public async Task<Model.Activity?> GetActivityByIdAsync(int id)
    {
        var activityDtos = await myRequest.GetByIdAsync(id);
        return activityDtos.ToModel();
    }

    /*!
     * \brief Ajoute une activité de manière asynchrone.
     * \param activity L'Activity à ajouter.
     * \return Une tâche représentant l'opération asynchrone qui retourne l'activité ajouté (Activity).
     #1#
    public async Task<Model.Activity?> AddActivity(Model.Activity activity)
    {
        return (await myRequest.PostAsync(activity.ToDto())).ToModel();
    }

    public async Task<ResponseActivityDto?> AddActivity(NewActivityDto activity)
    {
        throw new NotImplementedException();
    }

    public async Task<ResponseActivityDto?> UpdateActivity(int id, ActivityTinyDto activity)
    {
        throw new NotImplementedException();
    }

    /*!
     * \brief Met à jour une activité de manière asynchrone.
     * \param id L'identifiant de l'activité à mettre à jour.
     * \param activity Les nouvelles données de l'activité à mettre à jour.
     * \return Une tâche représentant l'opération asynchrone qui retourne l'activité mis à jour (Activity).
     #1#
    public async Task<Model.Activity?> UpdateActivity(int id, Model.Activity activity)
    {
        var activityDto = activity.ToDto();
        var updatedActivityDto = await myRequest.PutAsync(id, activityDto);
        return updatedActivityDto?.ToModel();
    }

    /*!
     * \brief Supprime une activité de manière asynchrone.
     * \param id L'identifiant de l'activité à supprimer.
     * \return Une tâche représentant l'opération asynchrone.
     #1#
    public async Task<bool> DeleteActivity(int id)
    {
        await myRequest.DeleteAsync(id);
        return true;
    }

    public Task<int> GetNbItems()
    {
        return myRequest.GetNbItems();
    }

    public async Task<IEnumerable<Model.Activity>?> GetActivitiesByUser(int userId, int index, int count, ActivityOrderCriteria orderCriteria, bool descending = false)
    {
        return (await myRequest.GetActivitiesByUser(userId, index, count, orderCriteria, descending)).ToModels();
    }

    public Task<int> GetNbActivitiesByUser(int userId)
    {
        return myRequest.GetNbActivitiesByUser(userId);
    }
}*/