//-----------------------------------------------------------------------
// FILENAME: ActivityTests.cs
// PROJECT: UnitTestsModel
// DATE CREATED: 16/03/2024
// AUTHOR: HeartTeam
// PURPOSE: Contains unit tests for the Activity class in the Model namespace.
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Model;
using Xunit;

namespace UnitTestsModel
{
    /// <summary>
    /// Contains unit tests for the <see cref="Activity"/> class.
    /// </summary>
    public class ActivityTests
    {
        /// <summary>
        /// Verifies that the constructor sets the effort property correctly when a valid effort is provided.
        /// </summary>
        [Fact]
        public void Constructor_WithValidEffort_SetsEffort()
        {
            var date = DateTime.Now;
            var validEffort = 3;

            var activity = new Activity(
                id: 1,
                type: "Running",
                date: date,
                startTime: date.AddHours(1),
                endTime: date.AddHours(2),
                effort: validEffort,
                variability: 0.5f,
                variance: 0.3f,
                standardDeviation: 0.2f,
                average: 150,
                maximum: 180,
                minimum: 120,
                averageTemperature: 25.5f,
                hasAutoPause: false,
                user: new User(),
                dataSource: new DataSource(1, "Type", "Model", 0.1f, new List<User>(), new List<Activity>()),
                heartRates: null
            );

            Assert.Equal(validEffort, activity.Effort);
        }

        /// <summary>
        /// Verifies that the constructor throws an ArgumentException when an invalid effort is provided.
        /// </summary>
        [Fact]
        public void Constructor_WithInvalidEffort_ThrowsArgumentException()
        {
            var date = DateTime.Now;
            var invalidEffort = 6;

            Assert.Throws<ArgumentException>(() => new Activity(
                id: 1,
                type: "Running",
                date: date,
                startTime: date.AddHours(1),
                endTime: date.AddHours(2),
                effort: invalidEffort,
                variability: 0.5f,
                variance: 0.3f,
                standardDeviation: 0.2f,
                average: 150,
                maximum: 180,
                minimum: 120,
                averageTemperature: 25.5f,
                hasAutoPause: false,
                user: new User(),
                dataSource: new DataSource(1, "Type", "Model", 0.1f, new List<User>(), new List<Activity>()),
                heartRates: null
            ));
        }

        /// <summary>
        /// Verifies that the ToString method returns the expected string representation of the activity.
        /// </summary>
        [Fact]
        public void ToString_ReturnsExpectedString()
        {
            var date = new DateTime(2023, 3, 16, 10, 0, 0);
            var activity = new Activity(
                id: 1,
                type: "Running",
                date: date,
                startTime: date.AddHours(1),
                endTime: date.AddHours(2),
                effort: 3,
                variability: 0.5f,
                variance: 0.3f,
                standardDeviation: 0.2f,
                average: 150,
                maximum: 180,
                minimum: 120,
                averageTemperature: 25.5f,
                hasAutoPause: false,
                user: new User(),
                dataSource: new DataSource(1, "Type", "Model", 0.1f, new List<User>(), new List<Activity>()),
                heartRates: null
            );

            var result = activity.ToString();

            Assert.Contains("Activity #1: Running on 16/3/2023 from 11:00:00 to 12:00:00", result);
        }

        /// <summary>
        /// Verifies that the constructor sets all properties correctly.
        /// </summary>
        [Fact]
        public void Constructor_SetsPropertiesCorrectly()
        {
            var date = DateTime.Now;
            var user = new User();
            var dataSource = new DataSource(1, "Type", "Model", 0.1f, new List<User>(), new List<Activity>());
            var heartRates = new List<HeartRate>();

            var activity = new Activity(
                id: 1,
                type: "Running",
                date: date,
                startTime: date.AddHours(1),
                endTime: date.AddHours(2),
                effort: 3,
                variability: 0.5f,
                variance: 0.3f,
                standardDeviation: 0.2f,
                average: 150,
                maximum: 180,
                minimum: 120,
                averageTemperature: 25.5f,
                hasAutoPause: false,
                user: user,
                dataSource: dataSource,
                heartRates: heartRates
            );

            Assert.Equal(1, activity.Id);
            Assert.Equal("Running", activity.Type);
            Assert.Equal(date, activity.Date);
            Assert.Equal(date.AddHours(1), activity.StartTime);
            Assert.Equal(date.AddHours(2), activity.EndTime);
            Assert.Equal(3, activity.Effort);
            Assert.Equal(0.5f, activity.Variability);
            Assert.Equal(0.3f, activity.Variance);
            Assert.Equal(0.2f, activity.StandardDeviation);
            Assert.Equal(150, activity.Average);
            Assert.Equal(180, activity.Maximum);
            Assert.Equal(120, activity.Minimum);
            Assert.Equal(25.5f, activity.AverageTemperature);
            Assert.False(activity.HasAutoPause);
            Assert.Equal(user, activity.Athlete);
            Assert.Equal(dataSource, activity.DataSource);
            Assert.Equal(heartRates, activity.HeartRates);
        }

        [Fact]
        public void Constructor_SetsPropertiesCorrectly2()
        {
            var id = 1;
            var type = "Running";
            var date = DateTime.Now;
            var startTime = date.AddHours(1);
            var endTime = date.AddHours(2);
            var effort = 3;
            var variability = 0.5f;
            var variance = 0.3f;
            var standardDeviation = 0.2f;
            var average = 150f;
            var maximum = 180;
            var minimum = 120;
            var averageTemperature = 25.5f;
            var hasAutoPause = false;
            var user = new User();

            var activity = new Activity(
                id,
                type,
                date,
                startTime,
                endTime,
                effort,
                variability,
                variance,
                standardDeviation,
                average,
                maximum,
                minimum,
                averageTemperature,
                hasAutoPause,
                user
            );

            Assert.Equal(id, activity.Id);
            Assert.Equal(type, activity.Type);
            Assert.Equal(date, activity.Date);
            Assert.Equal(startTime, activity.StartTime);
            Assert.Equal(endTime, activity.EndTime);
            Assert.Equal(effort, activity.Effort);
            Assert.Equal(variability, activity.Variability);
            Assert.Equal(variance, activity.Variance);
            Assert.Equal(standardDeviation, activity.StandardDeviation);
            Assert.Equal(average, activity.Average);
            Assert.Equal(maximum, activity.Maximum);
            Assert.Equal(minimum, activity.Minimum);
            Assert.Equal(averageTemperature, activity.AverageTemperature);
            Assert.Equal(hasAutoPause, activity.HasAutoPause);
            Assert.Equal(user, activity.Athlete);
        }

        [Fact]
        public void ToString_ReturnsCorrectStringRepresentation()
        {
            var date = DateTime.Now;
            var user = new User();
            var dataSource = new DataSource(1, "Type", "Model", 0.1f, new List<User>(), new List<Activity>());
            var heartRates = new List<HeartRate>();

            var activity = new Activity(
                id: 1,
                type: "Running",
                date: date,
                startTime: date.AddHours(1),
                endTime: date.AddHours(2),
                effort: 3,
                variability: 0.5f,
                variance: 0.3f,
                standardDeviation: 0.2f,
                average: 150,
                maximum: 180,
                minimum: 120,
                averageTemperature: 25.5f,
                hasAutoPause: false,
                user: user,
                dataSource: dataSource,
                heartRates: heartRates
            );

            var result = activity.ToString();

            Console.WriteLine(result);

            Assert.Equal($"Activity #1: Running on {date:d/M/yyyy} from {date.AddHours(1):HH:mm:ss} to {date.AddHours(2):HH:mm:ss} with an effort of 3/5 and an average temperature of 25.5°C and a heart rate variability of 0.5 bpm and a variance of 0.3 bpm and a standard deviation of 0.2 bpm and an average of 150 bpm and a maximum of 180 bpm and a minimum of 120 bpm and auto pause is disabled.", result);
        }
    }
}