using Model;
using System.Collections.Generic;
using Xunit;

namespace Model.Tests
{
    public class DataSourceTests
    {
        [Fact]
        public void Constructor_WithArguments_InitializesProperties()
        {
            var id = 1;
            var type = "Type";
            var model = "Model";
            var precision = 0.5f;
            var athletes = new List<User>();
            var activities = new List<Activity>();

            var dataSource = new DataSource(id, type, model, precision, athletes, activities);

            Assert.Equal(id, dataSource.Id);
            Assert.Equal(type, dataSource.Type);
            Assert.Equal(model, dataSource.Model);
            Assert.Equal(precision, dataSource.Precision);
            Assert.Same(athletes, dataSource.Athletes);
            Assert.Same(activities, dataSource.Activities);
        }

        [Fact]
        public void ToString_ReturnsExpectedString()
        {
            var athletes = new List<User>();
            var activities = new List<Activity>();
            var dataSource = new DataSource("Type", "Model", 0.1f, athletes, activities);

            var result = dataSource.ToString();

            Assert.Equal("DataSource #0: Type Model with a precision of 0.1", result);
        }
    }
}
