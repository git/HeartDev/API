using Model;
using Xunit;

namespace Model.Tests
{
    public class CoachTests
    {
        [Fact]
        public void CheckAdd_AthleteUser_ReturnsTrue()
        {
            var coach = new Coach();

            var user = new User { Role = new Athlete() };
            var result = coach.CheckAdd(user);

            Assert.True(result);
            Assert.Equal("CoachAthlete", coach.ToString());
        }

        [Fact]
        public void CheckAdd_NonAthleteUser_ReturnsFalse()
        {
            var coach = new Coach();

            var user = new User { Role = new Coach() };
            var result = coach.CheckAdd(user);

            Assert.False(result);
            Assert.Equal("CoachAthlete", coach.ToString());
        }
    }
}