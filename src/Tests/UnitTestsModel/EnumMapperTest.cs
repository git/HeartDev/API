using Model;
using Model.Repository;
using Moq;
using Xunit;

namespace UnitTestsModel
{
    public class EnumMapperTests
    {
        [Theory]
        [InlineData("None", Shared.AthleteOrderCriteria.None)]
        [InlineData("ByUsername", Shared.AthleteOrderCriteria.ByUsername)]
        [InlineData("ByFirstName", Shared.AthleteOrderCriteria.ByFirstName)]
        [InlineData("ByLastName", Shared.AthleteOrderCriteria.ByLastName)]
        [InlineData("BySexe", Shared.AthleteOrderCriteria.BySexe)]
        [InlineData("ByLenght", Shared.AthleteOrderCriteria.ByLenght)]
        [InlineData("ByWeight", Shared.AthleteOrderCriteria.ByWeight)]
        [InlineData("ByDateOfBirth", Shared.AthleteOrderCriteria.ByDateOfBirth)]
        [InlineData("ByEmail", Shared.AthleteOrderCriteria.ByEmail)]
        [InlineData("ByRole", Shared.AthleteOrderCriteria.ByRole)]
        [InlineData(null, Shared.AthleteOrderCriteria.None)]
        [InlineData("InvalidValue", Shared.AthleteOrderCriteria.None)]
        public void ToEnum_WithValidValue_ReturnsCorrectEnumValue(string? value, Shared.AthleteOrderCriteria expected)
        {
            var userRepositoryMock = new Mock<IUserRepository>();

            var result = EnumMappeur.ToEnum(userRepositoryMock.Object, value);

            Assert.Equal(expected, result);
        }
    }
}
