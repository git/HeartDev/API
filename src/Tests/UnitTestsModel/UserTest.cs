using Xunit;

namespace Model.Tests
{
    public class UserTests
    {
        [Fact]
        public void UserConstructor_ValidParameters_ConstructsObject()
        {
            string username = "john_doe";
            string profilePicture = "profile.jpg";
            string lastName = "Doe";
            string firstName = "John";
            string email = "john.doe@example.com";
            string password = "password";
            char sex = 'M';
            float length = 180.5f;
            float weight = 75.3f;
            DateTime dateOfBirth = new DateTime(1990, 5, 15);
            Role role = new Athlete();

            User user = new User(username, profilePicture, lastName, firstName, email, password, sex, length, weight, dateOfBirth, role);

            Assert.NotNull(user);
            Assert.Equal(username, user.Username);
            Assert.Equal(profilePicture, user.ProfilePicture);
            Assert.Equal(lastName, user.LastName);
            Assert.Equal(firstName, user.FirstName);
            Assert.Equal(email, user.Email);
            Assert.Equal(password, user.MotDePasse);
            Assert.Equal(sex, user.Sexe);
            Assert.Equal(length, user.Lenght);
            Assert.Equal(weight, user.Weight);
            Assert.Equal(dateOfBirth, user.DateOfBirth);
            Assert.Equal(role, user.Role);
        }
    }
}