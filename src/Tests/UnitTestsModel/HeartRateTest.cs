using Xunit;
using System;

namespace Model.Tests
{
    public class HeartRateTests
    {
        [Fact]
        public void HeartRate_Constructor_WithValidParameters()
        {
            
            int id = 1;
            int bpm = 80;
            TimeOnly timestamp = new TimeOnly(12, 30, 0);
            Activity activity = new Activity();
            double? latitude = 40.7128;
            double? longitude = -74.0060;
            double? altitude = 10.5;
            int? cadence = 120;
            double? distance = 5.2;
            double? speed = 10.5;
            int? power = 200;
            double? temperature = 25.5;

            var heartRate = new HeartRate(id, bpm, timestamp, activity, latitude, longitude, altitude, cadence, distance, speed, power, temperature);

            Assert.Equal(id, heartRate.Id);
            Assert.Equal(bpm, heartRate.Bpm);
            Assert.Equal(timestamp, heartRate.Timestamp);
            Assert.Equal(activity, heartRate.Activity);
            Assert.Equal(latitude, heartRate.Latitude);
            Assert.Equal(longitude, heartRate.Longitude);
            Assert.Equal(altitude, heartRate.Altitude);
            Assert.Equal(cadence, heartRate.Cadence);
            Assert.Equal(distance, heartRate.Distance);
            Assert.Equal(speed, heartRate.Speed);
            Assert.Equal(power, heartRate.Power);
            Assert.Equal(temperature, heartRate.Temperature);
        }

        [Fact]
        public void HeartRate_ToString_ReturnsExpectedString()
        {
            int id = 1;
            int bpm = 80;
            TimeOnly timestamp = new TimeOnly(12, 30, 0);
            Activity activity = new Activity();
            double? latitude = 40.7128;
            double? longitude = -74.0060;
            double? altitude = 10.5;
            int? cadence = 120;
            double? distance = 5.2;
            double? speed = 10.5;
            int? power = 200;
            double? temperature = 25.5;
            var heartRate = new HeartRate(id, bpm, timestamp, activity, latitude, longitude, altitude, cadence, distance, speed, power, temperature);

            var result = heartRate.ToString();

            Assert.Contains($"HeartRate #{id}", result);
            Assert.Contains($"{bpm} bpm", result);
            Assert.Contains($"{timestamp:HH:mm:ss}", result);
            Assert.Contains($"temperature of {temperature}°C", result);
            Assert.Contains($"altitude of {altitude}m", result);
            Assert.Contains($"at {longitude}°E and {latitude}°N", result);
        }
        [Fact]
        public void Constructor_WithAllParameters_ShouldInitializeCorrectly()
        {
            int bpm = 70;
            var timestamp = new TimeOnly(12, 0);
            var activity = new Activity();
            double? latitude = 40.7128;
            double? longitude = -74.0060;
            double? altitude = 10.0;
            int? cadence = 80;
            double? distance = 5.0;
            double? speed = 10.0;
            int? power = 200;
            double? temperature = 20.0;

            var heartRate = new HeartRate(bpm, timestamp, activity, latitude, longitude, altitude, cadence, distance, speed, power, temperature);

            Assert.NotNull(heartRate);
            Assert.Equal(bpm, heartRate.Bpm);
            Assert.Equal(timestamp, heartRate.Timestamp);
            Assert.Equal(activity, heartRate.Activity);
            Assert.Equal(latitude, heartRate.Latitude);
            Assert.Equal(longitude, heartRate.Longitude);
            Assert.Equal(altitude, heartRate.Altitude);
            Assert.Equal(cadence, heartRate.Cadence);
            Assert.Equal(distance, heartRate.Distance);
            Assert.Equal(speed, heartRate.Speed);
            Assert.Equal(power, heartRate.Power);
            Assert.Equal(temperature, heartRate.Temperature);
        }

        [Fact]
        public void Constructor_WithMinimalParameters_ShouldInitializeCorrectly()
        {
            int bpm = 70;
            var timestamp = new TimeOnly(12, 0);

            var heartRate = new HeartRate(bpm, timestamp, 1, null, null, null, null, null, null, null, null);

            Assert.NotNull(heartRate);
            Assert.Equal(bpm, heartRate.Bpm);
            Assert.Equal(timestamp, heartRate.Timestamp);
            Assert.Null(heartRate.Activity);
            Assert.Null(heartRate.Latitude);
            Assert.Null(heartRate.Longitude);
            Assert.Null(heartRate.Altitude);
            Assert.Null(heartRate.Cadence);
            Assert.Null(heartRate.Distance);
            Assert.Null(heartRate.Speed);
            Assert.Null(heartRate.Power);
            Assert.Null(heartRate.Temperature);
        }
    }
}