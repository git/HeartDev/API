using Xunit;

namespace Model.Tests
{
    public class NotificationTests
    {
        [Fact]
        public void Constructor_InitializesPropertiesCorrectly()
        {
            int id = 1;
            string message = "Test notification";
            DateTime date = DateTime.Now;
            bool status = false;
            string urgency = "High";
            int toUserId = 10;

            var notification = new Notification(id, message, date, status, urgency, toUserId);

            Assert.Equal(id, notification.IdNotif);
            Assert.Equal(message, notification.Message);
            Assert.Equal(date, notification.Date);
            Assert.Equal(status, notification.Statut);
            Assert.Equal(urgency, notification.Urgence);
            Assert.Equal(toUserId, notification.ToUserId);
        }

        [Fact]
        public void Constructor_DefaultConstructor_ShouldInitializeCorrectly()
        {
            var notification = new Notification();

            Assert.NotNull(notification);
        }
    }
}