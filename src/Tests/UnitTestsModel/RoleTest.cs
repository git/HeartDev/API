using Xunit;

namespace Model.Tests
{
    public class RoleTests
    {
        [Fact]
        public void AddUser_WithValidUser_ReturnsTrue()
        {
            var role = new Athlete();
            var user = new User("hello13", "dkjd.png", "John", "Doe", "john@exemple.com", "password", 'M', 180.5f, 75.3f, new System.DateTime(1990, 5, 15), new Athlete());

            bool result = role.AddUser(user);

            Assert.True(result);
        }

        [Fact]
        public void AddUser_WithInvalidUser_ReturnsFalse()
        {
            var role = new Athlete();
            var user = new User();

            bool result = role.AddUser(user);

            Assert.False(result);
        }

    }
}
