using Model;
using Xunit;

namespace Model.Tests
{
    public class LargeImageTests
    {
        [Fact]
        public void Equals_ReturnsTrue_WhenBase64StringsAreEqual()
        {
            var base64String = "VGhpcyBpcyBhIGJhc2U2NCBlbmNvZGVkIHN0cmluZw==";
            var image1 = new LargeImage(base64String);
            var image2 = new LargeImage(base64String);

            var result = image1.Equals(image2);

            Assert.True(result);
        }

        [Fact]
        public void Equals_ReturnsFalse_WhenBase64StringsAreDifferent()
        {
            var base64String1 = "VGhpcyBpcyBhIGJhc2U2NCBlbmNvZGVkIHN0cmluZw==";
            var base64String2 = "VGhpcyBpcyBhIGJhc2U2NSBlbmNvZGVkIHN0cmluZw==";
            var image1 = new LargeImage(base64String1);
            var image2 = new LargeImage(base64String2);

            var result = image1.Equals(image2);

            Assert.False(result);
        }
        
        [Fact]
        public void Equals_WithNull_ReturnsFalse()
        {
            LargeImage image = new LargeImage("base64");

            var result = image.Equals(null);

            
            Assert.False(result);
        }

        [Fact]
        public void Equals_WithSameReference_ReturnsTrue()
        {
            LargeImage image = new LargeImage("base64");

            var result = image.Equals(image);

            
            Assert.True(result);
        }

        [Fact]
        public void Equals_WithDifferentType_ReturnsFalse()
        {
            LargeImage image = new LargeImage("base64");
            var obj = new object();

            var result = image.Equals(obj);

            
            Assert.False(result);
        }

        [Fact]
        public void Equals_WithDifferentBase64_ReturnsFalse()
        {
            LargeImage image1 = new LargeImage("base64");
            LargeImage image2 = new LargeImage("differentBase64");

            var result = image1.Equals(image2);

            
            Assert.False(result);
        }

        [Fact]
        public void Equals_WithSameBase64_ReturnsTrue()
        {
            LargeImage image1 = new LargeImage("base64");
            LargeImage image2 = new LargeImage("base64");

            var result = image1.Equals(image2);

            
            Assert.True(result);
        }

        [Fact]
        public void GetHashCode_ReturnsSameHashCode_ForSameBase64()
        {
            string base64 = "abcdefghij";
            LargeImage largeImage1 = new LargeImage(base64);
            LargeImage largeImage2 = new LargeImage(base64);

            int hashCode1 = largeImage1.GetHashCode();
            int hashCode2 = largeImage2.GetHashCode();

            Assert.Equal(hashCode1, hashCode2);
        }

        [Fact]
        public void Equals_ReturnsTrue_WhenComparingWithItself()
        {
            var largeImage = new LargeImage("base64String");

            var result = largeImage.Equals(largeImage);

            Assert.True(result);
        }

        [Fact]
        public void Equals_ReturnsFalse_WhenComparingWithNull()
        {
            var largeImage = new LargeImage("base64String");

            var result = largeImage.Equals(null);

            Assert.False(result);
        }

        [Fact]
        public void Equals_ReturnsFalse_WhenComparingWithDifferentType()
        {
            var largeImage = new LargeImage("base64String");
            var otherObject = new object();

            var result = largeImage.Equals(otherObject);

            Assert.False(result);
        }

        [Fact]
        public void Equals_ReturnsFalse_WhenComparingWithDifferentLargeImage()
        {
            var largeImage1 = new LargeImage("base64String1");
            var largeImage2 = new LargeImage("base64String2");

            var result = largeImage1.Equals(largeImage2);

            Assert.False(result);
        }

        [Fact]
        public void Equals_ReturnsTrue_WhenComparingWithEqualLargeImage()
        {
            var base64String = "base64String";
            var largeImage1 = new LargeImage(base64String);
            var largeImage2 = new LargeImage(base64String);

            var result = largeImage1.Equals(largeImage2);

            Assert.True(result);
        }
    }
}
