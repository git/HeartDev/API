using Xunit;

namespace Model.Tests
{
    public class RelationshipRequestTests
    {
        [Fact]
        public void Constructor_InitializesPropertiesCorrectly()
        {
            int id = 1;
            int fromUserId = 2;
            int toUserId = 3;
            string status = "Pending";

            var request = new RelationshipRequest
            {
                Id = id,
                FromUser = fromUserId,
                ToUser = toUserId,
                Status = status
            };

            Assert.Equal(id, request.Id);
            Assert.Equal(fromUserId, request.FromUser);
            Assert.Equal(toUserId, request.ToUser);
            Assert.Equal(status, request.Status);
        }
    }
}
