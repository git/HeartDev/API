using Model;
using Xunit;

namespace Model.Tests
{
    public class AthleteTests
    {
        [Fact]
        public void CheckAdd_ValidUser_ReturnsTrue()
        {
            var athlete = new Athlete();

            var user = new User("hello13", "dkjd.png", "John", "Doe", "john@exemple.com", "password", 'M', 180.5f, 75.3f, new System.DateTime(1990, 5, 15), new Athlete());
            var result = athlete.CheckAdd(user);

            Assert.True(result);
        }

        [Fact]
        public void CheckAdd_CoachUser_ReturnsFalse()
        {
            var athlete = new Athlete();

            var user = new User("hello13", "dkjd.png", "John", "Doe", "john@exemple.com", "password", 'M', 180.5f, 75.3f, new System.DateTime(1990, 5, 15), new Coach());
            var result = athlete.CheckAdd(user);

            Assert.False(result);
        }
    }
}