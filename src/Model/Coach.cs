namespace Model;

public class Coach : Role
{ 
    public override bool CheckAdd(User user)
    {
        return user.Role is Athlete;
    }

    public override string ToString()
    {
        return "CoachAthlete";
    }
}