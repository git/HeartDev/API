namespace Model;

public abstract class Role
{
    protected List<User> UsersList { get; set; } = [];
    protected List<RelationshipRequest> UsersRequests { get; set; } = [];
    protected List<Training> TrainingList { get; set; } = [];
    public abstract bool CheckAdd(User user);
    
    public bool AddUser(User user)
    {
        if (!CheckAdd(user)) return false;
        UsersList.Add(user);
        return true;
    }
    
    public bool RemoveUser(User user)
    {
        return UsersList.Remove(user);
    }

    public void AddTraining(Training training)
    {
        TrainingList.Add(training);
    }

    public bool RemoveTraining(Training training)
    {
        return TrainingList.Remove(training);  
    }

    public void AddUserRequest(RelationshipRequest request)
    {
        UsersRequests.Add(request);
    }
    
    public bool RemoveUserRequest(RelationshipRequest request)
    {
        return UsersRequests.Remove(request);

    }
}