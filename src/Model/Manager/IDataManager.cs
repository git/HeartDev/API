using Dto.Tiny;
using Model.Repository;

namespace Model.Manager;

public interface IDataManager
{
    IUserRepository UserRepo { get; }
    IActivityRepository ActivityRepo { get; }
    
    IDataSourceRepository<DataSourceTinyDto> DataSourceRepo { get; }
}
