namespace Model;

public class RelationshipRequest  // : Observable
{
    public int Id { get ; set ; }
    public int FromUser { get ; set; }
    public int ToUser { get; set ; }
    public required string Status { get ; set; }
}