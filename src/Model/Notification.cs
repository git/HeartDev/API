namespace Model;

public class Notification
{

    public int IdNotif { get; private set; } 
    public string Message { get; set; }
    public DateTime Date { get; set; }
    public bool Statut { get; set; }
    public string Urgence { get; set; }
    public int ToUserId { get; set; }

    public Notification(int id,string message, DateTime date, bool statut, string urgence, int toUserId)
    {
        this.IdNotif = id;
        this.Message = message;
        this.Date = date;
        this.Statut = statut;
        this.Urgence = urgence;
        this.ToUserId = toUserId;
    }
    
    public Notification(){}
    
}