using Model.Repository;

namespace Model;

public static class EnumMappeur
{
    public static Shared.AthleteOrderCriteria ToEnum(this IUserRepository repository, string? value)
    {
        return value switch
        {
            "None" => Shared.AthleteOrderCriteria.None,
            "ById" => Shared.AthleteOrderCriteria.ById,
            "ByUsername" => Shared.AthleteOrderCriteria.ByUsername,
            "ByFirstName" => Shared.AthleteOrderCriteria.ByFirstName,
            "ByLastName" => Shared.AthleteOrderCriteria.ByLastName,
            "BySexe" => Shared.AthleteOrderCriteria.BySexe,
            "ByLenght" => Shared.AthleteOrderCriteria.ByLenght,
            "ByWeight" => Shared.AthleteOrderCriteria.ByWeight,
            "ByDateOfBirth" => Shared.AthleteOrderCriteria.ByDateOfBirth,
            "ByEmail" => Shared.AthleteOrderCriteria.ByEmail,
            "ByRole" => Shared.AthleteOrderCriteria.ByRole,
            _ => Shared.AthleteOrderCriteria.None
        };
    }
}