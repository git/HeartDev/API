using Dto;
using Dto.Tiny;
using Shared;

namespace Model.Repository;

public interface IActivityRepository
{
    public Task<IEnumerable<ActivityTinyDto>?> GetActivities(int index, int count, ActivityOrderCriteria criteria, bool descending = false);
    public Task<Activity?> GetActivityByIdAsync(int id);
    public Task<ResponseActivityDto?> GetActivityById(int id);
    public Task<Activity?> AddActivity(Activity activity);
    public Task<ResponseActivityDto?> AddActivity(NewActivityDto activity);

    public Task<ResponseActivityDto?> UpdateActivity(int id, ActivityTinyDto activity);
    public Task<bool> DeleteActivity(int id);
    public Task<int> GetNbItems();
    public Task<IEnumerable<Activity>?> GetActivitiesByUser(int userId, int index, int count, ActivityOrderCriteria orderCriteria, bool descending= false);
    public Task<int> GetNbActivitiesByUser(int userId);
}