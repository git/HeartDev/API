namespace Model.Repository;

public interface IDataSourceRepository<T>
{
    Task<T?> GetItemById(int id);
}