﻿using Dto;
using Dto.Tiny;
using Shared;

namespace Model.Repository;

public interface IUserRepository : IGenericRepository<User> // Make it generic
{
    // [TODO] [Dave] DELETE it use just in the test
    public Task<IEnumerable<User>?> GetUsers(int index, int count, AthleteOrderCriteria? criteria , bool descending = false);
    public Task<IEnumerable<UserTinyDto>?> GetUsersTiny(int index, int count, AthleteOrderCriteria? criteria , bool descending = false);
    public Task<bool> AddFollowing(int fromUser, int toUser); 
    public Task<bool> RemoveFollowing(int fromUser, int toUser);
    
    // DELETE
    public Task<IEnumerable<UserTinyDto>?> GetFriends(int user, int index, int count, AthleteOrderCriteria? criteria, bool descending = false);
    public Task<int> GetNbFriends(int user);
    public Task<UserTinyDto?> UpdateUser(int old,UserTinyDto user);
    public Task<ResponseUserDto> GetUserById(int id);
    public Task<UserTinyDto?> GetUserTinyById(int id);
    public Task<IEnumerable<User>?> GetAllAthletes(int index, int count, AthleteOrderCriteria? criteria, bool descending = false);
    public Task<IEnumerable<User>?> GetAllCoaches(int index, int count, AthleteOrderCriteria? criteria, bool descending = false);
    
}