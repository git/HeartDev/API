﻿namespace Model;

public class Athlete : Role
{
    public override bool CheckAdd(User user)
    {
        return user.Role is Athlete;
    }
}