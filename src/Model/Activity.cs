using System.ComponentModel.DataAnnotations;

namespace Model;
public class Activity
    {
        public int Id { get; set; }
        public string? Type { get; set; }
        public DateTime Date { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        
        private int _effort;
        
        [Range(0, 5)]
        public int Effort
        {
            get => _effort;
            set
            {
                if (value < 0 || value > 5)
                {
                    throw new ArgumentException("Effort must be between 0 and 5.");
                }
                _effort = value;
            }
        }
        public float Variability { get; set; }
        public float Variance { get; set; }
        public float StandardDeviation { get; set; }
        public float Average { get; set; }
        public int Maximum { get; set; }
        public int Minimum { get; set; }
        public float AverageTemperature { get; set; }
        public bool HasAutoPause { get; set; }
        
        public User Athlete { get; set; }
        public DataSource? DataSource { get; set; }
        public List<HeartRate> HeartRates { get; set; } = new List<HeartRate>();
        
        public Activity(int id, string type, DateTime date, DateTime startTime, DateTime endTime, int effort, float variability, float variance, float standardDeviation, float average, int maximum, int minimum, float averageTemperature, bool hasAutoPause, User user, DataSource dataSource, List<HeartRate> heartRates)
        {
            Id = id;
            Type = type;
            Date = date;
            StartTime = startTime;
            EndTime = endTime;
            Effort = effort;
            Variability = variability;
            Variance = variance;
            StandardDeviation = standardDeviation;
            Average = average;
            Maximum = maximum;
            Minimum = minimum;
            AverageTemperature = averageTemperature;
            HasAutoPause = hasAutoPause;
            Athlete = user;
            DataSource = dataSource;
            HeartRates = heartRates;
        }
        public Activity(int id, string type, DateTime date, DateTime startTime, DateTime endTime, int effort, float variability, float variance, float standardDeviation, float average, int maximum, int minimum, float averageTemperature, bool hasAutoPause, User user){
            Id = id;
            Type = type;
            Date = date;
            StartTime = startTime;
            EndTime = endTime;
            Effort = effort;
            Variability = variability;
            Variance = variance;
            StandardDeviation = standardDeviation;
            Average = average;
            Maximum = maximum;
            Minimum = minimum;
            AverageTemperature = averageTemperature;
            HasAutoPause = hasAutoPause;
            Athlete = user;
        }
        public Activity(){}
        
        public override string ToString()
        {
            return $"Activity #{Id}: {Type} on {Date:d/M/yyyy} from {StartTime:HH:mm:ss} to {EndTime:HH:mm:ss}" +
                   $" with an effort of {Effort}/5 and an average temperature of {AverageTemperature}°C" +
                   $" and a heart rate variability of {Variability} bpm" +
                   $" and a variance of {Variance} bpm" +
                   $" and a standard deviation of {StandardDeviation} bpm" +
                   $" and an average of {Average} bpm" +
                   $" and a maximum of {Maximum} bpm" +
                   $" and a minimum of {Minimum} bpm" +
                   $" and auto pause is {(HasAutoPause ? "enabled" : "disabled")}.";
        }
    }