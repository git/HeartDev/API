namespace Model;

public class User
{
    public int Id { get; set; }
    public string Username { get; set; }
    public string ProfilePicture { get; set; } = "";
    public string LastName { get; set; }
    public string FirstName { get; set; }
    public string Email { get; set; }
    public string? MotDePasse { get; set; }
    public char Sexe { get; set; }
    public float Lenght { get; set; }
    public float Weight { get; set; }
    public DateTime DateOfBirth { get; set; }
    public Role Role { get; set; }
    
    public LargeImage Image { get; set; } = new LargeImage("");
    
    public List<Notification> Notifications { get; set; } = new List<Notification>();
    public List<Activity> Activities { get; set; } = new List<Activity>(); 
    public List<User> Users { get; set; } = new List<User>();
    public List<DataSource> DataSources { get; set; } = new List<DataSource>();
    
    public User( string username, string profilePicture, string nom, string prenom, string email, string motDePasse, char sexe, float taille, float poids, DateTime dateNaissance, Role role)
    {
        Username = username;
        ProfilePicture = profilePicture;
        LastName = nom;
        FirstName = prenom;
        Email = email;
        MotDePasse = motDePasse;
        Sexe = sexe;
        Lenght = taille;
        Weight = poids;
        DateOfBirth = dateNaissance;
        Role = role;
    }
    
    public User(){}
    
}