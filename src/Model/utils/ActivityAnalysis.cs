using Model.utils;

namespace Dto.Tiny;

public class ActivityAnalysis
{
     public double AverageHeartRate { get; private set; }
    public string AverageHeartRateAdvice { get; private set; }
    public double Vo2Max { get; private set; }
    public string Vo2MaxAdvice { get; private set; }
    public double NormalBpm { get; private set; }
    public string NormalBpmAdvice { get; private set; }
    public double HeartRateVariability { get; private set; }
    public string HeartRateVariabilityAdvice { get; private set; }
    public string HeartRateZone { get; private set; }
    public string HeartRateZoneAdvice { get; private set; }
    public double Duration { get; private set; }
    public string DurationAdvice { get; private set; }
    public double Effort { get; private set; }
    public string EffortAdvice { get; private set; }
    public static ActivityAnalysis FromActivityData(ResponseActivityDto activity)
    {
        double dureeActivity = (activity.EndTime - activity.StartTime).TotalMinutes;
        var age = DateTime.Today.Year - activity.Athlete.DateOfBirth.Year;
        var gender = activity.Athlete.Sexe;
        var poids = activity.Athlete.Weight;
        var effortFelt = activity.EffortFelt;
        var averageHeartRate = activity.Average;
        var heartRateVariability = activity.Variability;

        var heartRateZones = HeartRateAdvise.CalculateHeartRateZones(age);

        var effortScore = HeartRateAdvise.EvaluateEffort(activity);

        var (seuilBPM, vo2Max) = HeartRateAdvise.SeuilBPMavance(gender, age, poids, dureeActivity);
        string averageHeartRateAdvice = HeartRateAdvise.GenerateAverageHeartRateAdvice(averageHeartRate, seuilBPM);
        string vo2MaxAdvice = HeartRateAdvise.GenerateVo2MaxAdvice(vo2Max);
        string normalBpmAdvice = HeartRateAdvise.GenerateNormalBpmAdvice(seuilBPM, averageHeartRate);
        string hrvAdvice = HeartRateAdvise.GenerateHrvAdvice(heartRateVariability);
        HeartRateAdvise.HeartRateZone currentZone = heartRateZones.Find(zone => averageHeartRate >= zone.MinHeartRate && averageHeartRate <= zone.MaxHeartRate);
        string heartRateZoneAdvice = HeartRateAdvise.GenerateHeartRateZoneAdvice(currentZone?.Name);
        var effortAccuracy = HeartRateAdvise.CompareEffort(effortFelt, (int)effortScore);
        var analysis = new ActivityAnalysis
        {
            AverageHeartRate = averageHeartRate,
            AverageHeartRateAdvice = averageHeartRateAdvice,
            Vo2Max = vo2Max,
            Vo2MaxAdvice = vo2MaxAdvice,
            NormalBpm = seuilBPM,
            NormalBpmAdvice = normalBpmAdvice,
            HeartRateVariability = heartRateVariability,
            HeartRateVariabilityAdvice = hrvAdvice,
            HeartRateZone = currentZone != null ? currentZone.Name : "N/A",
            HeartRateZoneAdvice = heartRateZoneAdvice,
            Duration = dureeActivity,
            DurationAdvice =HeartRateAdvise.GenerateDurationAdvice(dureeActivity),
            Effort = effortScore,
            EffortAdvice = HeartRateAdvise.GenerateEffortAdvice(effortAccuracy)
        };

        return analysis;
    }
}