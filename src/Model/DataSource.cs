namespace Model;

public class DataSource
{
    public int Id { get; set; }
    public string Type { get; set; }
    public string Model { get; set; }
    public float Precision { get; set; }
    public ICollection<Activity> Activities { get; set; } = new List<Activity>();
    public ICollection<User> Athletes { get; set; }
    
    public DataSource(int id, string type, string model, float precision, ICollection<User> athletes, ICollection<Activity>? activities)
    {
        Id = id;
        Type = type;
        Model = model;
        Precision = precision;
        Athletes = athletes;
        Activities = activities;
    }
    public DataSource(string type, string model, float precision, ICollection<User> athletes, ICollection<Activity>? activities)
        : this(0, type, model, precision, athletes, activities)
    {}
    
    public override string ToString()
    {
        return $"DataSource #{Id}: {Type} {Model} with a precision of {Precision}";
    }
}