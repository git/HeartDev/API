namespace Model;

public class HeartRate
{
    public int Id { get; set; }
    public int Bpm { get; set; }
    public TimeOnly Timestamp { get; set; }
    public Activity Activity { get; set; }
    public double? Latitude { get; set; }
    public double? Longitude { get; set; }
    public double? Altitude { get; set; }
    public int? Cadence { get; set; }
    public double? Distance { get; set; }
    public double? Speed { get; set; }
    public int? Power { get; set; }
    public double? Temperature { get; set; }
    
    public HeartRate(int id, int bpm, TimeOnly timestamp, Activity activity, double? latitude, double? longitude, double? altitude, int? cadence, double? distance, double? speed, int? power, double? temperature)
    {
        Id = id;
        Bpm = bpm;
        Timestamp = timestamp;
        Activity = activity;
        Latitude = latitude;
        Longitude = longitude;
        Altitude = altitude;
        Cadence = cadence;
        Distance = distance;
        Speed = speed;
        Power = power;
        Temperature = temperature;
    }
    
    
    public HeartRate(int bpm, TimeOnly timestamp, Activity activity, double? latitude, double? longitude, double? altitude, int? cadence, double? distance, double? speed, int? power, double? temperature)
        : this(0, bpm, timestamp, activity, latitude, longitude, altitude, cadence, distance, speed, power, temperature)
    {}
    
    public HeartRate(int bpm, TimeOnly timestamp, int activity, double? latitude, double? longitude, double? altitude, int? cadence, double? distance, double? speed, int? power, double? temperature)
        : this(0, bpm, timestamp, null, latitude, longitude, altitude, cadence, distance, speed, power, temperature)
    {}
    
    public override string ToString()
    {
        return $"HeartRate #{Id}: {Bpm} bpm at {Timestamp:HH:mm:ss} with a temperature of {Temperature}°C" +
               $" and an altitude of {Altitude}m at {Longitude}°E and {Latitude}°N";
    }
}