namespace Shared;

public enum HeartRateOrderCriteria
{
    None,
    ByDate,
    ByValue,
    ByActivity,
    ByUser
}