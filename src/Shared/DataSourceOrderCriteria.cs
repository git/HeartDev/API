namespace Shared;

public enum DataSourceOrderCriteria
{
    None,
    ByName,
    ByDate,
    ByType,
    ByContent
}