namespace Shared;

public enum ActivityOrderCriteria
{
    None,
    ByIdActivity,
    ByType,
    ByDate,
    ByStartTime,
    ByEndTime,
    ByEffortFelt,
    ByVariability,
    ByVariance,
    ByStandardDeviation,
    ByAverage,
    ByMaximum,
    ByMinimum,
    ByAverageTemperature,
    ByHasAutoPause,
    ByDataSourceId,
    ByAthleteId
}