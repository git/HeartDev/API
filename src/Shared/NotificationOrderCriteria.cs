namespace Shared;

public enum NotificationOrderCriteria
{
    None,
    ByDate,
    ByType,
    BySender,
    ByReceiver,
    ByContent
}