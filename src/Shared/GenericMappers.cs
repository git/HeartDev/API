namespace Shared;

public class GenericMapper<T,U> where T : class where U : class
{
    private HashSet<Tuple<T,U>> mapper = new HashSet<Tuple<T,U>>();
    public T? GetT(U u)
    {
        var found = mapper.Where(t => ReferenceEquals(t.Item2, u));
        if (found.Count() != 1)
        {
            return null;
        }
        return found.First().Item1;
    }
    public U? GetU(T t)
    {
        var found = mapper.Where(u => ReferenceEquals(u.Item1, t));
        if (found.Count() != 1)
        {
            return null;
        }
        return found.First().Item2;
    }
    public void Add(T model, U entity) 
    {
        var tuple = new Tuple<T, U>(model, entity);
        mapper.Add(tuple);
    }

    public void Reset()
    {
        mapper.Clear();
    }
}