namespace Shared;

public interface IGenericRepository<T>
{
    Task<IEnumerable<T>> GetItems(int index, int count, string? orderingProperty = null, bool descending = false);
    Task<T?> GetItemById(int id);
    Task<T?> UpdateItem(int oldItem, T newItem);
    Task<T?> AddItem(T item);
    Task<bool> DeleteItem(int item);
    Task<int> GetNbItems();
    
}