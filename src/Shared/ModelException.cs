namespace Shared;

public class FriendShipException : ModelNotFoundException
{
    public FriendShipException(string message) : base(message)
    {
    }
}

public class ModelNotFoundException : Exception
{
    public ModelNotFoundException(string message) : base(message)
    {
    }
}
