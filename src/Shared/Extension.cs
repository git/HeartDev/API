namespace Shared;

public static class Extensions
{
    public static U ToU<T, U>(this T t, GenericMapper<T, U> mapper, Func<T, U> func,Action<T, U>? action = null,bool useMapper = true) where U :class where  T :class
    {
        var res = mapper.GetU(t);
        if (res != null) return res;
        
        U u = func(t);
        if(useMapper) mapper.Add(t, u);
        if(action != null) action(t, u);
        return u;
    }
// , Action<T, U> action
    public static T ToT<T,U>(this U u, GenericMapper<T, U> mapper, Func<U, T> func,Action<U, T>? action = null,bool useMapper = true) where U :class where  T :class
    {
        var result = mapper.GetT(u);

        if(result != null) return result;

        T t = func(u);
        if(useMapper) mapper.Add(t, u);
        if(action != null) action(u, t);
        
        return t;
    }
    
    public static void AddRange<T>(this ICollection<T> set, IEnumerable<T> ts)
    {
        foreach(var t in ts)
        {
            set.Add(t);
        }
    }
}