﻿namespace Shared
{
	public enum AthleteOrderCriteria
	{
        None,
        ById,
        ByUsername,
        ByFirstName,
        ByLastName,
        ByEmail,
        BySexe,
        ByLenght,
        ByWeight,
        ByDateOfBirth,
        ByRole
    }
	
}
