namespace Shared;

public enum TrainingOrderCriteria
{
    None,
    ByDate,
    ByType,
    ByDuration,
    ByDistance,
    ByCalories,
    ByUser
}