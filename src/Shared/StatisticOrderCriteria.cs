namespace Shared;

public enum StatisticOrderCriteria
{
    None,
    ByDate,
    ByType,
    ByValue,
    ByUser
}