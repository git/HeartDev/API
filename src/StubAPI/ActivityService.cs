using Dto;
using Dto.Tiny;
using Model;
using Model.Repository;
using Shared;

namespace StubAPI;

public class ActivityService: IActivityRepository
{
    private List<ActivityTinyDto> _activities = new List<ActivityTinyDto>(
        new ActivityTinyDto[]
        {
            new ActivityTinyDto
            {
                Id = 1,
                    Type = "Running",
                    Date = new DateTime(2021, 10, 10),
                    StartTime = new DateTime(2021, 10, 10, 10, 0, 0),
                    EndTime = new DateTime(2021, 10, 10, 11, 0, 0),
                    EffortFelt = 3,
                    Variability = 0.5f,
                    Variance = 0.5f,
                    StandardDeviation = 0.5f,
                    Average = 5.0f,
                    Maximum = 10,
                    Minimum = 0,
                    AverageTemperature = 20.0f,
                    HasAutoPause = false,
            },
        }
    );
    
    public async Task<IEnumerable<ActivityTinyDto>?> GetActivities(int index, int count, ActivityOrderCriteria criteria, bool descending = false)
    => await Task.FromResult(_activities.GetItemsWithFilterAndOrdering(a => true, index, count, criteria, descending));

    public Task<Activity?> GetActivityByIdAsync(int id)
    {
        throw new NotImplementedException();
        // return Task.FromResult(_activities.FirstOrDefault(s => s.Id == id)?.ToModel());
    }

    public async Task<ResponseActivityDto?> GetActivityById(int id)
    {
        throw new NotImplementedException();
    }

    public Task<Activity?> AddActivity(Activity activity)
    =>         throw new NotImplementedException();


    public async Task<ResponseActivityDto?> AddActivity(NewActivityDto activity)
    {
        throw new NotImplementedException();
    }

    public async Task<ResponseActivityDto?> UpdateActivity(int id, ActivityTinyDto activity)
    {
        throw new NotImplementedException();
    }


    public async Task<Activity?> UpdateActivity(int id, Activity activity)
    {
        throw new NotImplementedException();

        /*var oldActivity = _activities.FirstOrDefault(s => s.Id == id);
        if (oldActivity == null) return null;
        return await _activities.UpdateItem(oldActivity, activity);*/
    }

    public Task<bool> DeleteActivity(int id)
    {
        var activity = _activities.FirstOrDefault(s => s.Id == id);
        if (activity == null) return Task.FromResult(false);
        return _activities.DeleteItem(activity);
    }

    public Task<int> GetNbItems()
    => Task.FromResult(_activities.Count);

    public async Task<IEnumerable<Activity>?> GetActivitiesByUser(int userId, int index, int count, ActivityOrderCriteria criteria, bool descending = false)
    {
        throw new NotImplementedException();

       /* var activities = _activities.GetItemsWithFilterAndOrdering(a => a.Athlete.Id == userId, index, count,
            criteria != ActivityOrderCriteria.None ? criteria : null, descending);
        return await Task.FromResult(activities);*/
    }

    public Task<int> GetNbActivitiesByUser(int userId)
    {
        throw new NotImplementedException();
        // return Task.FromResult(_activities.Count(a => a.Athlete.Id == userId));
    }
}