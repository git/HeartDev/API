using Dto.Tiny;
using Model.Manager;
using Model.Repository;

namespace StubAPI;

public class StubData : IDataManager
{
    public IUserRepository UserRepo { get; }
    public IActivityRepository ActivityRepo { get; }
    public IDataSourceRepository<DataSourceTinyDto> DataSourceRepo { get; }

    public StubData()
    {
        UserRepo = new UserService();
        ActivityRepo = new ActivityService();
    }
}