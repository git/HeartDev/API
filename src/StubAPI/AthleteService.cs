﻿using Dto;
using Dto.Tiny;
using Model;
using Model.Repository;
using Shared;

namespace StubAPI;

public class UserService : IUserRepository
{
    private List<User> athletes =
    [
        new User
        {
            Id = 1, Username = "DoeDoe", ProfilePicture = "https://images.unsplash.com/photo-1682687982134-2ac563b2228b?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D", FirstName = "John", LastName = "Doe",
            Sexe = 'M', Lenght = 180, Weight = 70, DateOfBirth = new DateTime(1990, 1, 1),
            Email = "john.doe@example.com", Role = new Athlete()
        },

        new User
        {
            Id = 2, Username = "SmithSmith", ProfilePicture = "https://images.unsplash.com/photo-1709507779917-242b560288be?q=80&w=2080&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D", FirstName = "Jane", LastName = "Smith",
            Sexe = 'F', Lenght = 170, Weight = 60, DateOfBirth = new DateTime(1992, 2, 2),
            Email = "athlete2@example.com", Role = new Coach()
        },

        new User
        {
            Id = 3, Username = "Athlete3", ProfilePicture = "https://plus.unsplash.com/premium_photo-1705091981693-6006f8a20479?q=80&w=1974&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D", FirstName = "First3", LastName = "Last3",
            Sexe = 'M', Lenght = 190, Weight = 80, DateOfBirth = new DateTime(1994, 3, 3), Email = "ath@ex.fr",
            Role = new Athlete()
        }

    ];
    
    public async Task<IEnumerable<User>> GetUsers(int index, int count, AthleteOrderCriteria? orderingProperty = null, bool descending = false)
        => athletes.GetItemsWithFilterAndOrdering(c=>true,index, count,orderingProperty != AthleteOrderCriteria.None ? orderingProperty: null , descending);

    public async Task<IEnumerable<UserTinyDto>?> GetUsersTiny(int index, int count, AthleteOrderCriteria? criteria, bool descending = false)
    {
        throw new NotImplementedException();
    }

    public async Task<bool> AddFollowing(int fromUser, int toUser)
    {
        throw new NotImplementedException();
    }

    public async Task<bool> RemoveFollowing(int fromUser, int toUser)
    {
        throw new NotImplementedException();
    }

    public async Task<IEnumerable<UserTinyDto>?> GetFriends(int user, int index, int count, AthleteOrderCriteria? criteria, bool descending = false)
    {
        throw new NotImplementedException();
    }

    public async Task<bool> AddFriend(User user, User friend)
    {
        if (user == null || friend == null)
        {
            return false;
        }

        if (user.Users.Contains(friend))
        {
            return false;
        }
        
        user.Users.Add(friend);
        
        return true; 
    }

    public async Task<bool> RemoveFriend(User user, User friend)
    {
        if (user == null || friend == null)
        {
            return false;
        }

        if (!user.Users.Contains(friend))
        {
            return false;
        }
        
        user.Users.Remove(friend);
        
        return true;
    }

    public async Task<IEnumerable<User>?>? GetFriends(User user, int index, int count, AthleteOrderCriteria? criteria, bool descending = false)
        =>await Task.FromResult(athletes.FirstOrDefault(s => s.Id == user.Id)?.Users.GetItemsWithFilterAndOrdering(c=>true,index, count,criteria, descending));

    public Task<int> GetNbFriends(int user)
    {
        return Task.FromResult(athletes.FirstOrDefault(s => s.Id == user)?.Users.Count ?? 0);
    }

    public async Task<UserTinyDto?> UpdateUser(int old, UserTinyDto user)
    {
        throw new NotImplementedException();
    }

    public async Task<ResponseUserDto> GetUserById(int id)
    {
        throw new NotImplementedException();
    }

    public Task<UserTinyDto?> GetUserTinyById(int id)
    {
        throw new NotImplementedException();
    }

    public async Task<IEnumerable<User>> GetItems(int index, int count, string? orderingProperty = null,
        bool descending = false)
    =>await GetUsers(index, count, this.ToEnum(orderingProperty), descending);
        

    public async Task<User?> GetItemById(int id)
    =>await Task.FromResult(athletes.FirstOrDefault(s => s.Id == id));


    public async Task<User?> AddItem(User user)
    {
       return await athletes.AddItem(user);
    }
    
    public async Task<User?> UpdateItem(int id, User user)
    {
        var oldUser = athletes.FirstOrDefault(s => s.Id == id);
        if (oldUser == null) return null;
        return await athletes.UpdateItem(oldUser, user);
    }

    public async Task<bool> DeleteItem(int id)
    {
        var user = athletes.FirstOrDefault(s => s.Id == id);
        if (user == null) return false;
        return await athletes.DeleteItem(user);
    }

    public async Task<int> GetNbItems()
    {
        return await Task.FromResult(athletes.Count);
    }
    
    public async Task<IEnumerable<User>> GetAllAthletes(int index, int count, AthleteOrderCriteria? criteria, bool descending = false)
    {
        return await Task.FromResult(athletes.GetItemsWithFilterAndOrdering(c=>c.Role is Athlete,index, count,criteria, descending));
    }
    
    public async Task<IEnumerable<User>> GetAllCoaches(int index, int count, AthleteOrderCriteria? criteria, bool descending = false)
    {
        return await Task.FromResult(athletes.GetItemsWithFilterAndOrdering(c=>c.Role is Coach,index, count,criteria, descending));
    }
}