﻿using Dto;
using Dto.Tiny;
using Entities;
using Shared;

namespace Entities2Dto;

public static class ActivityMapper
{
    private static GenericMapper<ActivityTinyDto, ActivityEntity> _mapper = new ();
    
    private static GenericMapper<ResponseActivityDto, ActivityEntity> _mapperFull = new ();
    
    public static void Reset()
    {
        _mapper.Reset();
        _mapperFull.Reset();
    }
    
    public static ActivityTinyDto ToDto(this ActivityEntity entity)
    {
        Func<ActivityEntity, ActivityTinyDto> create = activityEntity => new ActivityTinyDto
        {
            Id = activityEntity.IdActivity,
            Type = activityEntity.Type,
            Date = activityEntity.Date.ToDateTime(TimeOnly.MinValue),
            StartTime = activityEntity.Date.ToDateTime(activityEntity.StartTime),
            EndTime = activityEntity.Date.ToDateTime(activityEntity.EndTime),
            EffortFelt = activityEntity.EffortFelt,
            Variability = activityEntity.Variability,
            Variance = activityEntity.Variance,
            StandardDeviation = activityEntity.StandardDeviation,
            Average = activityEntity.Average,
            Maximum = activityEntity.Maximum,
            Minimum = activityEntity.Minimum,
            AverageTemperature = activityEntity.AverageTemperature,
            HasAutoPause = activityEntity.HasAutoPause
        };
        return entity.ToT(_mapper, create, null, false);
    }
    
    public static ActivityEntity ToEntity(this ActivityTinyDto dto)
    {
        Func<ActivityTinyDto, ActivityEntity> create = activity => new ActivityEntity
        {
            Type = activity.Type,
            Date = DateOnly.FromDateTime(activity.Date),
            StartTime = TimeOnly.FromDateTime(activity.StartTime),
            EndTime = TimeOnly.FromDateTime(activity.EndTime),
            EffortFelt = activity.EffortFelt,
            Variability = activity.Variability,
            Variance = activity.Variance,
            StandardDeviation = activity.StandardDeviation,
            Average = activity.Average,
            Maximum = activity.Maximum,
            Minimum = activity.Minimum,
            AverageTemperature = activity.AverageTemperature,
            HasAutoPause = activity.HasAutoPause
        };
        return dto.ToU(_mapper, create);
    }
    
    public static ResponseActivityDto ToResponseDto(this ActivityEntity entity)
    {
        Func<ActivityEntity, ResponseActivityDto> create = activityEntity => new ResponseActivityDto
        {
            Id = activityEntity.IdActivity,
            Type = activityEntity.Type,
            Date = activityEntity.Date.ToDateTime(TimeOnly.MinValue),
            StartTime = activityEntity.Date.ToDateTime(activityEntity.StartTime),
            EndTime = activityEntity.Date.ToDateTime(activityEntity.EndTime),
            EffortFelt = activityEntity.EffortFelt,
            Variability = activityEntity.Variability,
            Variance = activityEntity.Variance,
            StandardDeviation = activityEntity.StandardDeviation,
            Average = activityEntity.Average,
            Maximum = activityEntity.Maximum,
            Minimum = activityEntity.Minimum,
            AverageTemperature = activityEntity.AverageTemperature,
            HasAutoPause = activityEntity.HasAutoPause
        };
        
        Action<ActivityEntity, ResponseActivityDto> linker = (activityEntity, activity) =>
        {
            if (activityEntity.HeartRates != null) activity.HeartRates = activityEntity.HeartRates.ToTinyDtos().ToArray();
            activity.DataSource = activityEntity.DataSource != null ? activityEntity.DataSource.ToTinyDto() : null;
            activity.Athlete = activityEntity.Athlete.ToTinyDto();
        };
        
        return entity.ToT(_mapperFull, create, linker, false);
    }
    
    public static IEnumerable<ActivityTinyDto> ToTinyDtos(this IEnumerable<ActivityEntity> entities)
        => entities.Select(a => a.ToDto());

    public static IEnumerable<ActivityEntity> ToEntities(this IEnumerable<ActivityTinyDto> dtos)
        => dtos.Select(a => a.ToEntity());
    
    

}