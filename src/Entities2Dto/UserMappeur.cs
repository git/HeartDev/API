using Dto;
using Dto.Tiny;
using Entities;
using Shared;

namespace Entities2Dto;

public static class UserMappeur
{
    private static GenericMapper<UserTinyDto, AthleteEntity> _mapper = new();
    private static GenericMapper<ResponseUserDto, AthleteEntity> _mapperFull = new();

    
    public static void Reset()
    {
        _mapper.Reset();
        _mapperFull.Reset();
    }
    
    public static UserTinyDto ToTinyDto(this AthleteEntity entity)
    {
        Func<AthleteEntity, UserTinyDto> create = athleteEntity => new UserTinyDto
        {
            Id = athleteEntity.Id,
            FirstName = athleteEntity.FirstName,
            LastName = athleteEntity.LastName,
            Email = athleteEntity.Email,
            Password = athleteEntity.PasswordHash,
            DateOfBirth = athleteEntity.DateOfBirth.ToDateTime(TimeOnly.MinValue),
            Sexe = athleteEntity.Sexe,
            Username = athleteEntity.UserName,
            Weight = athleteEntity.Weight,
            Length = (float)athleteEntity.Length,
            ProfilePicture = athleteEntity.ProfilPicture ?? "",
            IsCoach = athleteEntity.IsCoach
        };
        
        return entity.ToT(_mapper, create, null, false);
    }
    
    public static AthleteEntity ToEntity(this UserTinyDto model)
    {
        Func<UserTinyDto, AthleteEntity> create = user => new AthleteEntity
        {
            Id = user.Id,
            UserName = user.Username,
            Sexe = user.Sexe,
            FirstName = user.FirstName,
            LastName = user.LastName,
            Email = user.Email,
            PasswordHash = user.Password ?? "",
            DateOfBirth = DateOnly.FromDateTime(user.DateOfBirth),
            IsCoach = user.IsCoach,
            Weight = user.Weight,
            Length = user.Length,
            ProfilPicture = user.ProfilePicture
        };
        
        return model.ToU(_mapper, create);
    }
    
    public static ResponseUserDto ToResponseDto(this AthleteEntity entity)
    {
        Func<AthleteEntity, ResponseUserDto> creator = athleteEntity => new ResponseUserDto
        {
            Id = athleteEntity.Id,
            FirstName = athleteEntity.FirstName,
            LastName = athleteEntity.LastName,
            Email = athleteEntity.Email,
            Password = athleteEntity.PasswordHash,
            DateOfBirth = athleteEntity.DateOfBirth.ToDateTime(TimeOnly.MinValue),
            Sexe = athleteEntity.Sexe,
            Username = athleteEntity.UserName,
            Weight = athleteEntity.Weight,
            Lenght = (float)athleteEntity.Length,
            ProfilePicture = athleteEntity.ProfilPicture ?? "",
            IsCoach = athleteEntity.IsCoach,
        };
        
        Action<AthleteEntity, ResponseUserDto> linker = (athleteEntity, userDto) =>
        {
            userDto.Activities = athleteEntity.Activities.ToTinyDtos().ToArray();
            userDto.Image = athleteEntity.Image?.ToDto();
            userDto.DataSource = athleteEntity.DataSource?.ToTinyDto();
            userDto.Followers = athleteEntity.Followers.ToTinyDtos().ToArray();
            userDto.Followings = athleteEntity.Followings.ToTinyDtos().ToArray();
        };
        
        return entity.ToT(_mapperFull, creator, linker, false);
    }
    
    public static IEnumerable<UserTinyDto> ToTinyDtos(this IEnumerable<AthleteEntity> entities)
    => entities.Select(e => e.ToTinyDto());
    
    public static IEnumerable<AthleteEntity> ToEntities(this IEnumerable<UserTinyDto> models)
    => models.Select(m => m.ToEntity());
    
    
    
    
    
}