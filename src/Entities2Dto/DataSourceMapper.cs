using Dto;
using Dto.Tiny;
using Entities;
using Shared;

namespace Entities2Dto;

public static class DataSourceMapper
{
    private static GenericMapper<DataSourceTinyDto, DataSourceEntity> _mapper = new();
    
    private static GenericMapper<ResponseDataSourceDto, DataSourceEntity> _mapperFull = new();
    
    public static void Reset()
    {
        _mapper.Reset();
        _mapperFull.Reset();
    }
    
    public static DataSourceTinyDto ToTinyDto(this DataSourceEntity entity)
    {
        Func<DataSourceEntity, DataSourceTinyDto> create = dataSourceEntity => new DataSourceTinyDto
        {
            Id = dataSourceEntity.IdSource,
            Type = dataSourceEntity.Type,
            Model = dataSourceEntity.Model,
            Precision = dataSourceEntity.Precision
        };
        return entity.ToT(_mapper, create, null,false);
    }
    
    public static DataSourceEntity ToEntity(this DataSourceTinyDto dto)
    {
        Func<DataSourceTinyDto, DataSourceEntity> create = dataSource => new DataSourceEntity
        {
            IdSource = dataSource.Id,
            Type = dataSource.Type,
            Model = dataSource.Model,
            Precision = dataSource.Precision
        };
        return dto.ToU(_mapper, create);
    }
    
    public static ResponseDataSourceDto ToResponseDto(this DataSourceEntity entity)
    {
        Func<DataSourceEntity, ResponseDataSourceDto> create = dataSourceEntity => new ResponseDataSourceDto
        {
            Id = dataSourceEntity.IdSource,
            Type = dataSourceEntity.Type,
            Model = dataSourceEntity.Model,
            Precision = dataSourceEntity.Precision,
        };
        
        Action<DataSourceEntity, ResponseDataSourceDto> linker = (dataSourceEntity, dto) =>
        {
            dto.Activities = dataSourceEntity.Activities.ToTinyDtos().ToArray();
            dto.Users = dataSourceEntity.Athletes.ToTinyDtos().ToArray();
        };
        
        return entity.ToT(_mapperFull, create, linker, false);
    }
    
    public static IEnumerable<DataSourceTinyDto> ToTinyDtos(this IEnumerable<DataSourceEntity> entities)
    => entities.Select(e => e.ToTinyDto());
    
    public static IEnumerable<DataSourceEntity> ToEntities(this IEnumerable<DataSourceTinyDto> dtos)
    => dtos.Select(d => d.ToEntity());
    
    
}