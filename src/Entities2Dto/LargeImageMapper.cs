using Dto;
using Entities;
using Shared;

namespace Entities2Dto;

public static class LargeImageMapper
{
    private static GenericMapper<LargeImageDto, LargeImageEntity> _mapper = new();

    public static void Reset()
    {
        _mapper.Reset();
    }

    public static LargeImageDto ToDto(this LargeImageEntity entity)
    {
        Func<LargeImageEntity, LargeImageDto> create = largeImageEntity => new() { Base64 = largeImageEntity.Base64 };

        return entity.ToT(_mapper, create, null, false);
    }

    public static LargeImageEntity ToEntity(this LargeImageDto dto)
    {
        Func<LargeImageDto, LargeImageEntity> create = largeImage => new LargeImageEntity
        {
            Base64 = largeImage.Base64
        };

        return dto.ToU(_mapper, create);
    }
}