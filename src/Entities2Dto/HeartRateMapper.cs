using Dto.Tiny;
using Entities;
using Shared;

namespace Entities2Dto;

public static class HeartRateMapper
{
    
    private static GenericMapper<HeartRateTinyDto, HeartRateEntity> _mapper = new();
    
    public static void Reset()
    {
        _mapper.Reset();
    }
    
    public static HeartRateTinyDto ToTinyDto(this HeartRateEntity entity)
    {
        var activityTmp = new DateTime();
        Func<HeartRateEntity, HeartRateTinyDto> create = heartRateEntity => new HeartRateTinyDto
        {
            Id = heartRateEntity.IdHeartRate,
            HeartRate = heartRateEntity.Bpm,
            Timestamp = activityTmp,
            Latitude = heartRateEntity.Latitude,
            Longitude = heartRateEntity.Longitude,
            Altitude = heartRateEntity.Altitude,
            Cadence = heartRateEntity.Cadence,
            Distance = heartRateEntity.Distance,
            Speed = heartRateEntity.Speed,
            Power = heartRateEntity.Power,
            Temperature = heartRateEntity.Temperature
        };
        
        return entity.ToT(_mapper, create, null, false);
    }
    
    public static HeartRateEntity ToEntity(this HeartRateTinyDto dto)
    {
        Func<HeartRateTinyDto, HeartRateEntity> create = heartRate => new HeartRateEntity
        {
            IdHeartRate = heartRate.Id,
            Bpm = heartRate.HeartRate,
            Time = TimeOnly.FromDateTime(heartRate.Timestamp),
            Latitude = heartRate.Latitude,
            Longitude = heartRate.Longitude,
            Altitude = heartRate.Altitude,
            Cadence = heartRate.Cadence,
            Distance = heartRate.Distance,
            Speed = heartRate.Speed,
            Power = heartRate.Power,
            Temperature = heartRate.Temperature
        };
        return dto.ToU(_mapper, create);
    }
    
    public static IEnumerable<HeartRateTinyDto> ToTinyDtos(this IEnumerable<HeartRateEntity> entities)
    => entities.Select(e => e.ToTinyDto());
    
    public static IEnumerable<HeartRateEntity> ToEntities(this IEnumerable<HeartRateTinyDto> dtos)
    => dtos.Select(d => d.ToEntity());
    
}