using Dto.Tiny;
using Entities;
using Shared;

namespace Entities2Dto;

public static class FriendshipMapper
{
    private static GenericMapper<FriendshipDto, FriendshipEntity> _mapper = new();

    public static void Reset()
    {
        _mapper.Reset();
    }

    public static FriendshipDto ToTinyDto(this FriendshipEntity entity)
    {
        Func<FriendshipEntity, FriendshipDto> create = friendshipEntity => new FriendshipDto
        {
            FollowedId = friendshipEntity.FollowingId,
            FollowerId = friendshipEntity.FollowerId,
        };

        return entity.ToT(_mapper, create, null, false);
    }
    
    public static IEnumerable<FriendshipDto> ToTinyDtos(this IEnumerable<FriendshipEntity> entities)
    => entities.Select(e => e.ToTinyDto());


    
}