namespace Dto;

public class LargeImageDto
{
    public string Base64 { get; set; }
}