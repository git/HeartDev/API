using System.ComponentModel.DataAnnotations;

namespace Dto.Tiny;

public class UserTinyDto
{
    public int Id { get; set; }
    [MaxLength(100)]
    public required string Username { get; set; }
    [MaxLength(150)]
    public required string LastName { get; set; }
    [MaxLength(100)]
    public required string FirstName { get; set; }
    public required string Email { get; set; }
    public required char Sexe { get; set; }
    public float Length { get; set; }
    public float Weight { get; set; }
    public string? Password { get; set; }
    public DateTime DateOfBirth { get; set; }
    public string ProfilePicture { get; set; } = "https://davidalmeida.site/assets/me_avatar.f77af006.png";
    public bool IsCoach { get; set; }
}