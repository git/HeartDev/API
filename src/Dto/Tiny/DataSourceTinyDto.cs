namespace Dto.Tiny;

public class DataSourceTinyDto
{
    public int Id { get; set; }

    public string Type { get; set; } = "Unknown";
    
    public string Model { get; set; }
    
    public float Precision { get; set; }
}