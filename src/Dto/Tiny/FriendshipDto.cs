namespace Dto.Tiny;

public class FriendshipDto
{
    public int FollowedId { get; set; }
    public int FollowerId { get; set; }
}