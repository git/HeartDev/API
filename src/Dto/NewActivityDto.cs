using Dto.Tiny;

namespace Dto;

public class NewActivityDto
{
    public ActivityTinyDto Activity { get; set; }
    public HeartRateTinyDto[]? HeartRates { get; set; }
    public int? DataSourceId { get; set; }
    public int AthleteId { get; set; }
    
}