using System.Text.Json.Serialization;

namespace Dto;

public class DataSourceDto
{
    public int Id { get; set; }

    public string? Type { get; set; }
    
    public string Model { get; set; }
    
    public float Precision { get; set; }
    
    [JsonIgnore] 
    public IEnumerable<UserDto>? Athletes { get; set; }
    
    [JsonIgnore]
    public IEnumerable<ActivityDto>? Activities { get; set; }
}