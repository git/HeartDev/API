namespace Dto;

public class HeartRateDto
{
    public DateTime Timestamp { get; set; }
    public double? Latitude { get; set; }
    public double? Longitude { get; set; }
    public double? Altitude { get; set; }
    public int HeartRate { get; set; }
    public int? Cadence { get; set; }
    public double? Distance { get; set; }
    public double? Speed { get; set; }
    public int? Power { get; set; }
    public double? Temperature { get; set; }
}