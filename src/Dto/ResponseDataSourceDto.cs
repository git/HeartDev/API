using Dto.Tiny;

namespace Dto;

public class ResponseDataSourceDto
{
    public int Id { get; set; }

    public string Type { get; set; } = "Unknown";
    
    public string Model { get; set; }
    
    public float Precision { get; set; }
    
    public ActivityTinyDto[]? Activities { get; set; }
    
    public UserTinyDto[]? Users { get; set; }
}