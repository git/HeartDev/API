using Dto.Tiny;

namespace Dto;

public class ResponseActivityDto
{
    public int Id { get; set; }
    public string Type { get; set; } = "";
    public DateTime Date { get; set; }
    public DateTime StartTime { get; set; }
    public DateTime EndTime { get; set; }
    public int EffortFelt { get; set; }
    public float Variability { get; set; }
    public float Variance { get; set; }
    public float StandardDeviation { get; set; }
    public float Average { get; set; }
    public int Maximum { get; set; }
    public int Minimum { get; set; }
    public float AverageTemperature { get; set; }
    public bool HasAutoPause { get; set; }
    public HeartRateTinyDto[]? HeartRates { get; set; }
    public DataSourceTinyDto? DataSource { get; set; }
    public UserTinyDto? Athlete { get; set; }
    
}