namespace Dto.Auth;

public class AuthResponseDto
{
    /// <summary>
    /// Gets or sets the access token issued by the OAuth provider.
    /// </summary>
    public string AccessToken { get; set; }

    /// <summary>Gets or sets the token type.</summary>
    /// <remarks>Typically the string “bearer”.</remarks>
    public string? TokenType { get; set; }

    /// <summary>
    /// Gets or sets a refresh token that applications can use to obtain another access token if tokens can expire.
    /// </summary>
    public string? RefreshToken { get; set; }

    /// <summary>
    /// Gets or sets the validatity lifetime of the token in seconds.
    /// </summary>
    public string? ExpiresIn { get; set; }
}