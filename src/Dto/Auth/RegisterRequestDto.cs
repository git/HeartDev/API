using System.ComponentModel.DataAnnotations;

namespace Dto.Auth;

public class RegisterRequestDto
{
    
    [MaxLength(100)]
    [Required(ErrorMessage = "Username is required")]
    public string Username { get; set; }
    [MaxLength(150)]
    [Required(ErrorMessage = "LastName is required")]
    public  string LastName { get; set; }
    [MaxLength(100)]
    [Required(ErrorMessage = "FirstName is required")]
    public  string FirstName { get; set; }
    [Required(ErrorMessage = "Email is required")]
    [EmailAddress]
    public string Email { get; set; }
    [Required(ErrorMessage = "Sexe is required")]
    public char Sexe { get; set; }
    [Required(ErrorMessage = "Size is required")]
    public float Size { get; set; }
    [Required(ErrorMessage = "Weight is required")]
    public float Weight { get; set; }
    [Required(ErrorMessage = "Password is required")]
    public string Password { get; set; }
    [Required(ErrorMessage = "DateOfBirth is required")]
    public DateTime DateOfBirth { get; set; }
    public string ProfilePicture { get; set; } = "https://davidalmeida.site/assets/me_avatar.f77af006.png";
    [Required(ErrorMessage = "Role is required")]
    public bool IsCoach { get; set; }
}