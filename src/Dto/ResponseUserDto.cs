using System.ComponentModel.DataAnnotations;
using Dto.Tiny;

namespace Dto;

public class ResponseUserDto
{
    public int Id { get; set; }
    [MaxLength(100)]
    public required string Username { get; set; }
    [MaxLength(150)]
    public required string LastName { get; set; }
    [MaxLength(100)]
    public required string FirstName { get; set; }
    public required string Email { get; set; }
    public required char Sexe { get; set; }
    public float Lenght { get; set; }
    public float Weight { get; set; }
    public string? Password { get; set; }
    public DateTime DateOfBirth { get; set; }
    public string ProfilePicture { get; set; } = "https://davidalmeida.site/assets/me_avatar.f77af006.png";
    public bool IsCoach { get; set; }
    public LargeImageDto? Image { get; set; }
    public ActivityTinyDto[] Activities { get; set; }
    public DataSourceTinyDto? DataSource { get; set; }
    public FriendshipDto?[] Followers { get; set; }
    public FriendshipDto?[] Followings { get; set; }
}