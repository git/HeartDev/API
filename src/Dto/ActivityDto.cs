using Newtonsoft.Json;

namespace Dto;

public class ActivityDto
{
    public int Id { get; set; }
    public string? Type { get; set; }
    public DateTime Date { get; set; }
    public DateTime StartTime { get; set; }
    public DateTime EndTime { get; set; }
    public int EffortFelt { get; set; }
    public float Variability { get; set; }
    public float Variance { get; set; }
    public float StandardDeviation { get; set; }
    public float Average { get; set; }
    public int Maximum { get; set; }
    public int Minimum { get; set; }
    public float AverageTemperature { get; set; }
    public bool HasAutoPause { get; set; }
    
    [System.Text.Json.Serialization.JsonIgnore]
    public DataSourceDto? DataSource { get; set; }
    
    [JsonProperty("DataSourceId")]
    public int? DataSourceId { get; set; }
    
    [System.Text.Json.Serialization.JsonIgnore]
    public UserDto? Athlete { get; set; }
    
    [JsonProperty("AthleteId")]
    public int AthleteId { get; set; }
    
    // public int? TrainingId { get; set; }
    public IEnumerable<HeartRateDto> HeartRates { get; set; }
}