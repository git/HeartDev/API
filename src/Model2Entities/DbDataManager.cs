﻿using DbContextLib;
using Dto.Tiny;
using EFMappers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Model.Manager;
using Model.Repository;

namespace Model2Entities;

public partial class DbDataManager: IDataManager
{
    public IActivityRepository ActivityRepo { get; }
    public IUserRepository UserRepo { get; }
    
    public IDataSourceRepository<DataSourceTinyDto> DataSourceRepo { get; }
    protected HeartTrackContext DbContext { get; }

    protected readonly ILogger<DbDataManager> _logger = new Logger<DbDataManager>(new LoggerFactory());

   // mettre si pb lors d'une requete si rollback ou pas 
    public DbDataManager(HeartTrackContext dbContext)
    {
        DbContext = dbContext;
        ActivityRepo = new ActivityRepository(this, _logger);
        UserRepo = new UserRepository(this, _logger);
        DataSourceRepo = new DataSourceRepository(this, _logger);
        ActivityMapper.Reset();
        UserMappeur.Reset();
        // Faire pour les autres reset() des autres mappers
    }
    
    public DbDataManager(string dbPlatformPath)
        : this(new HeartTrackContext(dbPlatformPath))
    {}
    
    public DbDataManager()
    {
        DbContext = new HeartTrackContext();
        ActivityRepo = new ActivityRepository(this, _logger);
        UserRepo= new UserRepository(this, _logger);
        DataSourceRepo = new DataSourceRepository(this, _logger);
    }
}
