using Dto.Tiny;
using Entities2Dto;
using Microsoft.Extensions.Logging;
using Model.Repository;

namespace Model2Entities;

public partial class DbDataManager
{
    public class DataSourceRepository : IDataSourceRepository<DataSourceTinyDto>
    {
        private readonly DbDataManager _dataManager;
        private readonly ILogger<DbDataManager> _logger;

        public DataSourceRepository(DbDataManager dbDataManager, ILogger<DbDataManager> logger)
        {
            _dataManager = dbDataManager;
            _logger = logger;
        }


        public async Task<DataSourceTinyDto?> GetItemById(int id)
        {
            var dataSource = await _dataManager.DbContext.DataSourcesSet.FindAsync(id);
            if (dataSource == null)
            {
                _logger.LogInformation($"DataSource with ID {id} not found.");
                return null;
            }

            return dataSource.ToTinyDto();
        }
    }
}