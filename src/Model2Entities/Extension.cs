using DbContextLib;
using Entities;
using Microsoft.EntityFrameworkCore;
using Model;

namespace Model2Entities;


public static class Extensions
{
    internal static async Task<T?> AddItem<T>(this HeartTrackContext context, T? item) where T :class
    {
        if(item == null || context.Set<T>().Contains(item))
        {
            return await Task.FromResult<T?>(null);
        }
        var entry = context.Set<T>().Add(item);
        try {
            await context.SaveChangesAsync();
        } 
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message, ex.InnerException, ex.StackTrace);
        }

        return await Task.FromResult<T?>(entry.Entity);
    }

    internal static async Task<bool> DeleteItem<T>(this HeartTrackContext context, int? id) where T:class
    {
        var item = await context.Set<T>().FindAsync(id);
        if(item == null) return await Task.FromResult(false);
        context.Set<T>().Remove(item);
        await context.SaveChangesAsync();
        return await Task.FromResult(true);
    }
    
    public static async Task<U?> UpdateItem<T, U>(this HeartTrackContext context, int? id, T? newItem, Action<T, U> updateAction) where T : class where U: class
    {
        var existingT = await context.Set<U>().FindAsync(id);
        if (existingT != null && newItem != null)
        {
            updateAction(newItem, existingT);

            context.Update(existingT);
        
            await context.SaveChangesAsync();
            return existingT;
        }
        return Task.FromResult<U?>(null).Result;

    }

    
    public static IEnumerable<T> GetItemsWithFilterAndOrdering<T>(this IEnumerable<T> list, Func<T, bool> filter, int index, int count, Enum? orderCriterium, bool descending = false ) where T : class
    {
        var filteredList = list.Where(filter);
        
        if(orderCriterium != null && orderCriterium.ToString() != "None")
        {
            filteredList = filteredList.OrderByCriteria(orderCriterium, descending);
        }
             
        return filteredList
            .Skip(index * count)
            .Take(count);
    }

    public static IOrderedEnumerable<T> OrderByCriteria<T>(this IEnumerable<T> list, Enum orderCriterium, bool descending = false ) where T : class
    {
        var orderCriteriumString = orderCriterium.ToString();
        if (orderCriteriumString.StartsWith("By"))
        {
            orderCriteriumString = orderCriteriumString.Substring(2);
        }
        var propertyInfo = typeof(T).GetProperty(orderCriteriumString);
        if (propertyInfo == null)
        {
            throw new ArgumentException($"No property {orderCriterium} in type {typeof(T)}");
        }

        return descending ? list.OrderByDescending(x => propertyInfo.GetValue(x)) : list.OrderBy(x => propertyInfo.GetValue(x));
    }
    public static IQueryable<TEntity> IncludeAll<TEntity>(this IQueryable<TEntity> query,  HeartTrackContext dbContext) where TEntity : class
    {
        var entityType = dbContext.Model.FindEntityType(typeof(TEntity));
        foreach (var navigation in entityType.GetNavigations())
        {
            query = query.Include(navigation.Name);
        }
        return query;
    }
    public static IQueryable<ActivityEntity> IncludeStandardProperties(this IQueryable<ActivityEntity> query)
    {
        return query.Include(a => a.HeartRates)
            .Include(a => a.Athlete)
            .Include(a => a.DataSource);
    }
    public static IQueryable<AthleteEntity> IncludeStandardProperties(this IQueryable<AthleteEntity> query)
    {
        return query.Include(a => a.DataSource)
            .Include(a => a.Activities).ThenInclude(ac => ac.HeartRates).Include(ac => ac.Activities).ThenInclude(ac => ac.DataSource)
            .Include(a => a.Image);
    }
    // public static Activity ToModel(this ActivityEntity entity)
    // {
    //     return new Activity (
    //         entity.IdActivity,
    //         entity.Type,
    //         new DateTime(entity.Date.Year, entity.Date.Month, entity.Date.Day),
    //         new DateTime().Add(entity.StartTime.ToTimeSpan()), // Utilisation de ToTimeSpan() pour obtenir la composante temps sans la date
    //         new DateTime().Add(entity.EndTime.ToTimeSpan()),
    //         entity.EffortFelt,
    //         entity.Variability,
    //         entity.Variance,
    //         entity.StandardDeviation,
    //         entity.Average,
    //         entity.Maximum,
    //         entity.Minimum,
    //         entity.AverageTemperature,
    //         entity.HasAutoPause
    //     );
    // }

    // public static ActivityEntity ToEntity(this Activity model)
    // {
    //     return new ActivityEntity
    //     {
    //         IdActivity = model.Id,
    //         Type = model.Type,
    //         Date = DateOnly.FromDateTime(model.Date),
    //         StartTime = TimeOnly.FromDateTime(model.StartTime),
    //         EndTime = TimeOnly.FromDateTime(model.EndTime),
    //         EffortFelt = model.Effort,
    //         Variability = model.Variability,
    //         Variance = model.Variance,
    //         StandardDeviation = model.StandardDeviation,
    //         Average = model.Average,
    //         Maximum = model.Maximum,
    //         Minimum = model.Minimum,
    //         AverageTemperature = model.AverageTemperature,
    //         HasAutoPause = model.HasAutoPause
    //     };
    // }

    // public static IEnumerable<Activity> ToModels(this IEnumerable<ActivityEntity> entities)
    //     => entities.Select(a => a.ToModel());

    // public static IEnumerable<ActivityEntity> ToEntities(this IEnumerable<Activity> models)
    //     => models.Select(a => a.ToEntity());
}

// using System;
// using Entities;
// using Models;
// using System.Collections.Generic; // Add missing namespace

// namespace Model2Entities
// {
// 	public static class Extension
//     {
        

//         public static TEntity ToEntity<T, TEntity>(this T model)
//             where TEntity : new()
//         {
//             return new TEntity
//             {
//                 Id = model.Id,
//                 Title = model.Title,
//                 Author = model.Author,
//                 Isbn = model.Isbn
//             };
//         }

//         public static IEnumerable<TEntity> ToEntities<T, TEntity>(this IEnumerable<T> models) // Add missing type parameter
//             where TEntity : new() // Add constraint for TEntity
//         {
//             return models.Select(m => m.ToEntity<T, TEntity>());
//         }

//         public static T ToModel<T, TEntity>(this TEntity myTEntity) // Add missing type parameter
//             where T : new() // Add constraint for T
//         {
//             return new T(myTEntity.Id, myTEntity.Title, myTEntity.Author, myTEntity.Isbn);
//         }
        
//         public static IEnumerable<T> ToModels(this IEnumerable<TEntity> TsEntities)
//             => TsEntities.Select(e => e.ToModel());
//     }
// 	{
// 		public static T ToEntity(this T model)
// 			=> new TEntity
// 			{
// 				Id = model.Id,
// 				Title = model.Title,
// 				Author = model.Author,
// 				Isbn = model.Isbn
// 			};

//         public static IEnumerable<T> ToEntities(this IEnumerable<T> models)
//             => models.Select(m => m.ToEntity());

//         public static T ToModel(this T myTEntity)
// 			=> new T(myTEntity.Id, myTEntity.Title, myTEntity.Author, myTEntity.Isbn);
		
// 		public static IEnumerable<T> ToModels(this IEnumerable<TEntity> TsEntities)
// 			=> TsEntities.Select(e => e.ToModel());
//     }
// }