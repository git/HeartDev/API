using Dto;
using Dto.Tiny;
using Model;
using Model.Repository;
using Shared;
using Model.Manager;
using Microsoft.Extensions.Logging;
using Entities;
using EFMappers;
using Entities2Dto;
using Microsoft.EntityFrameworkCore;

namespace Model2Entities;

public partial class DbDataManager : IDataManager
{
    public class ActivityRepository : IActivityRepository
    {
        private readonly DbDataManager _dataManager;
        private readonly ILogger<DbDataManager> _logger;
        public ActivityRepository(DbDataManager dbDataManager, ILogger<DbDataManager> logger)
        {
            this._dataManager = dbDataManager;
            this._logger = logger;
        }

        public async Task<IEnumerable<ActivityTinyDto>?> GetActivities(int index, int count, ActivityOrderCriteria criteria, bool descending = false)
        {
            _logger.LogInformation($"GetActivities with index {index} and count {count}", index, count);
            _logger.LogInformation($"GetActivities with criteria {criteria} and descending {descending}", criteria, descending);
            
            var activities = _dataManager.DbContext.ActivitiesSet
                .GetItemsWithFilterAndOrdering(b => true, index, count, criteria, descending).ToTinyDtos();
            
            _logger.LogInformation($"Retrieved {activities.Count()} activities");
            return await Task.FromResult(activities);
        }

        public async Task<Activity?> GetActivityByIdAsync(int id)
        {
            _logger.LogInformation($"GetActivityByIdAsync with id {id}", id);

            var activityEntity =  await _dataManager.DbContext.ActivitiesSet.IncludeStandardProperties().SingleOrDefaultAsync(a => a.IdActivity == id);
            var activity = activityEntity != null ? activityEntity.ToModel() : null;

            if (activity != null)
                _logger.LogInformation($"Retrieved activity with ID {id}");
            else
                _logger.LogWarning($"No activity found with ID {id}");

            return await Task.FromResult(activity);
        }
        public async Task<ResponseActivityDto> GetActivityById(int id)
        {
            _logger.LogInformation($"GetActivityByIdAsync with id {id}", id);

            var activityEntity =  await _dataManager.DbContext.ActivitiesSet.IncludeAll(_dataManager.DbContext).SingleOrDefaultAsync(a => a.IdActivity == id);
            var activity = activityEntity != null ? activityEntity.ToResponseDto() : null;

            if (activity != null)
                _logger.LogInformation($"Retrieved activity with ID {id}");
            else
                _logger.LogWarning($"No activity found with ID {id}");

            return await Task.FromResult(activity);
        }

        public async Task<Activity?> AddActivity(Activity activity)
        {
            try
            {
                _logger.LogInformation("Adding new activity");
                var addedActivity =  (await _dataManager.DbContext.AddItem(activity.ToEntity()))?.ToModel();
                if (addedActivity != null)
                    _logger.LogInformation($"Added activity with ID {addedActivity.Id}");
                else
                    _logger.LogError("Failed to add activity");
                return await Task.FromResult(addedActivity);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while adding activity");
                throw;
            }
        }

        public async Task<ResponseActivityDto?> AddActivity(NewActivityDto activity)
        {
            try
            {
                _logger.LogInformation("Adding new activity");

                var addedActivity = activity.Activity.ToEntity();
                    addedActivity.DataSourceId = activity.DataSourceId;
                    addedActivity.AthleteId = activity.AthleteId;
                    addedActivity.HeartRates = activity.HeartRates.ToEntities().ToList();
                    
                    await _dataManager.DbContext.AddItem(addedActivity);
                    _logger.LogInformation($"Added activity with ID {addedActivity.IdActivity}");
                    _dataManager.DbContext.SaveChanges();
                    
                return await Task.FromResult(addedActivity.ToResponseDto());
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while adding activity");
                throw;
            }
        }

        public async Task<ResponseActivityDto> UpdateActivity(int id, ActivityTinyDto activity)
        {
            try
            {
                var entity =await _dataManager.DbContext.ActivitiesSet.IncludeAll(_dataManager.DbContext)
                    .FirstOrDefaultAsync(a => a.IdActivity == id);
                
                if (entity != null)
                {
                    
                    _logger.LogInformation($"Updating activity with ID {id}");
                    entity.Type = activity.Type;
                    entity.Date = DateOnly.FromDateTime(activity.Date);
                    entity.StartTime = TimeOnly.FromDateTime(activity.StartTime);
                    entity.EndTime = TimeOnly.FromDateTime(activity.EndTime);
                    entity.EffortFelt = activity.EffortFelt;
                    entity.Variability = activity.Variability;
                    entity.Variance = activity.Variance;
                    entity.StandardDeviation = activity.StandardDeviation;
                    entity.Average = activity.Average;
                    entity.Maximum = activity.Maximum;
                    entity.Minimum = activity.Minimum;
                    entity.AverageTemperature = activity.AverageTemperature;
                    entity.HasAutoPause = activity.HasAutoPause;
                    _dataManager.DbContext.SaveChanges();

                    _logger.LogInformation($"Updated activity with ID {id}");
                    return await Task.FromResult(entity.ToResponseDto());
                }
                else
                {
                    _logger.LogError($"Failed to update activity with ID {id}");
                    return await Task.FromResult<ResponseActivityDto?>(null);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error occurred while updating activity with ID {id}");
                throw;
            }
        }

        public async Task<bool> DeleteActivity(int id)
        {
            try
            {
                _logger.LogInformation($"Deleting activity with ID {id}");
                var isDeleted = await _dataManager.DbContext.DeleteItem<ActivityEntity>(id);
                if (isDeleted)
                    _logger.LogInformation($"Deleted activity with ID {id}");
                else
                    _logger.LogWarning($"No activity found with ID {id}");
                return await Task.FromResult(isDeleted);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error occurred while deleting activity with ID {id}");
                throw;
            }
        }

        public async Task<int> GetNbItems()
        {
            try
            {
                _logger.LogInformation("Getting the total number of activities");
                var count = await _dataManager.DbContext.ActivitiesSet.CountAsync();
                _logger.LogInformation($"Total number of activities: {count}");
                return await Task.FromResult(count);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while getting the total number of activities");
                throw;
            }
        }

        public Task<IEnumerable<Activity>> GetActivitiesByUser(int userId, int index, int count, ActivityOrderCriteria criteria, bool descending = false)
        {
            try {
                _logger.LogInformation($"Getting activities for user with ID {userId}");
                var activities = _dataManager.DbContext.ActivitiesSet
                    .IncludeStandardProperties().GetItemsWithFilterAndOrdering(b => b.AthleteId == userId, index, count, criteria, descending).ToModels();
                _logger.LogInformation($"Retrieved {activities.Count()} activities for user with ID {userId}");
                return Task.FromResult(activities);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error occurred while getting activities for user with ID {userId}");
                throw;
            }
        }

        public Task<int> GetNbActivitiesByUser(int userId)
        {
            try {
                _logger.LogInformation($"Getting the total number of activities for user with ID {userId}");
                var count = _dataManager.DbContext.ActivitiesSet.Count(b => b.AthleteId == userId);
                _logger.LogInformation($"Total number of activities for user with ID {userId}: {count}");
                return Task.FromResult(count);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error occurred while getting the total number of activities for user with ID {userId}");
                throw;
            }
        }
    }
}