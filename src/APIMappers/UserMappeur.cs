﻿using Dto;
using Model;
using Shared;

namespace APIMappers;

public static class UserMappeur
{
    private static GenericMapper<User, UserDto> _mapper = new GenericMapper<User, UserDto>();

    public static UserDto ToDto(this User user)
    {
        return user.ToU(_mapper, userDto =>  new UserDto
        {
            Id = user.Id,
            Username = user.Username,
            ProfilePicture = user.ProfilePicture,
            LastName = user.LastName,
            FirstName = user.FirstName,
            Email = user.Email,
            Password = user.MotDePasse,
            Sexe = user.Sexe,
            Lenght = user.Lenght,
            Weight = user.Weight,
            DateOfBirth = user.DateOfBirth,
            IsCoach = user.Role is Coach
        });
     
    }
    

    public static User ToModel(this UserDto userDto)
    {
        return userDto.ToT(_mapper, user => new User
        {
            Username = userDto.Username,
            ProfilePicture = userDto.ProfilePicture,
            LastName = userDto.LastName,
            FirstName = userDto.FirstName,
            Email = userDto.Email,
            MotDePasse = userDto.Password,
            Sexe = userDto.Sexe,
            Lenght = userDto.Lenght,
            Weight = userDto.Weight,
            DateOfBirth = userDto.DateOfBirth,
            Role = userDto.IsCoach ? new Coach() : new Athlete()

        });
    }
    
    public static IEnumerable<User> ToModels(this IEnumerable<UserDto> dtos)
    => dtos.Select(dto => dto.ToModel());
    
    public static IEnumerable<UserDto> ToDtos(this IEnumerable<User> models)
    => models.Select(model => model.ToDto());

}