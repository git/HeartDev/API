using Dto;
using Dto.Tiny;
using Entities;
using Model;
using Shared;

namespace APIMappers;

public static class ActivityMapper
{
    private static GenericMapper<Activity, ActivityDto> _mapper = new();
    private static GenericMapper<ActivityTinyDto, ActivityEntity> _mapperTiny = new ();

    public static Activity ToModel(this ActivityDto activityDto, User user)
    {
        Func<ActivityDto, Activity> create = activity => new Activity 
        {
            Id = activity.Id,
            Type = activity.Type,
            Date = activity.Date,
            StartTime = activity.StartTime,
            EndTime = activity.EndTime,
            Effort = activity.EffortFelt,
            Variability = activity.Variability,
            Variance = activity.Variance,
            StandardDeviation = activity.StandardDeviation,
            Average = activity.Average,
            Maximum = activity.Maximum,
            Minimum = activity.Minimum,
            AverageTemperature = activity.AverageTemperature,
            HasAutoPause = activity.HasAutoPause
        };

        Action<ActivityDto, Activity> link = (activity, model) =>
        {
            if (activity.DataSource != null)
                model.DataSource = user.DataSources.FirstOrDefault(ds => ds.Id == activity.DataSource.Id);
            model.Athlete = user;
        };

        var act =  activityDto.ToT(_mapper, create, link);
        act.HeartRates.AddRange(activityDto.HeartRates.ToModels(act).ToList());
        
        return act;
    }
    
    public static ActivityDto ToDto(this Activity model)
    {
        Func<Activity, ActivityDto> create = activity => new ActivityDto
        {
            Id = activity.Id,
            Type = activity.Type,
            Date = activity.Date,
            StartTime = activity.StartTime,
            EndTime = activity.EndTime,
            EffortFelt = activity.Effort,
            Variability = activity.Variability,
            Variance = activity.Variance,
            StandardDeviation = activity.StandardDeviation,
            Average = activity.Average,
            Maximum = activity.Maximum,
            Minimum = activity.Minimum,
            AverageTemperature = activity.AverageTemperature,
            HasAutoPause = activity.HasAutoPause,
            AthleteId = activity.Athlete.Id
        };
        
        Action<Activity, ActivityDto> link = (activity, dto) =>
        {
            dto.HeartRates = activity.HeartRates.ToDtos().ToArray();
            dto.DataSource = activity.DataSource.ToDto();
            dto.Athlete = activity.Athlete.ToDto();
        };
        
        return model.ToU(_mapper, create, link);
    }
    
    public static ActivityEntity ToEntity(this ActivityTinyDto tinyDto)
    {
        Func<ActivityTinyDto, ActivityEntity> create = dto => new ActivityEntity
        {
            Type = dto.Type,
            Date = DateOnly.FromDateTime(dto.Date),
            StartTime = TimeOnly.FromDateTime(dto.StartTime),
            EndTime = TimeOnly.FromDateTime(dto.EndTime),
            EffortFelt = dto.EffortFelt,
            Variability = dto.Variability,
            Variance = dto.Variance,
            StandardDeviation = dto.StandardDeviation,
            Average = dto.Average,
            Maximum = dto.Maximum,
            Minimum = dto.Minimum,
            AverageTemperature = dto.AverageTemperature,
            HasAutoPause = dto.HasAutoPause
        };
        
        return tinyDto.ToU(_mapperTiny, create);
    }
    
    public static IEnumerable<Activity> ToModels(this IEnumerable<ActivityDto> dtos, User user)
    => dtos.Select(dto => dto.ToModel(user));
    
    public static IEnumerable<ActivityDto> ToDtos(this IEnumerable<Activity> models)
    => models.Select(model => model.ToDto());
    
    
    

}