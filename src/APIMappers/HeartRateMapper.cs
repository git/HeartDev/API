using Dto;
using Model;
using Shared;

namespace APIMappers;

public static class HeartRateMapper
{
    private static GenericMapper<HeartRate, HeartRateDto> _mapper = new();
    
    public static HeartRate ToModel(this HeartRateDto dto,Activity activityDto)
    {
        Func<HeartRateDto, HeartRate> create = heartRateDto =>
            new HeartRate(heartRateDto.HeartRate, TimeOnly.FromDateTime(heartRateDto.Timestamp), activityDto, heartRateDto.Latitude, heartRateDto.Longitude, heartRateDto.Altitude, heartRateDto.Cadence, heartRateDto.Distance, heartRateDto.Speed, heartRateDto.Power, heartRateDto.Temperature);
        
        return dto.ToT(_mapper, create);
    }
    
    public static HeartRateDto ToDto(this HeartRate model)//Activity activity
    {
        var activity = new DateTime();
        Func<HeartRate, HeartRateDto> create = heartRate => 
            new HeartRateDto
            {
                Timestamp = new DateTime(activity.Date.Year, activity.Date.Month, activity.Date.Day, heartRate.Timestamp.Hour, heartRate.Timestamp.Minute, heartRate.Timestamp.Second),
                Latitude = heartRate.Latitude,
                Longitude = heartRate.Longitude,
                Altitude = heartRate.Altitude,
                HeartRate = heartRate.Bpm,
                Cadence = heartRate.Cadence,
                Distance = heartRate.Distance,
                Speed = heartRate.Speed,
                Power = heartRate.Power,
                Temperature = heartRate.Temperature
            };
        return model.ToU(_mapper, create);
    }
    
    public static IEnumerable<HeartRate> ToModels(this IEnumerable<HeartRateDto> dtos, Activity activityDto)
    => dtos.Select(d => d.ToModel(activityDto));
    
    public static IEnumerable<HeartRateDto> ToDtos(this IEnumerable<HeartRate> models)
    => models.Select(m => m.ToDto());
    
    
}