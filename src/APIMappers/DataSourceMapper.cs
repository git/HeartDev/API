using Dto;
using Model;
using Shared;

namespace APIMappers;

public static class DataSourceMapper
{
    
    private static GenericMapper<DataSource, DataSourceDto> _mapper = new ();
    
    public static DataSource ToModel(this DataSourceDto dto, User user)
    {
        Func<DataSourceDto, DataSource> create = dataSourceDto =>
            new DataSource(dataSourceDto.Id, dataSourceDto.Type, dataSourceDto.Model, dataSourceDto.Precision,
                new List<User> { user }, user.Activities.Where(a => a.DataSource.Id == dataSourceDto.Id).ToList());
        /*
        Action<DataSourceDto, DataSource> link = (dataSourceDto, model) =>
        {
            model.Activities.AddRange(dataSourceDto.Activities.ToModels());
            model.Athletes.AddRange(dataSourceDto.Athletes.ToModels());
        };*/
        
        return dto.ToT(_mapper, create);
    }
    
    public static DataSourceDto ToDto(this DataSource model)
    {
        Func<DataSource, DataSourceDto> create = dataSource => 
            new DataSourceDto
            {
                Id = dataSource.Id,
                Type = dataSource.Type,
                Model = dataSource.Model,
                Precision = dataSource.Precision,
            };
        Action<DataSource, DataSourceDto> link = (dataSource, dto) =>
        {
            dto.Activities = dataSource.Activities.ToDtos().ToArray();
            dto.Athletes = dataSource.Athletes.ToDtos().ToArray();
        };
        return model.ToU(_mapper, create, link);
    }
    
    public static IEnumerable<DataSource> ToModels(this IEnumerable<DataSourceDto> dtos, User user)
    => dtos.Select(d => d.ToModel(user));
    
    public static IEnumerable<DataSourceDto> ToDtos(this IEnumerable<DataSource> models)
    => models.Select(m => m.ToDto());
 
    
}