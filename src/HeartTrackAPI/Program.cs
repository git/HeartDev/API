using DbContextLib;
using HeartTrackAPI.Utils;
using StubbedContextLib;

var builder = WebApplication.CreateBuilder(args);

builder.Logging.AddConsole();
builder.WebHost.ConfigureKestrel(serverOptions =>
{
    serverOptions.Limits.MaxRequestBodySize = long.MaxValue;
});

var init = new AppBootstrap(builder.Configuration);

init.ConfigureServices(builder.Services);

var app = builder.Build();

init.Configure(app, app.Environment);

var context = app.Services.GetService<HeartTrackContext>() ?? app.Services.GetService<TrainingStubbedContext>();

context!.Database.EnsureCreated();

app.Run();