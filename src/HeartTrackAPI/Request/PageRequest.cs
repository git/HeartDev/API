using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace HeartTrackAPI.Request;

public class PageRequest
{
    public string? OrderingPropertyName { get; set; } = null;
    public bool? Descending { get; set; }  = false;
    public int Index { get; set; } = 0;
    public int Count { get; set; } = 1;
}
