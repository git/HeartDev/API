using APIMappers;
using Dto;
using Dto.Tiny;
using HeartTrackAPI.Request;
using HeartTrackAPI.Responce;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shared;
using Model.Manager;
using Model.Repository;

namespace HeartTrackAPI.Controllers;
[ApiController]
[ApiVersion("1.0")]
[Route("api/v{version:apiVersion}/[controller]")]
[Authorize]

public class ActivityController : Controller
{
    private readonly IActivityRepository _activityService;
    private readonly ILogger<ActivityController> _logger;
    private readonly IUserRepository _userRepository;
    private readonly IDataSourceRepository<DataSourceTinyDto> _dataSourceRepository;


    public ActivityController(IDataManager dataManager, ILogger<ActivityController> logger)
    {
        _activityService = dataManager.ActivityRepo;
        _userRepository = dataManager.UserRepo;
        _dataSourceRepository = dataManager.DataSourceRepo;
        _logger = logger;
    }

    [HttpGet]
    [ProducesResponseType(typeof(PageResponse<ActivityTinyDto>), 200)]
    [ProducesResponseType(400)]
    [ProducesResponseType(500)] 
    public async Task<ActionResult<PageResponse<ActivityTinyDto>>> GetActivities([FromQuery] PageRequest pageRequest)
    {
        try
        {
            var totalCount = await _activityService.GetNbItems();
            if (pageRequest.Count * pageRequest.Index >= totalCount)
            {
                _logger.LogError("To many object is asked the max is {totalCount} but the request is superior of ", totalCount);
                return BadRequest("To many object is asked the max is : " + totalCount);
            }
            _logger.LogInformation("Executing {Action} with parameters: {Parameters}", nameof(GetActivities), pageRequest);
            var activities = await _activityService.GetActivities(pageRequest.Index, pageRequest.Count, Enum.TryParse(pageRequest.OrderingPropertyName, out ActivityOrderCriteria result) ? result : ActivityOrderCriteria.None, pageRequest.Descending ?? false);
            if(activities == null)
            {
                return NotFound("No activities found");
            }

            var pageResponse =
                new PageResponse<ActivityTinyDto>(pageRequest.Index, pageRequest.Count, totalCount, activities);
            return Ok(pageResponse);
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error while getting all activities");
            return StatusCode(500);
        }
    }
    
    [HttpPost]
    public async Task<IActionResult> PostActivity(NewActivityDto activityDto)
    {
        _logger.LogInformation("Executing {Action} with parameters: {Parameters}, {add}", nameof(PostActivity), activityDto.Activity.Average, activityDto.HeartRates[0].Timestamp);
        var user = await _userRepository.GetUserTinyById(activityDto.AthleteId);
        if (user == null)
        {
            _logger.LogError("Athlete with id {id} not found", activityDto.AthleteId);
            return NotFound($"Athlete with id {activityDto.AthleteId} not found");
        }
        if (activityDto.DataSourceId != null)
        {
            var dataSource = await _dataSourceRepository.GetItemById(activityDto.DataSourceId.Value);
            
            if (dataSource == null)
            {
                _logger.LogError("DataSource with id {id} not found", activityDto.DataSourceId);
                return NotFound($"DataSource with id {activityDto.DataSourceId} not found");
            }
        }
        
        var result = await _activityService.AddActivity(activityDto);
        
        if (result == null)
        {
            return BadRequest();
        }
        _logger.LogInformation("Activity added with id {id}", result.Id);

        return CreatedAtAction(nameof(GetActivity), new { id = result.Id }, result);
    }
    
    [HttpGet("{id}")]
    public async Task<ActionResult<ResponseActivityDto>> GetActivity(int id)
    {
        try
        {
            _logger.LogInformation("Executing {Action} with parameters: {Parameters}", nameof(GetActivity), id);

            var activity = await _activityService.GetActivityById(id);
            if (activity == null)
            {
                _logger.LogError("Activity with id {id} not found", id);

                return NotFound($"Activity with id {id} not found");
            }
            return Ok(activity);
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error while getting activity by id {id}", id);
            return Problem();
        }
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> PutActivity(int id, ActivityTinyDto activityDto)
    {
        var result = await _activityService.UpdateActivity(id, activityDto);
        if (result == null)
        {
            return NotFound();
        }

        return Ok(result);
    }

    /// <summary>
    /// Supprime une activity spécifique.
    /// </summary>
    /// <param name="id">L'identifiant de l'activity à supprimer.</param>
    /// <returns>Action result.</returns>
    /// <response code="200">Activity supprimé avec succès.</response>
    /// <response code="404">Activity non trouvé.</response>
        /// <response code="500">Erreur interne du serveur.</response>
    [HttpDelete("{id}")]
    [ProducesResponseType(200)]
    [ProducesResponseType(404)]
    [ProducesResponseType(500)]
    public async Task<IActionResult> DeleteActivity(int id)
    {
        try
        {
            _logger.LogInformation("Executing {Action} with parameters: {Parameters} for {Id}", nameof(DeleteActivity), null,id);
            
            
            var activity = await _activityService.GetActivityByIdAsync(id);
            if (activity == null)
            {
                _logger.LogError("Activity with id {id} not found", id);
                return NotFound($"Activity with id {id} not found");
            }
            var isDeleted = await _activityService.DeleteActivity(id);
            if(!isDeleted)
            {
                _logger.LogError("Error while deleting activity with id {id}", id);
                return Problem("Error while deleting activity");
            }
            return Ok();
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error while deleting activity");
            return Problem();
        }
    }
    
/*
    public async Task<FileUploadSummary> UploadFileAsync(Stream fileStream, string contentType)
    {
        var fileCount = 0;
        long totalSizeInBytes = 0;
        var boundary = GetBoundary(MediaTypeHeaderValue.Parse(contentType));
        var multipartReader = new MultipartReader(boundary, fileStream);
        var section = await multipartReader.ReadNextSectionAsync();
        var filePaths = new List<string>();
        var notUploadedFiles = new List<string>();
    
        while (section != null)
        {
            var fileSection = section.AsFileSection();
            if (fileSection != null)
            {
                totalSizeInBytes += await SaveFileAsync(fileSection, filePaths, notUploadedFiles);
                fileCount++;
            }
            section = await multipartReader.ReadNextSectionAsync();
        }
        return new FileUploadSummary
        {
            TotalFilesUploaded = fileCount,
            TotalSizeUploaded = ConvertSizeToString(totalSizeInBytes),
            FilePaths = filePaths,
            NotUploadedFiles = notUploadedFiles
        };
    }*/
}