using Dto;
using Dto.Tiny;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Model.Manager;
using Model.Repository;
using Model.utils;

namespace HeartTrackAPI.Controllers;

[ApiController]
[ApiVersion("1.0")]
[Route("api/[controller]")]
[Authorize]
public class AnalysisController : Controller
{
    
    private readonly IActivityRepository _activityService;
    private readonly ILogger<AnalysisController> _logger;


    public AnalysisController(IDataManager dataManager, ILogger<AnalysisController> logger)
    {
        _activityService = dataManager.ActivityRepo;
        _logger = logger;
    }
    
   


    [HttpGet("activity/{activityId}")]
    public async Task<IActionResult> AnalyseByActivityId(int activityId)
    {
        var activity = await _activityService.GetActivityById(activityId);
        if (activity == null)
        {
            _logger.LogInformation($"Activity with ID {activityId} not found.");
            return NotFound($"Activity with ID {activityId} not found.");
        }
        // for the moment no need to get the user Entity [Dave]
        var user = activity.Athlete;
        if (user == null)
        {
            _logger.LogInformation($"User not found for activity ID {activityId}.");
            return NotFound($"User not found for activity ID {activityId}.");
        }
        
        var analysis = ActivityAnalysis.FromActivityData(activity);
        return Ok(analysis);
    }
    

}


/*
 /*
 public class HeartRateZoneResult
   {
       public string Zone { get; set; }
       public TimeSpan TimeSpent { get; set; }
   }
   
     private readonly List<HeartRateZone> _heartRateZones = new()
   {
       new() { Name = "Repos", MinHeartRate = 0, MaxHeartRate = 60 },
       new() { Name = "Aérobie légère", MinHeartRate = 61, MaxHeartRate = 90 },
       new() { Name = "Aérobie", MinHeartRate = 91, MaxHeartRate = 140 },
       new() { Name = "Anaérobie", MinHeartRate = 141, MaxHeartRate = 180 },
       new() { Name = "VO2 Max", MinHeartRate = 181, MaxHeartRate = 220 }
   };
   [HttpGet("heart-rate/zones/{activityId}")]
   public IActionResult GetActivityHeartRateZones(int activityId)
   {
       var heartRateTinyDtos = _activityService.GetActivityById(activityId).Result?.HeartRates;
       if (heartRateTinyDtos != null)
       {
           var heartRates = heartRateTinyDtos.ToList();
           var results = _heartRateZones.Select(zone => new HeartRateZoneResult
           {
               Zone = zone.Name,
               TimeSpent = CalculateTimeInZone(zone, heartRates)
           }).ToList();

           return Ok(results);
       }

       return NotFound("Not heart rates");

   }
   private TimeSpan CalculateTimeInZone(HeartRateZone zone, List<HeartRateTinyDto> heartRates)
   {
       var secondsInZone =
           heartRates.Count(hr => hr.HeartRate >= zone.MinHeartRate && hr.HeartRate <= zone.MaxHeartRate);
       return TimeSpan.FromSeconds(secondsInZone);
   }* /
   
    [HttpGet("getOptimizedPath")]
    public IActionResult GetOptimizedPath(int activityId)
    {
        var heartRateData = GetMockHeartRateData();

        var sortedData = heartRateData.OrderBy(x => x.Timestamp).ToList();

        var path = new GeoJsonPath();
        foreach (var item in sortedData)
        {
            if (item.Latitude.HasValue && item.Longitude.HasValue)
            {
                path.Coordinates.Add(new GeoJsonCoordinate(item.Longitude.Value, item.Latitude.Value));
            }
        }

        return Ok(path.ToGeoJson());
    }
*/