namespace HeartTrackAPI.Responce;

public class PageResponse<T>
{
    // The index of the first item
    public int Index { get; set; } = 1;
    // The number of items
    public int Count { get; set; } = 1;
    // The total number of items
    public int Total { get; set; } = 1;
    // The items
    public IEnumerable<T> Items { get; set; }
    
    public PageResponse(int index, int count, int total, IEnumerable<T> items)
    {
        Index = index;
        Count = count;
        Total = total;
        Items = items;
    }

}