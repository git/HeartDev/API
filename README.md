<div align = center>

  <h1>HeartTrack</h1>
  <img src="docs/images/logo.png" />
    
</div>


<div align = center>



---
&nbsp;  ![C#](https://img.shields.io/badge/C%23-000?style=for-the-badge&logo=c-sharp&logoColor=white&color=purple)
&nbsp;  ![Entity Framework](https://img.shields.io/badge/Entity_Framework-000?style=for-the-badge&logo=.net&logoColor=white&color=blue)
&nbsp;  ![API](https://img.shields.io/badge/API-000?style=for-the-badge&logo=api&logoColor=white&color=orange)

</br>  

[![Build Status](https://codefirst.iut.uca.fr/api/badges/HeartDev/API/status.svg)](https://codefirst.iut.uca.fr/HeartDev/API)
[![Bugs](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=HeartTrack-API&metric=bugs&token=c3e0444eb978e1d346ed0041c552b24870316a03)](https://codefirst.iut.uca.fr/sonar/dashboard?id=HeartTrack-API)
[![Coverage](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=HeartTrack-API&metric=coverage&token=c3e0444eb978e1d346ed0041c552b24870316a03)](https://codefirst.iut.uca.fr/sonar/dashboard?id=HeartTrack-API)
[![Duplicated Lines (%)](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=HeartTrack-API&metric=duplicated_lines_density&token=c3e0444eb978e1d346ed0041c552b24870316a03)](https://codefirst.iut.uca.fr/sonar/dashboard?id=HeartTrack-API)
[![Maintainability Rating](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=HeartTrack-API&metric=sqale_rating&token=c3e0444eb978e1d346ed0041c552b24870316a03)](https://codefirst.iut.uca.fr/sonar/dashboard?id=HeartTrack-API)
[![Reliability Rating](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=HeartTrack-API&metric=reliability_rating&token=c3e0444eb978e1d346ed0041c552b24870316a03)](https://codefirst.iut.uca.fr/sonar/dashboard?id=HeartTrack-API)
[![Security Rating](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=HeartTrack-API&metric=security_rating&token=c3e0444eb978e1d346ed0041c552b24870316a03)](https://codefirst.iut.uca.fr/sonar/dashboard?id=HeartTrack-API)
[![Vulnerabilities](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=HeartTrack-API&metric=vulnerabilities&token=c3e0444eb978e1d346ed0041c552b24870316a03)](https://codefirst.iut.uca.fr/sonar/dashboard?id=HeartTrack-API)


</div>  
  
# Table des matières
[Présentation](#présentation) | [Répartition du Git](#répartition-du-git) | [Documentation](#documentation) | [Prerequisites](#prerequisites) | [Getting Started](#getting-started) | [Features](#features) | [Ce que nous avons fait](#ce-que-nous-avons-fait) | [Fabriqué avec](#fabriqué-avec) | [Contributeurs](#contributeurs) | [Comment contribuer](#comment-contribuer) | [License](#license) | [Remerciements](#remerciements)



## Présentation

**Nom de l'application :** HeartTrack

### Contexte

HeartTrack est une application web PHP et mobile Android destinée aux sportifs et aux coachs afin de permettre l'analyse de courbes de fréquences cardiaques et le suivi d'équipe sportive. L'objectif principal de cette application est de récupérer les données de fréquence cardiaque à partir de fichiers .FIT, de les afficher sous forme de courbes, d'identifier des paternes, de fournir des statistiques et de réaliser des prédictions liées à l'effort physique, à la chaleur, à la récupération, etc.
!! Fuseaux horaires de base

### Récapitulatif du Projet

Le projet HeartTrack, avec son application HeartTrack, vise à offrir une solution Open Source d'analyse des données de fréquence cardiaque, en mettant l'accent sur les besoins des sportifs et des coachs. L'application sera capable de traiter et d'interpréter les données de manière intelligente, fournissant ainsi des informations précieuses pour optimiser les performances sportives et la santé.


## Répartition du Git

[**Sources**](Sources/) : **Code de l'application**

[**Documents**](docs/Diagramme/README_DIAGRAMMES.md) : **Documentation de l'application et diagrammes**

[**Wiki**](https://codefirst.iut.uca.fr/git/HeartDev/Web/wiki/PHP) : **Wiki de notre projet (attendus PHP)**

---

Le projet HeartTrack utilise un modèle de flux de travail Git (Gitflow) pour organiser le développement. Voici une brève explication des principales branches :

- **branche WORK-** : Cette branche est utilisée pour le développement de nouvelles fonctionnalités. Chaque fonctionnalité est développée dans une branche séparée. WORK-NOMDUDEV permet de savoir qui travaille sur quoi.

- **branche master** : Cette branch est  la dernière version stable de l'application. Les modifications sur cette branche sont généralement destinées à des mises en production.

- **branche test** : Cette branche est utilisée pour tester les différentes fonctionnalités avant de les fusionner.

- **branche merge** : Cette branche est utilisée pour fusionner les différentes branches de fonctionnalités.


## Documentation 
Documentation et informations à propos de `HearthTrack` disponible [ici](https://codefirst.iut.uca.fr/documentation/HeartDev/API/doxygen/)

### Prerequisites
* [![.NET 8.0](https://img.shields.io/badge/Langage-.NET-000?style=for-the-badge&logo=.net&logoColor=white&color=blue)](https://dotnet.microsoft.com/download/dotnet/8.0)
* [![Entity Framework](https://img.shields.io/badge/ORM-Entity_Framework-000?style=for-the-badge&logo=.net&logoColor=white&color=blue)](https://docs.microsoft.com/fr-fr/ef/)
* [![API](https://img.shields.io/badge/API-000?style=for-the-badge&logo=api&logoColor=white&color=orange)](https://docs.microsoft.com/fr-fr/aspnet/core/web-api/?view=aspnetcore-8.0)
* [![Visual Studio](https://img.shields.io/badge/IDE-Visual_Studio-000?style=for-the-badge&logo=visual-studio&logoColor=white&color=purple)](https://visualstudio.microsoft.com/fr/)
* [![Git](https://img.shields.io/badge/Versioning-Git-000?style=for-the-badge&logo=git&logoColor=white&color=red)](https://git-scm.com/)
  
## Getting Started

## Ce que nous avons fait
### Entity Framework
réalisé | niveau | description | coeff | jalon   
--- | --- | --- | --- | ---  
✅ | ☢️ | Le dépôt doit être accessible par l'enseignant | ☢️ | J1  
✅ | ☢️ | un .gitignore doit exister au premier push | ☢️ | J1   
✅ | 🎬 | les *projets* et les tests compilent | 1 | J1 & J2    
✅ | 🎬 | le projet et le tests s'exécutent sans bug (concernant la partie persistance) | 3 | J1 & J2    
✅ | 🟢 | Transcription du modèle : Modèle vers entités (et inversement) | 2 | J1   
✅ | 🟢 | Requêtes CRUD simples (sur une table) | 1 | J1   
✅ | 🟢 | Utilisation de LINQ to Entities | 2 | J1   
✅ | 🟡 | Injection / indépendance du fournisseur | 1 | J1   
✅ | 🟡 | Requêtes CRUD sur des données complexes (images par exemple) | 2 | J1   
✅ | 🟢 | Tests - Appli Console | 1 | J1   
✅ | 🟢 | Tests - Tests unitaires (avec SQLite in memory) | 2 | J1   
✅ | 🟢 | Tests - Données stubbées et/ou Moq | 1 | J1   
✅ | 🟡 | CI : build, tests, Sonar (doc?) | 1 | J1   
✅ | 🟡 | Utilisation de relations (One-to-One, One-to-Many, Many-to-Many) (+ mapping, TU, Requêtes) | 4 | J1   
✅ | 🟢 | Liens avec le web service | 2 | J1   
✅ | 🟡 | Utilisation d'un *Logger* | 1 | J1   
✅ | 🟡 | Déploiement | 4 | J2   
✅ | 🔴 | Unit of Work / Repository + extras (héritage, accès concurrents...) | 8 | J2   
✅ | 🟢 | Utilisation dans le projet | 2 | J2    
✅ | 🟢 | mon dépôt possède un readme qui apporte quelque chose... | 2 | J2

### API
réalisé | niveau | description | coeff | jalon   
--- | --- | --- | --- | ---  
✅ | ☢️ | Le dépôt doit être accessible par l'enseignant | ☢️ | J1  
✅ | ☢️ | un .gitignore doit exister au premier push | ☢️ | J1   
✅ | 🎬 | les *projets* et les tests compilent | 1 | J1 & J2    
✅ | 🎬 | le projet et le tests s'exécutent sans bug (concernant la partie persistance) | 4 | J1 & J2    
✅ | 🟢 | Modèle <-> DTO | 1 | J1   
✅ | 🟢 | Entities <-> DTO | 1 | J1   
✅ | 🟡 | Authentification | 4 | J1   
✅ | 🟢 | Requêtes GET, PUT, POST, DELETE sur des données simples (1 seul type d'objet en retour, propriétés de types natifs) | 2 | J1   
✅ | 🟡 | Pagination & filtrage | 2 | J1   
✅ | 🟢 | Injection de service | 2 | J1   
✅ | 🟡 | Requêtes GET, PUT, POST, DELETE sur des données complexes (plusieurs données complexes en retour) | 4 | J1   
✅ | 🟢 | Tests - Appli Console (consommation des requêtes) | 4 | J1   
✅ | 🟢 | Tests - Tests unitaires (avec Stub et/ou Moq) | 2 | J1   
✅ | 🟡 | CI : build, tests, Sonar, Documentation (en particulier Swagger avec exemples...) | 1 | J1   
✅ | 🟢 | Liens avec la persistance en base de données | 4 | J1   
✅ | 🟡 | Utilisation d'un *Logger* | 1 | J1   
✅ | 🟡 | Déploiement | 4 | J2   
✅ | 🟡 | Utilisation dans le projet | 4 | J2    
✅ | 🎬 | mon dépôt possède un readme qui apporte quelque chose... | 1 | J2     

## Fabriqué avec
![.NET](https://img.shields.io/badge/Langage-.NET-000?style=for-the-badge&logo=.net&logoColor=white&color=blue)
![Entity Framework](https://img.shields.io/badge/ORM-Entity_Framework-000?style=for-the-badge&logo=.net&logoColor=white&color=blue)
![API](https://img.shields.io/badge/API-000?style=for-the-badge&logo=api&logoColor=white&color=orange)
![ASP.NET](https://img.shields.io/badge/ASP.NET-000?style=for-the-badge&logo=asp.net&logoColor=white&color=blue)
![Visual Studio](https://img.shields.io/badge/IDE-Visual_Studio-000?style=for-the-badge&logo=visual-studio&logoColor=white&color=purple)
![JetBrains Rider](https://img.shields.io/badge/IDE-JetBrains_Rider-000?style=for-the-badge&logo=rider&logoColor=white&color=purple)
![Git](https://img.shields.io/badge/Versioning-Git-000?style=for-the-badge&logo=git&logoColor=white&color=red)
![SonarQube](https://img.shields.io/badge/Qualit%C3%A9-SonarQube-000?style=for-the-badge&logo=sonarqube&logoColor=white&color=red)
![Drone](https://img.shields.io/badge/CI-Drone-000?style=for-the-badge&logo=drone&logoColor=white&color=orange)
![Docker](https://img.shields.io/badge/Container-Docker-000?style=for-the-badge&logo=docker&logoColor=white&color=blue)
![C#](https://img.shields.io/badge/Langage-C%23-000?style=for-the-badge&logo=c-sharp&logoColor=white&color=purple)
![Doxygen](https://img.shields.io/badge/Documentation-Doxygen-000?style=for-the-badge&logo=doxygen&logoColor=white&color=blue)



## Contributeurs
* [Antoine PEREDERII](https://codefirst.iut.uca.fr/git/antoine.perederii)
* [Paul LEVRAULT](https://codefirst.iut.uca.fr/git/paul.levrault)
* [Kevin MONTEIRO](https://codefirst.iut.uca.fr/git/kevin.monteiro)
* [Antoine PINAGOT](https://codefirst.iut.uca.fr/git/antoine.pinagot)
* [David D'HALMEIDA](https://codefirst.iut.uca.fr/git/david.d_almeida)

## Comment contribuer
1. Forkez le projet (<https://codefirst.iut.uca.fr/git/HeartDev/API>)
2. Créez votre branche (`git checkout -b feature/featureName`)
3. commit vos changements (`git commit -am 'Add some feature'`)
4. Push sur la branche (`git push origin feature/featureName`)
5. Créez une nouvelle Pull Request


## License
Ce projet est sous licence ``MIT`` - voir le fichier [LICENSE.md](LICENSE.md) pour plus d'informations.

## Remerciements
Ce projet a été réalisé dans le cadre de la SAÉ de l'IUT de Clermont-Ferrand.